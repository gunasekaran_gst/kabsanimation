<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});


Route::get('/home', function () {
    return view('home');
});


Route::get('/adobe', function () {
    return view('adobe');
});

Route::get('/after_effects', function () {
    return view('after_effects');
});

Route::get('/animate', function () {
    return view('animate');
});

Route::get('/captivate', function () {
    return view('captivate');
});

Route::get('/creativecloud', function () {
    return view('creativecloud');
});

Route::get('/expresiencedesign', function () {
    return view('expresiencedesign');
});

Route::get('/illustrator', function () {
    return view('illustrator');
});

Route::get('/indesign', function () {
    return view('indesign');
});

Route::get('/lightroom', function () {
    return view('lightroom');
});

Route::get('/muse', function () {
    return view('muse');
});

Route::get('/photoshop', function () {
    return view('photoshop');
});

Route::get('/premierepro', function () {
    return view('premierepro');
});

Route::get('/dsmax', function () {
    return view('dsmax');
});

Route::get('/autocad', function () {
    return view('autocad');
});

Route::get('/fusion', function () {
    return view('fusion');
});

Route::get('/inventor', function () {
    return view('inventor');
});

Route::get('/bulindinginformation', function () {
    return view('bulindinginformation');
});

Route::get('/maya', function () {
    return view('maya');
});

Route::get('/navisworks', function () {
    return view('navisworks');
});

Route::get('/revit', function () {
    return view('revit');
});

Route::get('/cinema', function () {
    return view('cinema');
});

Route::get('/hololens', function () {
    return view('hololens');
});

Route::get('/unity', function () {
    return view('unity');
});

Route::get('/unreal', function () {
    return view('unreal');
});

Route::get('/sketchup', function () {
    return view('sketchup');
});

Route::get('/zbrush', function () {
    return view('zbrush');
});

Route::get('/digitalart', function () {
    return view('digitalart');
});

Route::get('/designtequ', function () {
    return view('designtequ');
});

Route::get('/uxdesign', function () {
    return view('uxdesign');
});

Route::get('/davinci', function () {
    return view('davinci');
});

Route::get('/nuke', function () {
    return view('nuke');
});

Route::get('/autodesk', function () {
    return view('autodesk');
});

Route::get('/game', function () {
    return view('game');
});
Route::get('/quicksearch', function () {
    return view('quicksearch');
});

Route::get('/certification', function () {
    return view('certification');
});
Route::get('/learningpaths', function () {
    return view('learningpaths');
});
Route::get('/bespoke', function () {
    return view('bespoke');
});
Route::get('/ex', function () {
    return view('ex');
});