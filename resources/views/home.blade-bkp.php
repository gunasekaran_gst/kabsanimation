<!DOCTYPE html><!--[if IE 9]>
<html class="ie ie9" lang="en-US" prefix="og: http://ogp.me/ns#"> <![endif]-->
<html itemscope itemtype="http://schema.org/WebPage" lang="en-US" prefix="og: http://ogp.me/ns#">
<head>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Karla%3A400%2C700%7CMontserrat%3A400%2C700"/>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="https://www.academyclass.com/xmlrpc.php">
    <link rel="shortcut icon" href="https://www.academyclass.com/wp-content/uploads/2018/02/Academy-Class-icon.png"/>
    <script>dataLayer = [];</script>
    <style>.async-hide {
            opacity: 0 !important
        }</style>
    <script type="text/javascript">window.smartlook || (function (d) {
            var o = smartlook = function () {
                o.api.push(arguments)
            }, h = d.getElementsByTagName('head')[0];
            var c = d.createElement('script');
            o.api = new Array();
            c.async = true;
            c.type = 'text/javascript';
            c.charset = 'utf-8';
            c.src = 'https://rec.smartlook.com/recorder.js';
            h.appendChild(c);
        })(document);
        smartlook('init', 'c01f9ddac30621812b0f6678925f84d4047032ee');</script>
    <script>(function (w, d, t, r, u) {
            var f, n, i;
            w[u] = w[u] || [], f = function () {
                var o = {ti: "5613427"};
                o.q = w[u], w[u] = new UET(o), w[u].push("pageLoad")
            }, n = d.createElement(t), n.src = r, n.async = 1, n.onload = n.onreadystatechange = function () {
                var s = this.readyState;
                s && s !== "loaded" && s !== "complete" || (f(), n.onload = n.onreadystatechange = null)
            }, i = d.getElementsByTagName(t)[0], i.parentNode.insertBefore(n, i)
        })(window, document, "script", "//bat.bing.com/bat.js", "uetq");</script>
    <noscript><img src="//bat.bing.com/action/0?ti=5613427&Ver=2" height="0" width="0"
                   style="display:none; visibility: hidden;"></noscript>
    <noscript>
        <iframe src="//www.googletagmanager.com/ns.html?id=GTM-NNS6TTN"
                height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                '//www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-NNS6TTN');</script>
    <script type="text/javascript">var enqformcurrloc, enqformcurrdate;</script>
    <script type="application/ld+json">{
      "@context": "http://schema.org/",
      "@type": "Service",
      "serviceType": "Home 2018",
      "areaServed": {
         "@type": "Place",
          "name": "London"
      }
    }
    </script>
    <title>Training Courses for Designers &amp; Developers | Academy Class</title>
    <script>!function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window,
            document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1417846668241961');
        fbq('track', 'PageView');</script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1417846668241961&ev=PageView&noscript=1"
        /></noscript>
    <script>(window.gaDevIds = window.gaDevIds || []).push('5CDcaG');</script>
    <meta name="description"
          content="Get your competitive advantage with an authorized partner for Adobe, Autodesk, Unity Black Magic and Maxon. We&#039;ll take your skill set to a new level with In Class, Onsite, Online, or On-demand training."/>
    <link rel="canonical" href="https://www.academyclass.com/"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Training Courses for Designers &amp; Developers | Academy Class"/>
    <meta property="og:description"
          content="Get your competitive advantage with an authorized partner for Adobe, Autodesk, Unity Black Magic and Maxon. We&#039;ll take your skill set to a new level with In Class, Onsite, Online, or On-demand training."/>
    <meta property="og:url" content="https://www.academyclass.com/"/>
    <meta property="og:site_name" content="Academy Class"/>
    <script type='application/ld+json'>
        {"@context":"https:\/\/schema.org","@type":"WebSite","@id":"#website","url":"https:\/\/www.academyclass.com\/","name":"Academy Class","potentialAction":{"@type":"SearchAction","target":"https:\/\/www.academyclass.com\/?s={search_term_string}","query-input":"required name=search_term_string"}}
    </script>
    <script type='application/ld+json'>
        {"@context":"https:\/\/schema.org","@type":"Organization","url":"https:\/\/www.academyclass.com\/","sameAs":["https:\/\/www.facebook.com\/AcademyClass","https:\/\/www.instagram.com\/academy_class","https:\/\/www.linkedin.com\/academyclass","https:\/\/www.youtube.com\/user\/AcademyClassPodcasts","https:\/\/twitter.com\/academyclass"],"@id":"https:\/\/www.academyclass.com\/#organization","name":"Academy Class","logo":"http:\/\/www.academyclass.com\/wp-content\/uploads\/2016\/03\/logo-aclass.png"}
    </script>
    <link rel='dns-prefetch' href='//chimpstatic.com'/>
    <link rel='dns-prefetch' href='//www.youtube.com'/>
    <link rel='dns-prefetch' href='//f.vimeocdn.com'/>
    <link rel='dns-prefetch' href='//a.optmstr.com'/>
    <link rel='dns-prefetch' href='//fonts.googleapis.com'/>
    <link rel='dns-prefetch' href='//s.w.org'/>
    <link rel="alternate" type="application/rss+xml" title="Academy Class &raquo; Feed"
          href="https://www.academyclass.com/feed/"/>
    <link rel="alternate" type="application/rss+xml" title="Academy Class &raquo; Comments Feed"
          href="https://www.academyclass.com/comments/feed/"/>
    <style type="text/css">img.wp-smiley, img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important
        }</style>
    <style type="text/css">.hasCountdown {
            text-shadow: transparent 0 1px 1px;
            overflow: hidden;
            padding: 5px
        }

        .countdown_rtl {
            direction: rtl
        }

        .countdown_holding span {
            background-color: #ccc
        }

        .countdown_row {
            clear: both;
            width: 100%;
            text-align: center
        }

        .countdown_show1 .countdown_section {
            width: 98%
        }

        .countdown_show2 .countdown_section {
            width: 48%
        }

        .countdown_show3 .countdown_section {
            width: 32.5%
        }

        .countdown_show4 .countdown_section {
            width: 24.5%
        }

        .countdown_show5 .countdown_section {
            width: 19.5%
        }

        .countdown_show6 .countdown_section {
            width: 16.25%
        }

        .countdown_show7 .countdown_section {
            width: 14%
        }

        .countdown_section {
            display: block;
            float: left;
            font-size: 75%;
            text-align: center;
            margin: 3px 0
        }

        .countdown_amount {
            font-size: 200%
        }

        .countdown_descr {
            display: block;
            width: 100%
        }

        a.countdown_infolink {
            display: block;
            border-radius: 10px;
            width: 14px;
            height: 13px;
            float: right;
            font-size: 9px;
            line-height: 13px;
            font-weight: 700;
            text-align: center;
            position: relative;
            top: -15px;
            border: 1px solid
        }

        #countdown-preview {
            padding: 10px
        }</style>
    <link rel='stylesheet' id='dzs.vplayer-css'
          href='https://www.academyclass.com/wp-content/plugins/dzs-videogallery/videogallery/vplayer.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='contact-form-7-css'
          href='https://www.academyclass.com/wp-content/plugins/contact-form-7/includes/css/styles.css' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='feedzy-rss-feeds-css'
          href='https://www.academyclass.com/wp-content/plugins/feedzy-rss-feeds/css/feedzy-rss-feeds.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='woocommerce-layout-css'
          href='https://www.academyclass.com/wp-content/plugins/woocommerce/assets/css/woocommerce-layout.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='woocommerce-smallscreen-css'
          href='https://www.academyclass.com/wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen.css'
          type='text/css' media='only screen and (max-width: 768px)'/>
    <link rel='stylesheet' id='woocommerce-general-css'
          href='https://www.academyclass.com/wp-content/plugins/woocommerce/assets/css/woocommerce.css' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='ywcfav_style-css'
          href='https://www.academyclass.com/wp-content/plugins/yith-woocommerce-featured-video/assets/css/ywcfav_frontend.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='js_composer_front-css'
          href='https://www.academyclass.com/wp-content/plugins/js_composer/assets/css/js_composer.min.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='shopkeeper-styles-css'
          href='https://www.academyclass.com/wp-content/themes/shopkeeper/css/styles.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='shopkeeper-font-awesome-css'
          href='https://www.academyclass.com/wp-content/themes/shopkeeper/inc/fonts/font-awesome/css/font-awesome.min.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='shopkeeper-font-linea-arrows-css'
          href='https://www.academyclass.com/wp-content/themes/shopkeeper/inc/fonts/linea-fonts/arrows/styles.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='shopkeeper-font-linea-basic-css'
          href='https://www.academyclass.com/wp-content/themes/shopkeeper/inc/fonts/linea-fonts/basic/styles.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='shopkeeper-font-linea-basic_elaboration-css'
          href='https://www.academyclass.com/wp-content/themes/shopkeeper/inc/fonts/linea-fonts/basic_elaboration/styles.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='shopkeeper-font-linea-ecommerce-css'
          href='https://www.academyclass.com/wp-content/themes/shopkeeper/inc/fonts/linea-fonts/ecommerce/styles.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='shopkeeper-font-linea-music-css'
          href='https://www.academyclass.com/wp-content/themes/shopkeeper/inc/fonts/linea-fonts/music/styles.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='shopkeeper-font-linea-software-css'
          href='https://www.academyclass.com/wp-content/themes/shopkeeper/inc/fonts/linea-fonts/software/styles.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='shopkeeper-font-linea-weather-css'
          href='https://www.academyclass.com/wp-content/themes/shopkeeper/inc/fonts/linea-fonts/weather/styles.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='shopkeeper-fresco-css'
          href='https://www.academyclass.com/wp-content/themes/shopkeeper/css/fresco/fresco.css' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='shopkeeper-header-default-css'
          href='https://www.academyclass.com/wp-content/themes/shopkeeper/css/header-default.css' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='shopkeeper-default-style-css'
          href='https://www.academyclass.com/wp-content/themes/shopkeeper-child/style.css' type='text/css' media='all'/>
    <script type="text/template" id="tmpl-variation-template">
        <div
        class="woocommerce-variation-description">
        {{{ data.variation.variation_description }}}
        </div>

        <div class="woocommerce-variation-price">
            {{{ data.variation.price_html }}}
        </div>

        <div class="woocommerce-variation-availability">
            {{{ data.variation.availability_html }}}
        </div></script>
    <script type="text/template" id="tmpl-unavailable-variation-template">
        <p>Sorry, this product is unavailable. Please choose a different combination.</p></script>
    <script type='text/javascript' src='https://www.academyclass.com/wp-includes/js/jquery/jquery.js'></script>
    <script type='text/javascript'>/* <![CDATA[ */
        var wc_add_to_cart_params = {
            "ajax_url": "\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
            "i18n_view_cart": "View cart",
            "cart_url": "https:\/\/www.academyclass.com\/enquiry\/",
            "is_cart": "",
            "cart_redirect_after_add": "yes"
        };
        /* ]]> */</script>
    <script type='text/javascript'
            src='https://www.academyclass.com/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js'></script>
    <script type='text/javascript'
            src='https://www.academyclass.com/wp-content/plugins/js_composer/assets/js/vendors/woocommerce-add-to-cart.js'></script>
    <script type='text/javascript'>/* <![CDATA[ */
        var mailchimp_public_data = {
            "site_url": "https:\/\/www.academyclass.com",
            "ajax_url": "https:\/\/www.academyclass.com\/wp-admin\/admin-ajax.php"
        };
        /* ]]> */</script>
    <script type='text/javascript'
            src='https://www.academyclass.com/wp-content/plugins/mailchimp-for-woocommerce/public/js/mailchimp-woocommerce-public.min.js'></script>
    <script type='text/javascript' src='https://www.youtube.com/player_api'></script>
    <script type='text/javascript' src='https://f.vimeocdn.com/js/froogaloop2.min.js'></script>
    <script type='text/javascript' data-cfasync="false" id="omapi-script" async="async"
            src='https://a.optmstr.com/app/js/api.min.js'></script>
    <link rel='https://api.w.org/' href='https://www.academyclass.com/wp-json/'/>
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://www.academyclass.com/xmlrpc.php?rsd"/>
    <link rel="wlwmanifest" type="application/wlwmanifest+xml"
          href="https://www.academyclass.com/wp-includes/wlwmanifest.xml"/>
    <meta name="generator" content="WordPress 4.8.1"/>
    <meta name="generator" content="WooCommerce 3.1.1"/>
    <link rel='shortlink' href='https://www.academyclass.com/'/>
    <style type="text/css">.whyAC {
            background: rgb(150, 194, 236) !important;
            Font-weight: normal;
            padding: 5px 0 5px 0;
            line-height: 1% !important;
            font-size: 0.7em !important
        }

        .wpcf7-form-control.g-recaptcha.wpcf7-recaptcha {
            margin-left: 0 !important
        }

        .countertable {
            font-size: 12px !important
        }

        .headingwhite {
            color: #ffffff !important
        }

        #coupheading {
            font-size: 22px !important;
            text-transform: initial !important;
            color: #000000 !important;
            font-weight: bold !important;
            text-align: center !important;
            margin-top: 10px !important
        }

        #canyouattend {
            font-size: 17px !important;
            text-transform: initial !important;
            color: #000000 !important;
            font-weight: bold !important;
            text-align: center !important;
            margin-bottom: 10px !important
        }

        .widget h3 {
            margin: 0px !important;
            font-size: 20px !important
        }

        .widget h3, .wpb_wrapper .widget h3 {
            font-size: 20px !important
        }

        .headtext22 {
            font-size: 28px !important;
            color: #000000 !important;
            text-align: center !important;
            font-weight: bold !important
        }

        .widget a {
            font-weight: 400 !important
        }

        .courserowcopy {
            color: #ffffff !important
        }

        blockquote p {
            font-size: 1.475rem !important
        }

        .iconborder {
            border-right: .2px solid !important;
            border-color: #ffffff !important
        }

        .coursetime {
            border-right: .2px solid !important;
            border-color: #ffffff !important;
            font-size: 12px !important;
            height: 200px !important
        }

        .courseplace {
            border-right: .2px solid !important;
            border-color: #ffffff !important;
            height: 200px !important
        }

        ul.a {
            font-size: 13px !important;
            font-weight: lighter !important;
            line-height: 25px
        }

        ul.b {
            list-style: none;
            margin-left: -2px !important;
            line-height: 27px;
            list-style-position: inside;
            padding: 10px 0 10px 20px;
            text-indent: -1em
        }

        ul.b li:before {
            content: '✓';
            padding-right: 4px !important
        }

        .3
        darkrow {
            font-size: 13px !important;
            font-weight: lighter !important
        }

        .container-3 input#search_form {
            width: 230px;
            height: 48px;
            background: #ffffff !important
        }

        .greenbocktext {
            color: #ffffff !important
        }

        .boxtext {
            font-size: 16px !important;
            text-align: center !important;
            color: #1a989e !important
        }

        a.countdown_infolink {
            display: none !important
        }

        .widget {
            margin-bottom: 0px !important
        }

        .hasCountdown {
            padding: 0px !important;
            margin-top: 0px !important
        }

        .countertable {
            margin-top: -20px !important
        }

        div.greyrow {
            background: #2e3139;
            position: relative;
            margin: 0 auto
        }

        div.greyrow:after, div:before {
            height: 0;
            width: 0;
            content: "";
            display: block;
            position: relative
        }

        div.greyrow:before {
            border-bottom: 98px solid #2e3139;
            border-left: 22px solid transparent;
            left: -22px
        }

        p.percent {
            font-size: 45px;
            letter-spacing: 2px;
            word-spacing: 2px;
            color: #17989E;
            font-weight: 700;
            text-decoration: none;
            font-style: normal;
            font-variant: normal;
            text-transform: uppercase;
            text-align: center;
            text-shadow: rgb(255, 255, 255) 2px 0 0, rgb(255, 255, 255) 1.75517px .958851px 0, rgb(255, 255, 255) 1.0806px 1.68294px 0, rgb(255, 255, 255) .141474px 1.99499px 0, rgb(255, 255, 255) -.832294px 1.81859px 0, rgb(255, 255, 255) -1.60229px 1.19694px 0, rgb(255, 255, 255) -1.97998px .28224px 0, rgb(255, 255, 255) -1.87291px -.701566px 0, rgb(255, 255, 255) -1.30729px -1.5136px 0, rgb(255, 255, 255) -.421592px -1.95506px 0, rgb(255, 255, 255) .567324px -1.91785px 0, rgb(255, 255, 255) 1.41734px -1.41108px 0, rgb(255, 255, 255) 1.92034px -.558831px 0;
            font-weight: bold !important;
            margin-bottom: 0px !important
        }

        p.discount {
            font-size: 25px;
            letter-spacing: .3px;
            color: #000;
            font-weight: 700;
            text-decoration: none;
            font-style: normal;
            font-variant: normal;
            text-transform: uppercase;
            text-align: center;
            text-shadow: rgb(255, 255, 255) 2px 0 0, rgb(255, 255, 255) 1.75517px .958851px 0, rgb(255, 255, 255) 1.0806px 1.68294px 0, rgb(255, 255, 255) .141474px 1.99499px 0, rgb(255, 255, 255) -.832294px 1.81859px 0, rgb(255, 255, 255) -1.60229px 1.19694px 0, rgb(255, 255, 255) -1.97998px .28224px 0, rgb(255, 255, 255) -1.87291px -.701566px 0, rgb(255, 255, 255) -1.30729px -1.5136px 0, rgb(255, 255, 255) -.421592px -1.95506px 0, rgb(255, 255, 255) .567324px -1.91785px 0, rgb(255, 255, 255) 1.41734px -1.41108px 0, rgb(255, 255, 255) 1.92034px -.558831px 0;
            font-weight: bold !important;
            margin-bottom: 0px !important;
            margin-top: -15px !important
        }

        p.limited {
            font-size: 1.4em;
            font-weight: 400;
            text-decoration: none;
            font-style: normal;
            color: #000;
            font-variant: normal;
            text-align: center;
            margin-bottom: 0px !important
        }

        p.almost {
            font-size: 1.4em;
            font-weight: 400;
            text-decoration: none;
            color: #14989f;
            font-style: bold;
            font-variant: normal;
            text-align: center;
            margin-bottom: 0px !important
        }

        p.offerendtext {
            font-size: 22px;
            color: #ffffff !important;
            font-weight: 700;
            text-decoration: none;
            font-style: normal;
            font-variant: normal;
            text-align: center !important;
            margin-top: 10px !important;
            margin-bottom: 20px !important;
            text-transform: normal !important
        }

        p.book {
            font-size: 1.5em;
            line-height: 100% !important;
            font-weight: 700;
            text-decoration: none;
            font-style: normal;
            font-variant: normal;
            text-align: center !important;
            margin-top: 0px !important;
            margin-bottom: -10px !important
        }

        p.claim {
            font-size: .8em;
            line-height: 100% !important;
            font-weight: 700;
            text-decoration: none;
            font-style: normal;
            font-variant: normal;
            text-align: center !important;
            margin-top: 10px !important;
            margin-bottom: -10px !important
        }

        div.buttonbook {
            text-align: center;
            color: #fff;
            background-color: #17989e;
            -moz-border-radius: 2px;
            -webkit-border-radius: 2px;
            border-radius: 2px;
            padding-top: 10px !important;
            padding-bottom: 10px !important;
            height: 50px !important;
            width: 70% !important;
            height: 60px !important
        }

        div.buttonbook a {
            color: #fff;
            text-align: center;
            background-color: #c2ce53
        }

        .bird {
            top: -10px;
            z-index: 1000 !important;
            margin-left: -35px !important;
            margin-top: 5px !important
        }

        p.special {
            color: #fff;
            -webkit-transform: rotate(344deg);
            -moz-transform: rotate(344deg);
            -ms-transform: rotate(344deg);
            -o-transform: rotate(344deg);
            transform: rotate(344deg);
            line-height: 90% !important;
            text-align: center !important;
            font-size: 18px;
            font-weight: normal !important;
            margin-bottom: 0px !important;
            margin-top: -35px !important
        }

        p.specialprice {
            color: #fff;
            -webkit-transform: rotate(344deg);
            -moz-transform: rotate(344deg);
            -ms-transform: rotate(344deg);
            -o-transform: rotate(344deg);
            transform: rotate(344deg);
            line-height: 90% !important;
            text-align: center !important;
            font-size: 25px;
            font-weight: bold !important;
            margin-top: 0px !important
        }

        .reddot {
            height: 80px;
            width: 80px;
            background-color: #ae1b1b;
            border-radius: 100%;
            display: inline-block
        }

        @media only screen and (max-width: 972px) {
            p.offerend {
                font-size: 18px
            }
        }

        @media only screen and (max-width: 921px) {
            p.offerend {
                font-size: 18px
            }

            p.almost {
                font-size: 1.3em !important
            }
        }

        @media only screen and (max-width: 918px) {
            p.offerendtext {
                font-size: 17px
            }

            p.limited {
                font-size: 1.2em !important
            }

            p.almost {
                font-size: 1.1em !important
            }
        }

        @media only screen and (max-width: 898px) {
            p.limited {
                font-size: 1em !important
            }

            p.almost {
                font-size: 1em !important
            }
        }

        @media only screen and (max-width: 883px) {
            p.offerendtext {
                font-size: 16px
            }

            p.limited {
                font-size: 1.1em !important
            }

            p.almost {
                font-size: 1.2em !important
            }

            p.discount {
                font-size: 20px;
                padding-bottom: 5px !important
            }
        }

        @media only screen and (max-width: 850px) {
            p.offerendtext {
                font-size: 18px
            }

            p.percent {
                font-size: 35px
            }

            p.discount {
                font-size: 20px
            }

            p.limited {
                font-size: 1em !important;
                margin-top: 3px !important
            }

            p.almost {
                font-size: 1em !important
            }

            p.book {
                font-size: 1.2em
            }

            p.claim {
                font-size: .6em
            }

            div.buttonbook {
                height: 50px !important
            }

            div.greyrow:before {
                border-bottom: 90px solid #2e3139 !important
            }
        }

        @media only screen and (max-width: 814px) {
            p.offerendtext {
                font-size: 14px !important
            }
        }

        @media only screen and (max-width: 812px) {
            p.limited {
                font-size: 1.1em !important
            }

            p.almost {
                font-size: 1em !important
            }
        }

        @media only screen and (max-width: 810px) {
            p.limited {
                font-size: 1.1em !important
            }

            p.almost {
                font-size: 1em !important
            }
        }

        @media only screen and (max-width: 808px) {
            p.limited {
                font-size: 1.1em !important
            }

            p.almost {
                font-size: 1em !important
            }
        }

        @media only screen and (max-width: 780px) {
            p.offerendtext {
                font-size: 19px !important
            }

            p.percent {
                font-size: 45px
            }

            p.discount {
                font-size: 30px
            }

            p.limited {
                font-size: 1.6em !important
            }

            p.almost {
                font-size: 1.5em !important
            }

            p.book {
                font-size: 1.4em
            }

            p.claim {
                font-size: 1em
            }

            div.buttonbook {
                width: 80% !important;
                height: 60px !important
            }

            .reddot {
                float: right !important
            }
        }

        @media only screen and (max-width: 500px) {
            .reddot {
                float: right !important
            }
        }

        @media only screen and (max-width: 300px) {
            p.percent {
                font-size: 35px
            }

            p.discount {
                font-size: 25px
            }
        }</style>
    <script>window.dzsvg_swfpath = "https://www.academyclass.com/wp-content/plugins/dzs-videogallery/preview.swf";</script>
    <script>!function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1417846668241961');
        fbq('track', 'PageView');</script>
    <noscript><img height="1" width="1"
                   src="https://www.facebook.com/tr?id=1417846668241961&ev=PageView
&noscript=1"/></noscript>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-904326-7"></script>
    <script>window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-904326-7');</script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-934212749"></script>
    <script>window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'AW-934212749');</script>
    <script>gtag('config', 'AW-934212749/VAgiCKfalmYQjem7vQM', {
            'phone_conversion_number': '08000438889'
        });</script>
    <script type='text/javascript'>window.__lo_site_id = 106639;
        (function () {
            var wa = document.createElement('script');
            wa.type = 'text/javascript';
            wa.async = true;
            wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wa, s);
        })();</script>
    <style type="text/css">.learnbox {
            font-size: 10px !important
        }

        .courserowcopy {
            color: #ffffff !important
        }

        .coursetime {
            border-right: .2px solid !important;
            border-color: #ffffff !important;
            font-size: 12px !important;
            height: 200px !important
        }

        .courseplace {
            border-right: .2px solid !important;
            border-color: #ffffff !important;
            height: 200px !important
        }

        ul.a {
            font-size: 13px !important;
            font-weight: lighter !important;
            line-height: 25px
        }

        ul.b {
            list-style: none;
            margin-left: -2px !important;
            line-height: 27px;
            list-style-position: inside;
            padding: 10px 0 10px 20px;
            text-indent: -1em
        }

        ul.b li:before {
            content: '✓';
            padding-right: 4px !important
        }

        text-indent:

        -
        1
        em

        ;
        padding-left:

        1
        em

        ;
        .3
        darkrow {
            font-size: 13px !important;
            font-weight: lighter !important
        }

        .container-3 input#search_form {
            width: 230px;
            height: 53px;
            background: #ffffff !important;
            margin-top: -33px !important;
            padding-left: 25px !important;
            border-radius: 0px !important
        }

        .greenbocktext {
            color: #ffffff !important
        }

        select.wpcf7-form-control, input.wpcf7-form-control, textarea.wpcf7-form-control {
            background-color: #ffffff !important;
            border-color: #5a5a5a !important
        }

        input[type="text"], input[type="password"], input[type="date"], input[type="datetime"], input[type="datetime-local"], input[type="month"], input[type="week"], input[type="email"], input[type="number"], input[type="search"], input[type="tel"], input[type="time"], input[type="url"], textarea, select, .chosen-container-single .chosen-single, .country_select.select2-container, .woocommerce form .form-row.woocommerce-validated .select2-container, .woocommerce form .form-row.woocommerce-validated input.input-text, .woocommerce form .form-row.woocommerce-validated select, .woocommerce form .form-row.woocommerce-invalid .select2-container, .woocommerce form .form-row.woocommerce-invalid input.input-text, .woocommerce form .form-row.woocommerce-invalid select, .country_select.select2-container, .state_select.select2-container, #coupon_code {
            border-color: #5a5a5a !important
        }

        input.wpcf7-form-control.wpcf7-submit {
            color: white;
            background-color: #e8341a !important
        }

        h6 {
            font-size: 11px !important;
            margin-top: 30px !important;
            font-weight: 200 !important;
            padding-top: 30px !important
        }

        h3 {
            font-size: 1.675rem;
            line-height: 1.2em
        }

        h1, h2, h3, h4, h5, h6 {
            font-weight: 500 !important
        }

        .headingwhite {
            color: #ffffff !important
        }

        .fourth_row a {
            background-color: #e8341a !important;
            color: #ffffff !important
        }

        @media screen and (max-width: 1000px) {
            .trainingbox {
                width: 100% !important
            }

            .learnbox {
                font-size: 8px !important
            }
        }

        @media screen and (max-width: 800px) {
            .learnbox {
                width: 100% !important;
                text-align: center !important
            }
        }</style>
    <meta name="referrer" content="always"/>
    <script type="text/javascript">var google_replace_number = "0800 043 8889";
        (function (a, e, c, f, g, b, d) {
            var h = {ak: "934212749", cl: "VAgiCKfalmYQjem7vQM"};
            a[c] = a[c] || function () {
                (a[c].q = a[c].q || []).push(arguments)
            };
            a[f] || (a[f] = h.ak);
            b = e.createElement(g);
            b.async = 1;
            b.src = "//www.gstatic.com/wcm/loader.js";
            d = e.getElementsByTagName(g)[0];
            d.parentNode.insertBefore(b, d);
            a._googWcmGet = function (b, d, e) {
                a[c](2, b, h, d, null, new Date, e)
            }
        })(window, document, "_googWcmImpl", "_googWcmAk", "script");</script>
    <script type="text/javascript">var shopkeeper_ajaxurl = '/wp-admin/admin-ajax.php';</script>
    <noscript>
        <style>.woocommerce-product-gallery {
                opacity: 1 !important
            }</style>
    </noscript>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-904326-7"></script>
    <script>window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag("js", new Date());
        gtag("config", "UA-904326-7", {"cookie_domain": "auto"});</script>
    <meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css"
          href="https://www.academyclass.com/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css"
          media="screen"><![endif]--><!--[if IE  8]>
    <link rel="stylesheet" type="text/css"
          href="https://www.academyclass.com/wp-content/plugins/js_composer/assets/css/vc-ie8.min.css" media="screen">
    <![endif]-->
    <style>.st-content {
            background-color: #fff;
            background-repeat:;
            background-position:;
            background-size:;
            background-attachment:
        }

        h1, h2, h3, h4, h5, h6, .comments-title, .comment-author, #reply-title, #site-footer .widget-title, .accordion_title, .ui-tabs-anchor, .products .button, .site-title a, .post_meta_archive a, .post_meta a, .post_tags a, #nav-below a, .list_categories a, .list_shop_categories a, .main-navigation > ul > li > a, .main-navigation .mega-menu > ul > li > a, .more-link, .top-page-excerpt, .select2-search input, .product_after_shop_loop_buttons a, .woocommerce .products-grid a.button, .page-numbers, input.qty, .button, button, .button_text, input[type="button"], input[type="reset"], input[type="submit"], .woocommerce a.button, .woocommerce-page a.button, .woocommerce button.button, .woocommerce-page button.button, .woocommerce input.button, .woocommerce-page input.button, .woocommerce #respond input#submit, .woocommerce-page #respond input#submit, .woocommerce #content input.button, .woocommerce-page #content input.button, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, .woocommerce #respond input#submit.alt, .woocommerce #content input.button.alt, .woocommerce-page a.button.alt, .woocommerce-page button.button.alt, .woocommerce-page input.button.alt, .woocommerce-page #respond input#submit.alt, .woocommerce-page #content input.button.alt, .yith-wcwl-wishlistexistsbrowse.show a, .share-product-text, .tabs > li > a, label, .comment-respond label, .product_meta_title, .woocommerce table.shop_table th, .woocommerce-page table.shop_table th, #map_button, .coupon_code_text, .woocommerce .cart-collaterals .cart_totals tr.order-total td strong, .woocommerce-page .cart-collaterals .cart_totals tr.order-total td strong, .cart-wishlist-empty, .return-to-shop .wc-backward, .order-number a, .account_view_link, .post-edit-link, .from_the_blog_title, .icon_box_read_more, .vc_pie_chart_value, .shortcode_banner_simple_bullet, .shortcode_banner_simple_height_bullet, .category_name, .woocommerce span.onsale, .woocommerce-page span.onsale, .out_of_stock_badge_single, .out_of_stock_badge_loop, .page-numbers, .page-links, .add_to_wishlist, .yith-wcwl-wishlistaddedbrowse, .yith-wcwl-wishlistexistsbrowse, .filters-group, .product-name, .woocommerce-page .my_account_container table.shop_table.order_details_footer tr:last-child td:last-child .amount, .customer_details dt, .widget h3, .widget ul a, .widget a, .widget .total .amount, .wishlist-in-stock, .wishlist-out-of-stock, .comment-reply-link, .comment-edit-link, .widget_calendar table thead tr th, .page-type, .mobile-navigation a, table thead tr th, .portfolio_single_list_cat, .portfolio-categories, .shipping-calculator-button, .vc_btn, .vc_btn2, .vc_btn3, .offcanvas-menu-button .menu-button-text, .account-tab-item .account-tab-link, .account-tab-list .sep, ul.order_details li span, ul.order_details.bacs_details li, .widget_calendar caption, .widget_recent_comments li a, .edit-account legend, .widget_shopping_cart li.empty, .cart-collaterals .cart_totals .shop_table .order-total .woocommerce-Price-amount, .woocommerce table.cart .cart_item td a, .woocommerce #content table.cart .cart_item td a, .woocommerce-page table.cart .cart_item td a, .woocommerce-page #content table.cart .cart_item td a, .woocommerce table.cart .cart_item td span, .woocommerce #content table.cart .cart_item td span, .woocommerce-page table.cart .cart_item td span, .woocommerce-page #content table.cart .cart_item td span, .woocommerce-MyAccount-navigation ul li {
            font-family: 'Montserrat', sans-serif
        }

        body, p, #site-navigation-top-bar, .site-title, .widget_product_search #searchsubmit, .widget_search #searchsubmit, .widget_product_search .search-submit, .widget_search .search-submit, #site-menu, .copyright_text, blockquote cite, table thead th, .recently_viewed_in_single h2, .woocommerce .cart-collaterals .cart_totals table th, .woocommerce-page .cart-collaterals .cart_totals table th, .woocommerce .cart-collaterals .shipping_calculator h2, .woocommerce-page .cart-collaterals .shipping_calculator h2, .woocommerce table.woocommerce-checkout-review-order-table tfoot th, .woocommerce-page table.woocommerce-checkout-review-order-table tfoot th, .qty, .shortcode_banner_simple_inside h4, .shortcode_banner_simple_height h4, .fr-caption, .post_meta_archive, .post_meta, .page-links-title, .yith-wcwl-wishlistaddedbrowse .feedback, .yith-wcwl-wishlistexistsbrowse .feedback, .product-name span, .widget_calendar table tbody a, .fr-touch-caption-wrapper, .woocommerce .login-register-container p.form-row.remember-me-row label, .woocommerce .checkout_login p.form-row label[for="rememberme"], .woocommerce .checkout_login p.lost_password, .form-row.remember-me-row a, .wpb_widgetised_column aside ul li span.count, .woocommerce td.product-name dl.variation dt, .woocommerce td.product-name dl.variation dd, .woocommerce td.product-name dl.variation dt p, .woocommerce td.product-name dl.variation dd p, .woocommerce-page td.product-name dl.variation dt, .woocommerce-page td.product-name dl.variation dd p, .woocommerce-page td.product-name dl.variation dt p, .woocommerce-page td.product-name dl.variation dd p, .woocommerce span.amount, .woocommerce ul#shipping_method label, .woocommerce .select2-container, .check_label, .woocommerce-page #payment .terms label, ul.order_details li strong, .woocommerce-order-received .woocommerce table.shop_table tfoot th, .woocommerce-order-received .woocommerce-page table.shop_table tfoot th, .woocommerce-view-order .woocommerce table.shop_table tfoot th, .woocommerce-view-order .woocommerce-page table.shop_table tfoot th, .widget_recent_comments li, .widget_shopping_cart p.total, .widget_shopping_cart p.total .amount, .mobile-navigation li ul li a, .woocommerce table.cart .cart_item td:before, .woocommerce #content table.cart .cart_item td:before, .woocommerce-page table.cart .cart_item td:before, .woocommerce-page #content table.cart .cart_item td:before {
            font-family: 'Karla', sans-serif
        }

        body, table tr th, table tr td, table thead tr th, blockquote p, label, .woocommerce .woocommerce-breadcrumb a, .woocommerce-page .woocommerce-breadcrumb a, .select2-dropdown-open.select2-drop-above .select2-choice, .select2-dropdown-open.select2-drop-above .select2-choices, .select2-container .select2-choice, .select2-container, .big-select, .select.big-select, .list-centered li a, .post_meta_archive a, .post_meta a, .nav-next a, .nav-previous a, .blog-single h6, .page-description, .woocommerce #content nav.woocommerce-pagination ul li a:focus, .woocommerce #content nav.woocommerce-pagination ul li a:hover, .woocommerce #content nav.woocommerce-pagination ul li span.current, .woocommerce nav.woocommerce-pagination ul li a:focus, .woocommerce nav.woocommerce-pagination ul li a:hover, .woocommerce nav.woocommerce-pagination ul li span.current, .woocommerce-page #content nav.woocommerce-pagination ul li a:focus, .woocommerce-page #content nav.woocommerce-pagination ul li a:hover, .woocommerce-page #content nav.woocommerce-pagination ul li span.current, .woocommerce-page nav.woocommerce-pagination ul li a:focus, .woocommerce-page nav.woocommerce-pagination ul li a:hover, .woocommerce-page nav.woocommerce-pagination ul li span.current, .woocommerce table.shop_table th, .woocommerce-page table.shop_table th, .woocommerce .cart-collaterals .cart_totals h2, .woocommerce .cart-collaterals .cross-sells h2, .woocommerce-page .cart-collaterals .cart_totals h2, .woocommerce .cart-collaterals .cart_totals table tr.order-total td:last-child, .woocommerce-page .cart-collaterals .cart_totals table tr.order-total td:last-child, .woocommerce-checkout .woocommerce-info, .woocommerce-checkout h3, .woocommerce-checkout h2, .woocommerce-account h2, .woocommerce-account h3, .customer_details dt, .wpb_widgetised_column .widget a, .wpb_widgetised_column .widget.widget_product_categories a:hover, .wpb_widgetised_column .widget.widget_layered_nav a:hover, .wpb_widgetised_column .widget.widget_layered_nav li, .portfolio_single_list_cat a, .gallery-caption-trigger, .woocommerce .widget_layered_nav ul li.chosen a, .woocommerce-page .widget_layered_nav ul li.chosen a, .widget_layered_nav ul li.chosen a, .woocommerce .widget_product_categories ul li.current-cat > a, .woocommerce-page .widget_product_categories ul li.current-cat > a, .widget_product_categories ul li.current-cat > a, .wpb_widgetised_column .widget.widget_layered_nav_filters a, .woocommerce-cart .cart-collaterals .cart_totals table .order-total td .amount, .widget_shopping_cart p.total, .widget_shopping_cart p.total .amount, .wpb_widgetised_column .widget_shopping_cart li.empty {
            color: #8a8b8c
        }

        .woocommerce a.remove {
            color: #8a8b8c !important
        }

        .nav-previous-title, .nav-next-title, .post_tags a, .wpb_widgetised_column .tagcloud a, .products .add_to_wishlist:before {
            color: rgba(138, 139, 140, .4)
        }

        .required {
            color: rgba(138, 139, 140, .4) !important
        }

        .yith-wcwl-add-button, .yith-wcwl-wishlistaddedbrowse, .yith-wcwl-wishlistexistsbrowse, .share-product-text, .product_meta .sku, .product_meta a, .product_meta_separator, .woocommerce table.shop_attributes td, .woocommerce-page table.shop_attributes td, .woocommerce .woocommerce-breadcrumb, .woocommerce-page .woocommerce-breadcrumb, .tob_bar_shop, .post_meta_archive, .post_meta, del, .woocommerce .cart-collaterals .cart_totals table tr td:last-child, .woocommerce-page .cart-collaterals .cart_totals table tr td:last-child, .product-name .product-quantity, .woocommerce #payment div.payment_box, .wpb_widgetised_column .widget li, .wpb_widgetised_column .widget_calendar table thead tr th, .wpb_widgetised_column .widget_calendar table thead tr td, .wpb_widgetised_column .widget .post-date, .wpb_widgetised_column .recentcomments, .wpb_widgetised_column .amount, .wpb_widgetised_column .quantity, .products li:hover .add_to_wishlist:before, .product_after_shop_loop .price, .product_after_shop_loop .price ins, .wpb_wrapper .add_to_cart_inline del, .wpb_widgetised_column .widget_price_filter .price_slider_amount, .woocommerce td.product-name dl.variation dt, .woocommerce td.product-name dl.variation dd, .woocommerce td.product-name dl.variation dt p, .woocommerce td.product-name dl.variation dd p, .woocommerce-page td.product-name dl.variation dt, .woocommerce-page td.product-name dl.variation dd p, .woocommerce-page td.product-name dl.variation dt p, .woocommerce-page td.product-name dl.variation dd p {
            color: rgba(138, 139, 140, .55)
        }

        .products a.button.add_to_cart_button.loading {
            color: rgba(138, 139, 140, .55) !important
        }

        .add_to_cart_inline .amount, .wpb_widgetised_column .widget, .wpb_widgetised_column .widget a:hover, .wpb_widgetised_column .widget.widget_product_categories a, .wpb_widgetised_column .widget.widget_layered_nav a, .widget_layered_nav ul li a, .widget_layered_nav, .wpb_widgetised_column aside ul li span.count, .shop_table.cart .product-price .amount, .woocommerce-cart .cart-collaterals .cart_totals table th, .woocommerce-cart .cart-collaterals .cart_totals table td, .woocommerce-cart .cart-collaterals .cart_totals table td .amount, .woocommerce ul#shipping_method label {
            color: rgba(138, 139, 140, .8)
        }

        input[type="text"], input[type="password"], input[type="date"], input[type="datetime"], input[type="datetime-local"], input[type="month"], input[type="week"], input[type="email"], input[type="number"], input[type="search"], input[type="tel"], input[type="time"], input[type="url"], textarea, select, .chosen-container-single .chosen-single, .country_select.select2-container, .woocommerce form .form-row.woocommerce-validated .select2-container, .woocommerce form .form-row.woocommerce-validated input.input-text, .woocommerce form .form-row.woocommerce-validated select, .woocommerce form .form-row.woocommerce-invalid .select2-container, .woocommerce form .form-row.woocommerce-invalid input.input-text, .woocommerce form .form-row.woocommerce-invalid select, .country_select.select2-container, .state_select.select2-container, #coupon_code {
            border-color: rgba(138, 139, 140, .1)
        }

        input[type="text"]:focus, input[type="password"]:focus, input[type="date"]:focus, input[type="datetime"]:focus, input[type="datetime-local"]:focus, input[type="month"]:focus, input[type="week"]:focus, input[type="email"]:focus, input[type="number"]:focus, input[type="search"]:focus, input[type="tel"]:focus, input[type="time"]:focus, input[type="url"]:focus, textarea:focus, select:focus, #coupon_code:focus, .chosen-container-single .chosen-single:focus, .woocommerce .product_infos .quantity input.qty, .woocommerce #content .product_infos .quantity input.qty, .woocommerce-page .product_infos .quantity input.qty, .woocommerce-page #content .product_infos .quantity input.qty, .post_tags a, .wpb_widgetised_column .tagcloud a, .coupon_code_wrapper, .woocommerce form.checkout_coupon, .woocommerce-page form.checkout_coupon, .woocommerce ul.digital-downloads:before, .woocommerce-page ul.digital-downloads:before, .woocommerce ul.digital-downloads li:after, .woocommerce-page ul.digital-downloads li:after, .widget_search .search-form, .woocommerce .widget_layered_nav ul li a:before, .woocommerce-page .widget_layered_nav ul li a:before, .widget_layered_nav ul li a:before, .woocommerce .widget_product_categories ul li a:before, .woocommerce-page .widget_product_categories ul li a:before, .widget_product_categories ul li a:before, .woocommerce-cart.woocommerce-page #content .quantity input.qty {
            border-color: rgba(138, 139, 140, .15)
        }

        .list-centered li a, .woocommerce .cart-collaterals .cart_totals h2, .woocommerce .cart-collaterals .cross-sells h2, .woocommerce-page .cart-collaterals .cart_totals h2, .my_address_title, .woocommerce .shop_table.order_details tbody tr:last-child td, .woocommerce-page .shop_table.order_details tbody tr:last-child td, .woocommerce #payment ul.payment_methods li, .woocommerce-page #payment ul.payment_methods li, .comment-separator, .comment-list .pingback, .wpb_widgetised_column .widget, .search_result_item, .woocommerce div.product .woocommerce-tabs ul.tabs li:after, .woocommerce #content div.product .woocommerce-tabs ul.tabs li:after, .woocommerce-page div.product .woocommerce-tabs ul.tabs li:after, .woocommerce-page #content div.product .woocommerce-tabs ul.tabs li:after, .woocommerce .cart-collaterals .cart_totals .order-total td, .woocommerce .cart-collaterals .cart_totals .order-total th, .woocommerce-page .cart-collaterals .cart_totals .order-total td, .woocommerce-page .cart-collaterals .cart_totals .order-total th {
            border-bottom-color: rgba(138, 139, 140, .15)
        }

        table tr td, .woocommerce table.shop_table td, .woocommerce-page table.shop_table td, .product_socials_wrapper, .woocommerce-tabs, .comments_section, .portfolio_content_nav #nav-below, .woocommerce .cart-collaterals .cart_totals .order-total td, .woocommerce .cart-collaterals .cart_totals .order-total th, .woocommerce-page .cart-collaterals .cart_totals .order-total td, .woocommerce-page .cart-collaterals .cart_totals .order-total th {
            border-top-color: rgba(138, 139, 140, .15)
        }

        table.shop_attributes tr td, .wishlist_table tr td, .shop_table.cart tr td {
            border-bottom-color: rgba(138, 139, 140, .1)
        }

        .product_meta, .woocommerce .cart-collaterals, .woocommerce-page .cart-collaterals, .checkout_right_wrapper, .track_order_form, .order-info {
            background: rgba(138, 139, 140, .05)
        }

        .woocommerce-cart .cart-collaterals:before, .woocommerce-cart .cart-collaterals:after, .custom_border:before, .custom_border:after {
            background-image: radial-gradient(closest-side, transparent 9px, rgba(138, 139, 140, .05) 100%)
        }

        .actions .button, .actions .coupon .button {
            background: rgba(138, 139, 140, .55) !important
        }

        .actions .button:hover, .actions .coupon .button:hover {
            background: rgba(138, 139, 140, .44) !important
        }

        .wpb_widgetised_column aside ul li span.count {
            background: rgba(138, 139, 140, .05)
        }

        .comments_section {
            background-color: rgba(138, 139, 140, .01) !important
        }

        h1, h2, h3, h4, h5, h6, .entry-title-archive a, .woocommerce #content div.product .woocommerce-tabs ul.tabs li.active a, .woocommerce div.product .woocommerce-tabs ul.tabs li.active a, .woocommerce-page #content div.product .woocommerce-tabs ul.tabs li.active a, .woocommerce-page div.product .woocommerce-tabs ul.tabs li.active a, .woocommerce #content div.product .woocommerce-tabs ul.tabs li.active a:hover, .woocommerce div.product .woocommerce-tabs ul.tabs li.active a:hover, .woocommerce-page #content div.product .woocommerce-tabs ul.tabs li.active a:hover, .woocommerce-page div.product .woocommerce-tabs ul.tabs li.active a:hover, .woocommerce table.cart .product-name a, .product-title-link, .wpb_widgetised_column .widget .product_list_widget a {
            color: #202329
        }

        .woocommerce div.product .woocommerce-tabs ul.tabs li a, .woocommerce #content div.product .woocommerce-tabs ul.tabs li a, .woocommerce-page div.product .woocommerce-tabs ul.tabs li a, .woocommerce-page #content div.product .woocommerce-tabs ul.tabs li a {
            color: rgba(32, 35, 41, .35)
        }

        .woocommerce #content div.product .woocommerce-tabs ul.tabs li a:hover, .woocommerce div.product .woocommerce-tabs ul.tabs li a:hover, .woocommerce-page #content div.product .woocommerce-tabs ul.tabs li a:hover, .woocommerce-page div.product .woocommerce-tabs ul.tabs li a:hover {
            color: rgba(32, 35, 41, .45)
        }

        .page-title:after {
            background: #202329
        }

        a, .comments-area a, .edit-link, .post_meta_archive a:hover, .post_meta a:hover, .entry-title-archive a:hover, blockquote:before, .no-results-text:before, .list-centered a:hover, .comment-reply i, .comment-edit-link i, .comment-edit-link, .filters-group li:hover, #map_button, .widget_shopkeeper_social_media a, .account-tab-link-mobile, .lost-reset-pass-text:before, .list_shop_categories a:hover, .add_to_wishlist:hover, .woocommerce div.product span.price, .woocommerce-page div.product span.price, .woocommerce #content div.product span.price, .woocommerce-page #content div.product span.price, .woocommerce div.product p.price, .woocommerce-page div.product p.price, .woocommerce #content div.product p.price, .woocommerce-page #content div.product p.price, .comment-metadata time, .woocommerce p.stars a.star-1.active:after, .woocommerce p.stars a.star-1:hover:after, .woocommerce-page p.stars a.star-1.active:after, .woocommerce-page p.stars a.star-1:hover:after, .woocommerce p.stars a.star-2.active:after, .woocommerce p.stars a.star-2:hover:after, .woocommerce-page p.stars a.star-2.active:after, .woocommerce-page p.stars a.star-2:hover:after, .woocommerce p.stars a.star-3.active:after, .woocommerce p.stars a.star-3:hover:after, .woocommerce-page p.stars a.star-3.active:after, .woocommerce-page p.stars a.star-3:hover:after, .woocommerce p.stars a.star-4.active:after, .woocommerce p.stars a.star-4:hover:after, .woocommerce-page p.stars a.star-4.active:after, .woocommerce-page p.stars a.star-4:hover:after, .woocommerce p.stars a.star-5.active:after, .woocommerce p.stars a.star-5:hover:after, .woocommerce-page p.stars a.star-5.active:after, .woocommerce-page p.stars a.star-5:hover:after, .yith-wcwl-add-button:before, .yith-wcwl-wishlistaddedbrowse .feedback:before, .yith-wcwl-wishlistexistsbrowse .feedback:before, .woocommerce .star-rating span:before, .woocommerce-page .star-rating span:before, .product_meta a:hover, .woocommerce .shop-has-sidebar .no-products-info .woocommerce-info:before, .woocommerce-page .shop-has-sidebar .no-products-info .woocommerce-info:before, .woocommerce .woocommerce-breadcrumb a:hover, .woocommerce-page .woocommerce-breadcrumb a:hover, .intro-effect-fadeout.modify .post_meta a:hover, .from_the_blog_link:hover .from_the_blog_title, .portfolio_single_list_cat a:hover, .widget .recentcomments:before, .widget.widget_recent_entries ul li:before, #placeholder_product_quick_view .product_title:hover, .wpb_widgetised_column aside ul li.current-cat > span.count {
            color: #bfd432
        }

        @media only screen and (min-width: 40.063em) {
            .nav-next a:hover, .nav-previous a:hover {
                color: #bfd432
            }
        }

        .widget_shopping_cart .buttons a.view_cart, .widget.widget_price_filter .price_slider_amount .button, .products a.button, .woocommerce .products .added_to_cart.wc-forward, .woocommerce-page .products .added_to_cart.wc-forward {
            color: #bfd432 !important
        }

        .order-info mark, .login_footer, .post_tags a:hover, .with_thumb_icon, .wpb_wrapper .wpb_toggle:before, #content .wpb_wrapper h4.wpb_toggle:before, .wpb_wrapper .wpb_accordion .wpb_accordion_wrapper .ui-state-default .ui-icon, .wpb_wrapper .wpb_accordion .wpb_accordion_wrapper .ui-state-active .ui-icon, .widget .tagcloud a:hover, .single_product_summary_related h2:after, .single_product_summary_upsell h2:after, .page-title.portfolio_item_title:after, #button_offcanvas_sidebar_left, #button_offcanvas_sidebar_left i, .thumbnail_archive_container:before, .from_the_blog_overlay, .select2-results .select2-highlighted, .wpb_widgetised_column aside ul li.chosen span.count, .woocommerce .widget_product_categories ul li.current-cat > a:before, .woocommerce-page .widget_product_categories ul li.current-cat > a:before, .widget_product_categories ul li.current-cat > a:before {
            background: #bfd432
        }

        @media only screen and (max-width: 40.063em) {
            .nav-next a:hover, .nav-previous a:hover {
                background: #bfd432
            }
        }

        .woocommerce .widget_layered_nav ul li.chosen a:before, .woocommerce-page .widget_layered_nav ul li.chosen a:before, .widget_layered_nav ul li.chosen a:before, .woocommerce .widget_layered_nav ul li.chosen:hover a:before, .woocommerce-page .widget_layered_nav ul li.chosen:hover a:before, .widget_layered_nav ul li.chosen:hover a:before, .woocommerce .widget_layered_nav_filters ul li a:before, .woocommerce-page .widget_layered_nav_filters ul li a:before, .widget_layered_nav_filters ul li a:before, .woocommerce .widget_layered_nav_filters ul li a:hover:before, .woocommerce-page .widget_layered_nav_filters ul li a:hover:before, .widget_layered_nav_filters ul li a:hover:before, .woocommerce .widget_rating_filter ul li.chosen a:before {
            background-color: #bfd432
        }

        .woocommerce .widget_price_filter .ui-slider .ui-slider-range, .woocommerce-page .widget_price_filter .ui-slider .ui-slider-range, .woocommerce .quantity .plus, .woocommerce .quantity .minus, .woocommerce #content .quantity .plus, .woocommerce #content .quantity .minus, .woocommerce-page .quantity .plus, .woocommerce-page .quantity .minus, .woocommerce-page #content .quantity .plus, .woocommerce-page #content .quantity .minus, .widget_shopping_cart .buttons .button.wc-forward.checkout {
            background: #bfd432 !important
        }

        .button, input[type="button"], input[type="reset"], input[type="submit"] {
            background-color: #bfd432 !important
        }

        .product_infos .yith-wcwl-wishlistaddedbrowse a:hover, .product_infos .yith-wcwl-wishlistexistsbrowse a:hover, .shipping-calculator-button:hover, .products a.button:hover, .woocommerce .products .added_to_cart.wc-forward:hover, .woocommerce-page .products .added_to_cart.wc-forward:hover, .products .yith-wcwl-wishlistexistsbrowse:hover a, .products .yith-wcwl-wishlistaddedbrowse:hover a, .order-number a:hover, .account_view_link:hover, .post-edit-link:hover, .url:hover {
            color: rgba(191, 212, 50, .8) !important
        }

        .product-title-link:hover {
            color: rgba(32, 35, 41, .8)
        }

        .button:hover, input[type="button"]:hover, input[type="reset"]:hover, input[type="submit"]:hover, .woocommerce .product_infos .quantity .minus:hover, .woocommerce #content .product_infos .quantity .minus:hover, .woocommerce-page .product_infos .quantity .minus:hover, .woocommerce-page #content .product_infos .quantity .minus:hover, .woocommerce .quantity .plus:hover, .woocommerce #content .quantity .plus:hover, .woocommerce-page .quantity .plus:hover, .woocommerce-page #content .quantity .plus:hover {
            background: rgba(191, 212, 50, .8) !important
        }

        .post_tags a:hover, .widget .tagcloud a:hover, .widget_shopping_cart .buttons a.view_cart, .account-tab-link-mobile, .woocommerce .widget_price_filter .ui-slider .ui-slider-handle, .woocommerce-page .widget_price_filter .ui-slider .ui-slider-handle, .woocommerce .widget_product_categories ul li.current-cat > a:before, .woocommerce-page .widget_product_categories ul li.current-cat > a:before, .widget_product_categories ul li.current-cat > a:before, .widget_product_categories ul li a:hover:before, .widget_layered_nav ul li a:hover:before, .widget_product_categories ul li a:hover ~ .count, .widget_layered_nav ul li a:hover ~ .count, .cd-top {
            border-color: #bfd432
        }

        .wpb_tour.wpb_content_element .wpb_tabs_nav li.ui-tabs-active a, .wpb_tabs.wpb_content_element .wpb_tabs_nav li.ui-tabs-active a, .woocommerce div.product .woocommerce-tabs ul.tabs li.active a, .woocommerce #content div.product .woocommerce-tabs ul.tabs li.active a, .woocommerce-page div.product .woocommerce-tabs ul.tabs li.active a, .woocommerce-page #content div.product .woocommerce-tabs ul.tabs li.active a, .main-navigation ul ul li a:hover {
            border-bottom-color: #bfd432
        }

        .woocommerce div.product .woocommerce-tabs ul.tabs li.active, .woocommerce #content div.product .woocommerce-tabs ul.tabs li.active, .woocommerce-page div.product .woocommerce-tabs ul.tabs li.active, .woocommerce-page #content div.product .woocommerce-tabs ul.tabs li.active {
            border-top-color: #bfd432
        }

        #site-top-bar {
            height: 43px
        }

        #site-top-bar, #site-navigation-top-bar .sf-menu ul {
            background: #202329
        }

        #site-top-bar, #site-top-bar a {
            color: #a5abaf
        }

        .site-header {
            background: #2c3139
        }

        @media only screen and (min-width: 63.9375em) {
            .site-header {
                background-color: #2c3139;
                background-repeat:;
                background-position:;
                background-size:;
                background-attachment:
            }
        }

        .site-header, #site-top-bar {
            padding-left: 20px;
            padding-right: 20px
        }

        @media only screen and (min-width: 63.9375em) {
            .site-branding img {
                height: 57px;
                width: auto
            }

            .site-header .main-navigation, .site-header .site-tools {
                height: 57px;
                line-height: 57px
            }
        }

        @media only screen and (min-width: 63.9375em) {
            .site-header.sticky .main-navigation, .site-header.sticky .site-tools, .site-header.sticky .site-branding img {
                height: 33px;
                line-height: 33px;
                width: auto
            }
        }

        @media only screen and (min-width: 63.9375em) {
            .site-header {
                padding-top: 22px
            }
        }

        @media only screen and (min-width: 63.9375em) {
            .site-header {
                padding-bottom: 22px
            }
        }

        @media only screen and (min-width: 63.9375em) {
            #page_wrapper.sticky_header .content-area, #page_wrapper.transparent_header .content-area {
                margin-top: 144px
            }

            .transparent_header .single-post-header .title, #page_wrapper.transparent_header .shop_header .page-title {
                padding-top: 144px
            }

            .transparent_header .single-post-header.with-thumb .title {
                padding-top: 344px
            }

            .transparent_header.sticky_header .page-title-shown .entry-header.with_featured_img, {
                margin-top: -229px
            }

            .sticky_header .page-title-shown .entry-header.with_featured_img {
                margin-top: -144px
            }

            .page-template-default .transparent_header .entry-header.with_featured_img, .page-template-page-full-width .transparent_header .entry-header.with_featured_img {
                margin-top: -229px
            }
        }

        .site-header, .default-navigation, .main-navigation .mega-menu > ul > li > a {
            font-size: 16px
        }

        .site-header, .main-navigation a, .site-tools ul li a, .shopping_bag_items_number, .wishlist_items_number, .site-title a, .widget_product_search .search-but-added, .widget_search .search-but-added {
            color: #616a75
        }

        .site-branding {
            border-color: #cecece
        }

        @media only screen and (min-width: 63.9375em) {
            .site-header, .main-navigation a, .site-tools ul li a, .shopping_bag_items_number, .wishlist_items_number, .site-title a, .widget_product_search .search-but-added, .widget_search .search-but-added {
                color: #cecece
            }

            .site-branding {
                border-color: #cecece
            }
        }

        @media only screen and (min-width: 63.9375em) {
            #page_wrapper.transparent_header.transparency_light .site-header, #page_wrapper.transparent_header.transparency_light .site-header .main-navigation a, #page_wrapper.transparent_header.transparency_light .site-header .site-tools ul li a, #page_wrapper.transparent_header.transparency_light .site-header .shopping_bag_items_number, #page_wrapper.transparent_header.transparency_light .site-header .wishlist_items_number, #page_wrapper.transparent_header.transparency_light .site-header .site-title a, #page_wrapper.transparent_header.transparency_light .site-header .widget_product_search .search-but-added, #page_wrapper.transparent_header.transparency_light .site-header .widget_search .search-but-added {
                color: #fff
            }
        }

        @media only screen and (min-width: 63.9375em) {
            #page_wrapper.transparent_header.transparency_dark .site-header, #page_wrapper.transparent_header.transparency_dark .site-header .main-navigation a, #page_wrapper.transparent_header.transparency_dark .site-header .site-tools ul li a, #page_wrapper.transparent_header.transparency_dark .site-header .shopping_bag_items_number, #page_wrapper.transparent_header.transparency_dark .site-header .wishlist_items_number, #page_wrapper.transparent_header.transparency_dark .site-header .site-title a, #page_wrapper.transparent_header.transparency_dark .site-header .widget_product_search .search-but-added, #page_wrapper.transparent_header.transparency_dark .site-header .widget_search .search-but-added {
                color: #2c3139
            }
        }

        @media only screen and (min-width: 63.9375em) {
            .site-header.sticky, #page_wrapper.transparent_header .site-header.sticky {
                background: #2c3139
            }
        }

        @media only screen and (min-width: 63.9375em) {
            .site-header.sticky, .site-header.sticky .main-navigation a, .site-header.sticky .site-tools ul li a, .site-header.sticky .shopping_bag_items_number, .site-header.sticky .wishlist_items_number, .site-header.sticky .site-title a, .site-header.sticky .widget_product_search .search-but-added, .site-header.sticky .widget_search .search-but-added, #page_wrapper.transparent_header .site-header.sticky, #page_wrapper.transparent_header .site-header.sticky .main-navigation a, #page_wrapper.transparent_header .site-header.sticky .site-tools ul li a, #page_wrapper.transparent_header .site-header.sticky .shopping_bag_items_number, #page_wrapper.transparent_header .site-header.sticky .wishlist_items_number, #page_wrapper.transparent_header .site-header.sticky .site-title a, #page_wrapper.transparent_header .site-header.sticky .widget_product_search .search-but-added, #page_wrapper.transparent_header .site-header.sticky .widget_search .search-but-added {
                color: #616a75
            }

            .site-header.sticky .site-branding {
                border-color: #616a75
            }
        }

        @media only screen and (max-width: 63.9375em) {
            .site-logo {
                display: none
            }

            .sticky-logo {
                display: block
            }
        }

        #site-footer {
            background: #2c3139
        }

        #site-footer, #site-footer .copyright_text a {
            color: #707984
        }

        #site-footer a, #site-footer .widget-title, .cart-empty-text, .footer-navigation-wrapper ul li:after {
            color: #707984
        }

        .trigger-footer-widget-area {
            display: none
        }

        .site-footer-widget-area {
            display: block
        }

        .additional-line-content {
            margin-bottom: 30px
        }

        select#pa_city, select#pa_dates {
            font-size: 18px;
            line-height: 25px;
            border: solid 2px #B5C832;
            font-family: "Montserrat";
            font-weight: 400;
            padding: 0 20px;
            color: #888
        }

        .hide-section, span.product_image_zoom_button.show-for-medium-up {
            display: none !important
        }

        .custom-side-menu ul li:after {
            content: "";
            display: block;
            width: 40px;
            height: 1px;
            background-color: #ADADAD;
            margin-top: 10px
        }

        .custom-side-menu ul li a {
            font-size: 16px;
            font-family: Montserrat
        }

        @media screen and (max-width: 1024px) and (min-width: 480px) {
            .person-memo {
                display: none
            }
        }

        .menu-main-menu-en li:nth-child(6) ul {
            top: -170px
        }

        .menu-main-menu-en li:nth-child(7) ul {
            top: -117px
        }

        .menu-main-menu-en li:nth-child(8) ul {
            top: -50px
        }

        .menu-main-menu-en li:nth-child(9) ul {
            top: -175px
        }

        .menu-main-menu-en li:nth-child(10) ul {
            top: -239px
        }

        .menu-main-menu-en li:nth-child(11) ul {
            top: -259px
        }

        .menu-main-menu-en li:nth-child(12) ul {
            top: -280px
        }

        @media screen and (min-width: 63.9375em) {
            .vc_grid-filter-dropdown .vc_grid-styled-select select, .vc_grid-filter-select .vc_grid-styled-select select {
                font-size: 16px;
                line-height: 1.5em;
                padding: 15px;
                height: auto
            }

            .vc_grid-filter-dropdown .vc_grid-styled-select, .vc_grid-filter-select .vc_grid-styled-select {
                height: auto
            }

            #masthead .extended-menu > ul > li > ul > li > a {
                padding: 5px 30px
            }

            .info-page-ul-properties {
                position: fixed;
                width: 16.66666667%
            }
        }

        .aclass_blue .vc_toggle_content a {
            color: #fff
        }

        .hover-effect-link .hover-effect-thumb {
            opacity: .7
        }

        #shopkeeper-menu-item-15 a:after {
            display: none
        }

        .menu-main-menu-en li#shopkeeper-menu-item-15 ul {
            top: 105%;
            background: #3A414C;
            left: -45px
        }

        li#shopkeeper-menu-item-15 ul li a {
            color: #9E9E9E !important;
            text-transform: uppercase;
            font-family: Montserrat;
            border-bottom: 0
        }

        li#shopkeeper-menu-item-15 ul li a span {
            color: #BFD432
        }

        li#shopkeeper-menu-item-15 ul li a:hover {
            color: #AFAFAF !important
        }

        .menu-main-menu-en li#shopkeeper-menu-item-15 ul li {
            text-align: center
        }

        @media only screen and (min-width: 40.063em) {
            .tools_button_icon i {
                margin-top: 6px
            }

            .search-button .tools_button_icon {
                background-color: #BFD432;
                border-radius: 50%;
                width: 32px;
                padding: 0 7px;
                color: #2C3139
            }
        }

        .page-template-default .entry-header.with_featured_img, .page-template-default .entry-header.with_featured_img {
            display: none
        }

        .clickdesk-lighten .cd-chat-inner .cd-send-msg {
            padding: 0 !important
        }

        .course-accordeon {
            padding-left: 0
        }

        .products a.button {
            font-size: 9px !important
        }

        .products a.button:before {
            top: 4px;
            font-size: 8px
        }

        .tp-caption.Slider-p-Karla, .Slider-p-Karla {
            font-size: 17px
        }

        .vc_gitem-post-data {
            margin-bottom: 0
        }

        .vc_custom_1464176270445 {
            margin-right: 0 !important;
            margin-left: 0 !important
        }

        .vc_custom_1464176289618 {
            margin-right: 0 !important;
            margin-left: 0 !important
        }

        .vc_custom_1464176052118 {
            margin-right: 0 !important;
            margin-left: 0 !important;
            text-align: justify
        }

        .page-id-125 .vc_column_container > .vc_column-inner {
            padding: 10px !important
        }

        .page-id-125 .vc_gitem_row .vc_gitem-col {
            padding: 5px !important
        }

        .masonry-hide-on-large {
            display: none
        }

        @media only screen and (max-width: 768px) {
            .row {
                padding: 0 5px
            }

            .cd-top {
                display: none
            }

            .woocommerce table.shop_table.cart td, .woocommerce-page table.shop_table.cart td {
                padding-right: 5px !important
            }

            .masonry-hide-on-small {
                display: none
            }

            .masonry-hide-on-large {
                display: block
            }

            .vc_tta.vc_tta-accordion .vc_tta-controls-icon-position-left.vc_tta-panel-title > a {
                padding-left: 11px !important;
                padding-right: 0
            }

            .course-accordeon .vc_tta-panel-heading span {
                font-size: 16px;
                padding-right: 0
            }
        }

        .woocommerce-checkout .content-area .shortcode_icon_box h3 {
            margin: 8px 0 11px 0 !important;
            color: #202329 !important
        }

        form span.wpcf7-not-valid-tip {
            text-align: left
        }

        .wpcf7-form-control.wpcf7-submit:disabled {
            opacity: .5
        }

        .single-product .product_description {
            display: none
        }

        .enq-form-desc {
            background: #202329 none repeat scroll 0 0;
            clear: both;
            padding: 20px 0;
            text-align: center
        }

        .enq-form-desc form {
            margin: 0 auto;
            max-width: 33%
        }

        #enq-form form > h3 {
            color: #fff;
            font-size: 2.2rem;
            margin-bottom: 28px;
            text-transform: inherit
        }

        .enq-form-button {
            display: block !important;
            float: right !important;
            height: 50px !important;
            line-height: 20px;
            margin-left: 20px;
            max-width: 100%;
            min-width: 140px
        }

        .enq-form-button.disabled {
            opacity: .5;
            cursor: not-allowed
        }

        .single_add_to_cart_button {
            float: right !important;
            min-width: auto !important;
            padding: 0 26px !important
        }

        body.woocommerce-account .myaccount_user {
            margin: 0 auto !important
        }

        .archive-product-rating > span {
            color: #bfd432;
            display: block;
            float: left;
            font-size: 12px;
            font-weight: 700;
            line-height: 14px
        }

        .archive-product-rating .star-rating {
            float: left !important;
            margin-bottom: 0 !important
        }

        .product_after_shop_loop {
            clear: both
        }

        .product-overlay {
            background: rgba(44, 50, 58, .85) none repeat scroll 0 0;
            color: #fff;
            height: 100%;
            left: -100%;
            padding: 20px 10px;
            position: absolute;
            text-align: left;
            top: 0;
            width: 100%;
            -webkit-transition: all 0.2s ease;
            transition: all 0.2s ease
        }

        .product-overlay::after {
            bottom: 4px;
            color: #bfd432;
            content: "more";
            font-size: 10px;
            left: 0;
            position: absolute;
            text-align: center;
            display: none;
            width: 100%
        }

        .product_thumbnail_wrapper {
            overflow: hidden
        }

        #products-grid li .product_thumbnail_wrapper:hover .product-overlay {
            left: 0
        }

        .product-overlay-skill {
            font-size: 14px;
            letter-spacing: -1px;
            line-height: 90%
        }

        .product-overlay-date, .product-overlay-duration {
            bottom: 0;
            color: #bfd432;
            font-size: 13px;
            left: 0;
            margin: 20px 0;
            overflow: hidden;
            padding: 0 10px;
            position: absolute;
            width: 100%;
            line-height: 100%
        }

        body.page.page-child .product-overlay-date, body.page.page-child .product-overlay-duration {
            bottom: 6px;
            font-size: 10px;
            height: auto;
            line-height: 90%;
            margin: 0;
            padding: 0 6px
        }

        .product-overlay-date span, .product-overlay-duration span {
            color: #fff;
            font-size: 10px
        }

        .site-social-icons-wrapper {
            padding: 6px 0 !important
        }

        .header-review-body {
            color: #fff
        }

        .header-review-stars {
            float: right
        }

        .header-review-stars::before {
            content: "";
            font-family: FontAwesome;
            font-style: normal;
            font-weight: 400;
            color: #bfd432;
            text-decoration: inherit
        }

        .header-review-count {
            color: #aeaeae
        }

        .shop-subtitle {
            text-align: center
        }

        .prod-skill-short > p {
            font-size: 10px;
            line-height: 90%;
            margin: 0 0 6px
        }

        .prod-skill-short > p:nth-child(2) {
            left: 0;
            padding: 6px;
            position: absolute;
            text-align: left;
            top: 50%;
            -webkit-transform: translate3d(0, -50%, 0);
            -moz-transform: translate3d(0, -50%, 0);
            transform: translate3d(0, -50%, 0);
            width: 100%
        }

        .product-overlay, body.page.page-child .prod-skill-short {
            display: block !important
        }

        body.page.page-child .product-overlay {
            padding: 6px
        }

        .prod-emailback-open {
            display: none;
            margin: 0 auto 40px !important;
            max-width: 240px
        }

        .prod-emailback {
            background: #202329;
            display: none;
            left: 50%;
            position: fixed;
            top: 50%;
            padding: 40px;
            -webkit-transform: translate3d(-50%, -50%, 0);
            -moz-transform: translate3d(-50%, -50%, 0);
            transform: translate3d(-50%, -50%, 0);
            width: 100%;
            max-height: 80%;
            z-index: 33333333;
            max-width: 480px
        }

        .prod-emailback-close {
            cursor: pointer;
            z-index: 3333333;
            background: rgba(0, 0, 0, .75);
            position: fixed;
            height: 100%;
            width: 100%;
            left: 0;
            top: 0;
            display: none
        }

        .prod-emailback-close-in {
            background: transparent url(/wp-content/themes/shopkeeper-child/img/close.png) no-repeat scroll center center / cover;
            cursor: pointer;
            font-weight: 700;
            height: 20px;
            position: fixed;
            right: 8px;
            top: 8px;
            width: 20px;
            -webkit-transition: all 0.2s ease;
            transition: all 0.2s ease
        }

        .prod-emailback-thank {
            display: none;
            margin: 14px auto 0;
            text-align: center;
            text-transform: uppercase
        }

        .prod-emailback-close-in:hover {
            opacity: .75
        }

        .prod-emailback input {
            margin-bottom: 16px
        }

        .prod-emailback-send {
            margin: 0 !important;
            width: 100% !important
        }

        .page-id-261 .vc_grid-item h4 {
            cursor: pointer
        }

        .page-id-261 .vc_gitem-post-data-source-post_excerpt p {
            font-size: .9rem
        }

        .blog-cta-wrap {
            text-align: center;
            padding: 30px 0
        }

        .blog-related-list {
            padding: 30px 0
        }

        .blog-soc-sharing {
            font-size: 2rem;
            text-align: center
        }

        .blog-soc-sharing a {
            margin-right: 1rem
        }

        .blog-soc-sharing a:hover {
            opacity: .75
        }

        .blog-soc-sharing a:last-child {
            margin-right: 0
        }

        .blog-soc-sharing > h5 {
            color: #fff
        }

        .search-field::-webkit-input-placeholder {
            opacity: 1 !important
        }

        .search-field:-moz-placeholder {
            opacity: 1 !important
        }

        .search-field::-moz-placeholder {
            opacity: 1 !important
        }

        .search-field:-ms-input-placeholder {
            opacity: 1 !important
        }

        .page-id-261 #page_wrapper {
            min-height: 600px
        }

        .blog-search-form {
            display: none;
            position: absolute;
            right: 290px;
            z-index: 2
        }

        .blog-subtitle {
            text-align: center
        }

        .blog-search-form input {
            border: 1px solid #ccc;
            font-size: 1.1rem;
            height: 54px
        }

        .blog-search-form .search-submit {
            background: rgba(0, 0, 0, 0) none repeat scroll 0 0 !important;
            display: inline-block !important;
            max-width: 36px !important;
            min-width: auto !important;
            position: absolute;
            right: 0;
            text-indent: -9999px;
            padding: 0px !important;
            width: 100% !important;
            top: 0;
            z-index: 2
        }

        .blog-search-form .fa.fa-search {
            position: absolute;
            right: 10px;
            top: 20px
        }

        ins .amount {
            font-weight: 700
        }

        .about-city-col {
            width: 20%
        }

        .search-results .blog-search-form {
            display: block !important;
            margin: 20px auto 0;
            max-width: 400px;
            position: relative !important;
            right: auto
        }

        .parent-pageid-324 .vc_tta-accordion .vc_tta-title-text {
            padding: 14px 0 12px
        }

        .parent-pageid-324 .vc_tta.vc_tta-accordion .vc_tta-controls-icon-position-left.vc_tta-panel-title > a {
            padding-left: 0
        }

        .parent-pageid-324 .vc_tta.vc_general .vc_tta-panel-body {
            padding: 14px 0
        }

        .parent-pageid-324 .vc_tta-panel-title a {
            cursor: inherit
        }

        .checkbox-770 span.wpcf7-list-item input {
            margin: 0 4px 0 0;
            position: relative;
            top: 2px
        }

        .checkbox-770 span.wpcf7-list-item span {
            font-size: 1rem
        }

        .checkbox-770 span.wpcf7-list-item {
            display: block;
            float: left;
            margin: 0 0 10px 0;
            width: 33.3%
        }

        .newsletter-int-label {
            color: #bfd432;
            font-size: 1rem !important
        }

        .newsletter-int-options {
            margin-bottom: 20px !important;
            margin-top: 0 !important
        }

        #site-footer .wpcf7-form > p.newsletter-int-options {
            display: none;
            margin: 0 !important
        }

        .video-search-form {
            display: block;
            margin: 10px auto 20px;
            max-width: 300px;
            position: relative;
            right: auto
        }

        .single-portfolio .blog-soc-sharing h5 {
            color: #202329
        }

        .single-portfolio .blog-soc-sharing {
            margin-bottom: 32px
        }

        .onsale {
            -webkit-transition: all 0.2s ease;
            transition: all 0.2s ease
        }

        ul.products li:hover .onsale {
            opacity: 0
        }

        .cart-addmore-prods.button.alt {
            max-width: 220px;
            margin: 0 auto !important;
            display: block !important;
            height: 32px !important;
            line-height: 32px !important;
            padding: 0px !important
        }

        .partners-slider > .wpb_column > .vc_column-inner > .wpb_wrapper img {
            width: 100% !important
        }

        .partners-slider .wpb_wrapper.vc_figure {
            margin: 10px
        }

        .prod-courses-search {
            margin: 32px auto 0 auto;
            max-width: 300px
        }

        .prod-courses-search > .widget {
            margin-bottom: 0px !important
        }

        .prod-courses-search input {
            border-color: #ccc !important;
            font-size: 18px;
            height: 52px !important
        }

        .prod-courses-search .widget_product_search input:focus {
            background: #fafafa !important;
            border-color: #ccc !important
        }

        .prod-courses-search form::after {
            display: none !important
        }

        .prod-courses-search form label {
            display: block;
            height: 100%;
            margin: 0;
            position: absolute;
            right: 0;
            text-indent: -9999px;
            top: 0;
            width: 36px;
            border-left: 1px solid #ccc
        }

        .prod-courses-search form label::after {
            font-family: FontAwesome;
            font-size: 16px;
            font-weight: 400;
            text-indent: 0;
            content: "";
            position: absolute;
            top: 50%;
            left: 50%;
            -webkit-transform: translate3d(-50%, -50%, 0);
            -moz-transform: translate3d(-50%, -50%, 0);
            transform: translate3d(-50%, -50%, 0)
        }

        .prod-courses-search form input[type="submit"] {
            width: 36px
        }

        .prod-courses-search form input[type="submit"]:hover {
            background: rgba(191, 212, 50, .8) none repeat scroll 0 0 !important
        }

        .single .wpb_video_widget > .wpb_wrapper {
            float: none !important;
            margin: 0 auto !important
        }

        body.woocommerce-cart .cart_totals h2, #billing_address_1_field, #billing_city_field, .prod-skill-short, .blog-posts-grid .vc_gitem-post-data-source-post_excerpt > div > p, .blog-soc-email, #site-footer .newsletter-int-label {
            display: none
        }

        .blog-posts-grid .vc_gitem-post-data-source-post_excerpt > div > p:first-child {
            display: block
        }

        .enq-form-loc, .enq-form-date, #billing_country_field, .woocommerce-cart .cart-buttons .update_and_checkout, .woocommerce-cart .cart-buttons .checkout-button, body.woocommerce-order-received .vc_custom_1469721166338, body.page.page-child .prod-skill-long, body.page.page-child .product-overlay-duration span, .prod-emailback-info, .parent-pageid-324 .vc_tta-panel-title .vc_tta-controls-icon, .addtocart-hidden .single_add_to_cart_button, .shop_table tr.shipping, .partners-slider .owl-controls {
            display: none !important
        }

        @media screen and (min-width: 768px) and (max-width: 1300px) {
            .product-overlay-duration span {
                display: none
            }
        }

        @media screen and (min-width: 641px) {
            .blog-single .single-post-header.with-thumb .title {
                padding: 225px 0 80px
            }
        }

        @media screen and (min-width: 1300px) {
            body.page.page-child .product-overlay {
                padding: 6px
            }

            body.page.page-child .product-overlay-date, body.page.page-child .product-overlay-duration, .product-overlay-date span, .product-overlay-duration span, .prod-skill-short > p {
                font-size: 14px
            }
        }

        @media screen and (min-width: 768px) and (max-width: 1022px) {
            .blog-search-form {
                right: 200px
            }

            .blog-search-form input {
                font-size: .8rem;
                height: 28px
            }

            .blog-search-form .fa.fa-search {
                top: 6px
            }
        }

        @media screen and (max-width: 1024px) {
            .checkbox-770 span.wpcf7-list-item {
                width: 50%
            }

            .land-courses-left, .land-courses-right, .land-courses-top > div {
                float: none !important
            }
        }

        @media screen and (max-width: 768px) {
            .about-city-col {
                width: 100%
            }

            .enq-form-desc form {
                max-width: 90%
            }

            body.page.page-child .product-overlay-date, body.page.page-child .product-overlay-duration, .product-overlay-date span, .product-overlay-duration span, .prod-skill-short > p {
                font-size: 14px
            }

            .blog-search-form {
                position: relative;
                margin-top: 20px;
                right: auto
            }

            .blog-search-form input {
                font-size: 1rem;
                height: 42px
            }

            .blog-search-form .fa.fa-search {
                top: 14px
            }
        }

        @media screen and (max-width: 479px) {
            .product-overlay {
                display: none
            }
        }

        .land-courses-wrap {
            padding: 80px 0
        }

        .lct-header {
            background: #c1d72f none repeat scroll 0 0
        }

        .lct-header th {
            color: #fff;
            font-size: 1rem !important;
            font-weight: 400;
            padding: 10px;
            text-transform: inherit;
            line-height: 140%
        }

        .lct-body td {
            font-size: .8rem;
            color: #000;
            padding: 10px;
            line-height: 140%
        }

        .lct-body tr:nth-child(2n+2) {
            background: #f3f7d5
        }

        .lct-body tr {
            -webkit-transition: all 0.2s ease;
            transition: all 0.2s ease
        }

        .lct-body tr:hover {
            opacity: .75
        }

        .lct-body .lct-open {
            color: #58c9e8;
            cursor: pointer;
            text-decoration: underline
        }

        .lct-body td:last-child {
            color: #e74c3c
        }

        .lct-body > tr > td:nth-child(2) {
            max-width: 106px
        }

        .lct-body > tr > td:nth-child(4) {
            min-width: 140px
        }

        .land-courses-items {
            padding: 80px 0
        }

        .land-courses-content h4 {
            text-transform: inherit !important
        }

        .land-courses-top {
            position: relative;
            padding-bottom: 40px
        }

        .land-courses-top::after {
            bottom: 0;
            content: "";
            display: block;
            height: 10px;
            left: 0;
            position: absolute;
            width: 100%;
            -webkit-box-shadow: 0 4px 4px 0 rgba(50, 50, 50, .5) inset;
            -moz-box-shadow: 0 4px 4px 0 rgba(50, 50, 50, .5) inset;
            box-shadow: 0 4px 4px 0 rgba(50, 50, 50, .5) inset
        }

        .woocommerce-cart .product-price .woocommerce-price-suffix, .woocommerce-cart .product-subtotal .tax_label {
            display: block;
            font-size: .6rem;
            line-height: 100%
        }

        .land-courses-item .product-images-wrapper > .images {
            display: block !important
        }

        .land-courses-item .product-images-wrapper > .images {
            display: block !important
        }

        .land-courses-item .product-images-wrapper > script + .images {
            display: none !important
        }

        .shop_table.shop_table_responsive tr.order-total .includes_tax {
            display: block
        }

        .lct-body del .woocommerce-price-suffix, .land-courses-item, .page-template-page-landing .product_meta, .page-template-page-landing .woocommerce-breadcrumb, .page-template-page-landing .product_navigation, .shop_table.shop_table_responsive tr.order-total .includes_tax .woocommerce-price-suffix {
            display: none
        }

        @media screen and (max-width: 1299px) {
            .land-courses-left, .land-courses-right {
                width: 100% !important
            }
        }

        @media screen and (max-width: 767px) {
            .lct-body td, .lct-header th {
                font-size: 0.7rem !important
            }
        }

        @media screen and (max-width: 479px) {
            .lct-body td, .lct-header th {
                font-size: 0.65rem !important
            }

            .lct-body td:last-child, .lct-header th:last-child {
                display: none
            }
        }

        .eddie-logo {
            background-image: url(https://www.academyclass.com/wp-content/uploads/2016/06/eddie_profile.png);
            background-color: white;
            background-position: center;
            background-size: cover
        }

        .courses-block .courses-tab, .courses-block .packages-tab {
            max-width: 730px !important
        }

        .offcanvas_content_right li.open-offcanvas {
            display: block
        }

        nav#site-navigation-top-bar {
            width: 888px
        }

        .logout_link {
            display: none
        }

        .wpcf7-form-control.g-recaptcha.wpcf7-recaptcha {
            margin-left: 140px
        }

        @media screen and (max-width: 1370px) {
            .wpcf7-form-control.g-recaptcha.wpcf7-recaptcha {
                margin-left: 65px
            }
        }

        @media screen and (max-width: 1200px) {
            nav#site-navigation-top-bar {
                width: 760px !important
            }
        }</style>
    <style type="text/css" id="wp-custom-css">.headerText {
            font-size: .9em
        }</style>
    <style type="text/css" data-type="vc_custom-css">@media all and (min-width: 720px) {
            .fourth_row a, .second_row span {
                text-transform: uppercase;
                color: #000
            }

            .top_slider {
                background-size: cover;
                width: 100%;
                min-height: 410px;
                transform: translate3d(0, 0, 0) scale(1.05, 1.05);
                margin-bottom: -25px
            }

            .first_row h1, .second_row span {
                min-height: 0;
                max-height: none;
                visibility: inherit;
                opacity: 1
            }

            .top_slider_content {
                width: 50%;
                margin-left: 15%;
                padding-top: 50px;
                min-width: 730px
            }

            .first_row {
                margin-bottom: 20px;
                max-width: 684px
            }

            .first_row h1 {
                font-size: 38px;
                font-weight: 700;
                transition: none;
                line-height: 50px;
                border-width: 0;
                margin: 0;
                padding: 0;
                letter-spacing: 0
            }

            .second_row img, .third_row span {
                border-width: 0;
                margin: 0;
                padding: 0;
                font-weight: 400;
                transition: none;
                letter-spacing: 0
            }

            .second_row img {
                width: 100px;
                height: 54px;
                line-height: 0;
                font-size: 18px
            }

            .second_row span {
                z-index: 6;
                white-space: nowrap;
                transition: none;
                line-height: 16px;
                border-width: 0;
                margin: 0;
                padding: 0;
                letter-spacing: 0;
                font-size: 16px;
                min-width: 0;
                max-width: none;
                transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
                transform-origin: 50% 50% 0
            }

            .third_row {
                width: 71%;
                max-width: 530px
            }

            .third_row span {
                z-index: 7;
                min-width: 525px;
                max-width: 525px;
                white-space: normal;
                font-size: 21px;
                line-height: 26px;
                visibility: inherit;
                min-height: 75px;
                max-height: 75px;
                opacity: 1;
                transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
                transform-origin: 50% 50% 0;
                color: #000
            }

            .fourth_row {
                margin-top: 20px
            }

            .fourth_row a {
                z-index: 8;
                white-space: nowrap;
                padding: 18px 25px;
                outline: 0;
                box-shadow: none;
                box-sizing: border-box;
                cursor: pointer;
                visibility: inherit;
                transition: none;
                border-radius: 0;
                font-style: normal;
                text-decoration: none;
                border: 0 solid #000;
                line-height: 12px;
                margin: 0;
                letter-spacing: 0;
                font-size: 12px;
                min-height: 0;
                min-width: 0;
                max-height: none;
                max-width: none;
                opacity: 1;
                transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
                transform-origin: 50% 50% 0;
                background-color: #bfd432
            }

            .c_row {
                min-height: 80px
            }
        }

        @media all and (max-width: 720px) {
            .top_slider {
                width: 100%;
                min-height: 410px;
                transform: translate3d(0, 0, 0) scale(1.05, 1.05);
                margin-bottom: -25px
            }

            .first_row h1, .second_row span {
                visibility: inherit;
                min-height: 0;
                max-height: none;
                opacity: 1
            }

            .second_row span, .third_row span {
                transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)
            }

            .top_slider_content {
                width: 75%;
                margin-left: 5%;
                padding-top: 50px;
                min-width: 250px
            }

            .first_row {
                margin-bottom: 20px;
                max-width: 684px
            }

            .first_row h1 {
                font-size: 20px;
                font-weight: 700;
                transition: none;
                line-height: 21px;
                border-width: 0;
                margin: 40px 0 0;
                padding: 0;
                letter-spacing: 0
            }

            .second_row img, .second_row span {
                transition: none;
                margin: 0;
                letter-spacing: 0;
                border-width: 0;
                padding: 0
            }

            .second_row img {
                width: 50px;
                height: 27px;
                line-height: 0;
                font-weight: 400;
                font-size: 18px
            }

            .second_row span {
                z-index: 6;
                white-space: nowrap;
                text-transform: uppercase;
                line-height: 16px;
                font-size: 16px;
                min-width: 0;
                max-width: none;
                transform-origin: 50% 50% 0;
                color: #000
            }

            .third_row {
                width: 100%;
                margin: 10px 0 30px;
                max-width: 530px
            }

            .fourth_row a, .third_row span {
                visibility: inherit;
                transition: none;
                color: #000;
                margin: 0;
                letter-spacing: 0;
                opacity: 1
            }

            .third_row span {
                z-index: 7;
                min-width: 525px;
                max-width: 525px;
                white-space: normal;
                font-size: 14px;
                line-height: 15px;
                border-width: 0;
                padding: 0;
                font-weight: 400;
                min-height: 75px;
                max-height: 75px;
                transform-origin: 50% 50% 0
            }

            .fourth_row a {
                z-index: 8;
                white-space: nowrap;
                text-transform: uppercase;
                padding: 18px 25px;
                outline: 0;
                box-shadow: none;
                box-sizing: border-box;
                cursor: pointer;
                border-radius: 0;
                font-style: normal;
                text-decoration: none;
                border: 0 solid #000;
                line-height: 12px;
                font-size: 12px;
                min-height: 0;
                min-width: 0;
                max-height: none;
                max-width: none;
                transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
                transform-origin: 50% 50% 0;
                background-color: #bfd432
            }

            .c_row {
                min-height: 50px
            }
        }

        @media all and (min-width: 720px) and (max-width: 900px) {
            .first_row h1 {
                font-size: 37px !important
            }
        }

        img.emoji, img.wp-smiley {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -.1em !important;
            background: 0 0 !important;
            padding: 0 !important
        }

        .vc_custom_1463561559457 {
            background-color: rgba(255, 255, 255, .18) !important
        }

        .container-3 {
            width: 230px;
            vertical-align: top;
            white-space: nowrap;
            position: relative;
            margin-left: 180px
        }

        .container-3 input#search_form {
            width: 230px;
            height: 48px;
            background: #D8D8D8;
            border: 1px solid #abaaaa;
            font-size: 10pt;
            float: left;
            color: #262626;
            padding-left: 45px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            color: #green
        }

        .container-3 input#search_form::-webkit-input-placeholder {
            color: #65737e
        }

        .container-3 input#search_form:-moz-placeholder {
            color: #65737e
        }

        .container-3 input#search_form::-moz-placeholder {
            color: #65737e
        }

        .container-3 input#search_form:-ms-input-placeholder {
            color: #65737e
        }

        .container-3 .icon {
            position: absolute;
            top: 50%;
            margin-left: 17px;
            margin-top: 17px;
            z-index: 1;
            color: #4f5b66;
            -webkit-transition: all .55s ease;
            -moz-transition: all .55s ease;
            -ms-transition: all .55s ease;
            -o-transition: all .55s ease;
            transition: all .55s ease
        }

        .container-22 {
            margin-bottom: -47px
        }

        @-moz-document url-prefix() {
            .container-3 .icon {
                margin-left: -210px
            }
        }

        @media all and (max-width: 720px) {
            .container-3 {
                margin-left: 0
            }

            .container-22 {
                margin-bottom: 18px
            }
        }

        .icon-block {
            width: 14%;
            background-size: cover;
            background-position: center;
            display: inline-block;
            height: 155px;
            margin-left: 20px;
            margin-bottom: 20px;
            position: relative
        }

        .icons-container {
            max-width: 1200px;
            margin-left: auto;
            margin-right: auto
        }

        .container {
            background-color: #eff1f6;
            padding: 40px 0 70px
        }

        .heading-text {
            color: #202329;
            font-family: Montserrat, sans-serif;
            text-align: center;
            font-size: 1.875rem
        }

        .icon-text {
            position: absolute;
            bottom: 10px;
            width: 100%;
            text-align: center;
            font-family: Karla, sans-serif;
            color: #fff;
            font-weight: 700
        }

        .button-courses {
            background-color: #bfd432;
            color: #191919;
            padding: 10px;
            width: 200px;
            margin: 0 auto
        }

        @media (max-width: 1250px) {
            .icon-block {
                height: 160px;
                width: 22%
            }
        }

        @media (max-width: 1050px) {
            .icon-block {
                height: 200px;
                width: 30%
            }
        }

        @media (max-width: 798px) {
            .icon-block {
                height: 220px;
                width: 45%
            }
        }

        @media (max-width: 630px) {
            .icon-block {
                width: 42%
            }
        }

        @media (max-width: 500px) {
            .icon-block {
                height: 150px
            }
        }

        #add-subs-list input[type="checkbox"] {
            transform: scale(1);
            margin: 0
        }</style>
    <style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1520596794170 {
            margin-top: 0px !important;
            margin-right: 0px !important;
            margin-bottom: 0px !important;
            margin-left: 0px !important;
            border-top-width: 0px !important;
            border-right-width: 0px !important;
            border-bottom-width: 0px !important;
            border-left-width: 0px !important;
            padding-top: 0px !important;
            padding-right: 0px !important;
            padding-bottom: 0px !important;
            padding-left: 0px !important;
            background-image: url(https://www.academyclass.com/wp-content/uploads/2018/02/Home-1.jpg?id=19928) !important;
            background-position: center !important;
            background-repeat: no-repeat !important;
            background-size: cover !important
        }

        .vc_custom_1524059809030 {
            margin-top: 40px !important;
            margin-right: 0px !important;
            margin-bottom: 0px !important;
            margin-left: 0px !important;
            border-top-width: 0px !important;
            border-right-width: 0px !important;
            border-bottom-width: 0px !important;
            border-left-width: 0px !important;
            padding-top: 0px !important;
            padding-right: 0px !important;
            padding-bottom: 0px !important;
            padding-left: 0px !important;
            background: #fff url(https://www.academyclass.com/wp-content/uploads/2018/02/Academy-Class-Top-Courses-2.jpg?id=20042) !important;
            background-position: center !important;
            background-repeat: no-repeat !important;
            background-size: contain !important
        }

        .vc_custom_1519747927185 {
            margin-top: 0px !important;
            margin-right: 0px !important;
            margin-bottom: 15px !important;
            margin-left: 0px !important;
            border-top-width: 0px !important;
            border-right-width: 0px !important;
            border-bottom-width: 0px !important;
            border-left-width: 0px !important;
            padding-top: 0px !important;
            padding-right: 0px !important;
            padding-bottom: 0px !important;
            padding-left: 0px !important
        }

        .vc_custom_1519741661376 {
            margin-bottom: 30px !important
        }

        .vc_custom_1524058684918 {
            margin-top: 0px !important;
            margin-bottom: 0px !important;
            border-bottom-width: 0px !important;
            padding-bottom: 0px !important;
            background-color: #ffffff !important
        }

        .vc_custom_1524059960147 {
            margin-top: 50px !important;
            margin-bottom: 0px !important;
            border-top-width: 0px !important;
            border-bottom-width: 0px !important;
            padding-top: 0px !important;
            padding-bottom: 0px !important
        }

        .vc_custom_1524059239797 {
            margin-top: 30px !important
        }

        .vc_custom_1524059123118 {
            margin-top: 10px !important
        }

        .vc_custom_1492074820118 {
            background-color: #ffffff !important
        }

        .vc_custom_1520952630086 {
            margin-right: 0px !important;
            margin-left: 0px !important;
            background: #003242 url(https://www.academyclass.com/wp-content/uploads/2018/02/blue-bar.jpg?id=19289) !important;
            background-position: center !important;
            background-repeat: no-repeat !important;
            background-size: cover !important
        }

        .vc_custom_1521108302156 {
            padding-top: 80px !important
        }

        .vc_custom_1521016914790 {
            padding-bottom: 40px !important;
            background: #fff url(https://www.academyclass.com/wp-content/uploads/2018/02/Academy-Class-Logo-background.jpg?id=20045) !important;
            background-position: center !important;
            background-repeat: no-repeat !important;
            background-size: contain !important
        }

        .vc_custom_1519741632341 {
            margin-top: 30px !important;
            margin-bottom: 30px !important;
            background-color: #ffffff !important
        }

        .vc_custom_1519747991285 {
            margin-bottom: 20px !important
        }

        .vc_custom_1524059558335 {
            margin-top: 0px !important;
            margin-bottom: 0px !important;
            border-top-width: 0px !important;
            border-bottom-width: 0px !important;
            padding-top: 0px !important;
            padding-bottom: 0px !important
        }

        .vc_custom_1521034847749 {
            margin-top: 0px !important;
            margin-right: 0px !important;
            margin-bottom: 0px !important;
            margin-left: 0px !important;
            border-top-width: 0px !important;
            border-right-width: 0px !important;
            border-bottom-width: 0px !important;
            border-left-width: 0px !important;
            padding-top: 0px !important;
            padding-right: 0px !important;
            padding-bottom: 0px !important;
            padding-left: 0px !important
        }

        .vc_custom_1524059936179 {
            margin-top: 20px !important;
            margin-bottom: 0px !important;
            border-top-width: 0px !important;
            border-bottom-width: 0px !important;
            padding-top: 0px !important;
            padding-bottom: 0px !important
        }

        .vc_custom_1524059572520 {
            margin-top: 0px !important;
            margin-bottom: 0px !important;
            border-top-width: 0px !important;
            border-bottom-width: 0px !important;
            padding-top: 0px !important;
            padding-bottom: 0px !important
        }

        .vc_custom_1524059875989 {
            margin-bottom: 0px !important;
            border-bottom-width: 0px !important;
            padding-bottom: 30px !important
        }

        .vc_custom_1524059720917 {
            margin-bottom: 0px !important;
            border-bottom-width: 0px !important;
            padding-bottom: 0px !important
        }

        .vc_custom_1524552191916 {
            margin-top: 20px !important
        }

        .vc_custom_1524058821436 {
            margin-top: 0px !important;
            margin-right: 0px !important;
            margin-bottom: 0px !important;
            margin-left: 0px !important;
            border-top-width: 0px !important;
            border-right-width: 0px !important;
            border-bottom-width: 0px !important;
            border-left-width: 0px !important;
            padding-top: 0px !important;
            padding-right: 0px !important;
            padding-bottom: 0px !important;
            padding-left: 0px !important
        }

        .vc_custom_1521034960339 {
            margin-top: 0px !important;
            margin-right: 0px !important;
            margin-bottom: 0px !important;
            margin-left: 0px !important;
            border-top-width: 0px !important;
            border-right-width: 0px !important;
            border-bottom-width: 0px !important;
            border-left-width: 0px !important;
            padding-top: 0px !important;
            padding-right: 0px !important;
            padding-bottom: 0px !important;
            padding-left: 0px !important
        }

        .vc_custom_1524058463686 {
            margin-top: 0px !important;
            margin-right: 0px !important;
            margin-bottom: 0px !important;
            margin-left: 0px !important;
            border-top-width: 0px !important;
            border-right-width: 0px !important;
            border-bottom-width: 0px !important;
            border-left-width: 0px !important;
            padding-top: 0px !important;
            padding-right: 0px !important;
            padding-bottom: 0px !important;
            padding-left: 0px !important
        }

        .vc_custom_1524059682142 {
            margin-top: 0px !important;
            margin-bottom: 0px !important;
            border-top-width: 0px !important;
            border-bottom-width: 0px !important;
            padding-top: 0px !important;
            padding-bottom: 0px !important
        }

        .vc_custom_1524059425122 {
            margin-top: 0px !important;
            border-top-width: 0px !important;
            padding-top: 0px !important
        }

        .vc_custom_1480082500745 {
            margin-right: 10px !important;
            margin-left: 10px !important
        }

        .vc_custom_1480082530336 {
            margin-right: 10px !important;
            margin-left: 10px !important
        }

        .vc_custom_1480082561140 {
            margin-right: 10px !important;
            margin-left: 10px !important
        }

        .vc_custom_1480082583233 {
            margin-right: 10px !important;
            margin-left: 10px !important
        }

        .vc_custom_1480082600430 {
            margin-right: 10px !important;
            margin-left: 10px !important
        }

        .vc_custom_1480082613142 {
            margin-right: 10px !important;
            margin-left: 10px !important
        }

        .vc_custom_1480082626207 {
            margin-right: 10px !important;
            margin-left: 10px !important
        }

        .vc_custom_1480082641876 {
            margin-right: 10px !important;
            margin-left: 10px !important
        }

        .vc_custom_1480082703393 {
            margin-right: 10px !important;
            margin-left: 10px !important
        }

        .vc_custom_1480082715270 {
            margin-right: 10px !important;
            margin-left: 10px !important
        }

        .vc_custom_1480082769154 {
            margin-right: 10px !important;
            margin-left: 10px !important
        }

        .vc_custom_1480082787455 {
            margin-right: 10px !important;
            margin-left: 10px !important
        }

        .vc_custom_1480082801892 {
            margin-right: 10px !important;
            margin-left: 10px !important
        }

        .vc_custom_1480082814615 {
            margin-right: 10px !important;
            margin-left: 10px !important
        }

        .vc_custom_1480082836370 {
            margin-right: 10px !important;
            margin-left: 10px !important
        }

        .vc_custom_1521025116116 {
            margin-top: 0px !important;
            margin-right: 0px !important;
            margin-bottom: 50px !important;
            margin-left: 0px !important;
            border-top-width: 0px !important;
            border-right-width: 0px !important;
            border-bottom-width: 0px !important;
            border-left-width: 0px !important;
            padding-top: 0px !important;
            padding-right: 0px !important;
            padding-bottom: 0px !important;
            padding-left: 20px !important
        }

        .vc_custom_1521024841845 {
            margin-top: 0px !important;
            margin-right: 0px !important;
            margin-bottom: 0px !important;
            margin-left: 0px !important;
            border-top-width: 0px !important;
            border-right-width: 0px !important;
            border-bottom-width: 0px !important;
            border-left-width: 0px !important;
            padding-top: 0px !important;
            padding-right: 0px !important;
            padding-bottom: 0px !important;
            padding-left: 0px !important
        }

        .vc_custom_1521024187747 {
            margin-top: 0px !important;
            margin-right: 0px !important;
            margin-bottom: 0px !important;
            margin-left: 0px !important;
            border-top-width: 0px !important;
            border-right-width: 0px !important;
            border-bottom-width: 0px !important;
            border-left-width: 0px !important;
            padding-top: 0px !important;
            padding-right: 0px !important;
            padding-bottom: 0px !important;
            padding-left: 0px !important
        }

        .vc_custom_1521024768651 {
            margin-top: 0px !important;
            margin-right: 0px !important;
            margin-bottom: 0px !important;
            margin-left: 0px !important;
            border-top-width: 0px !important;
            border-right-width: 0px !important;
            border-bottom-width: 0px !important;
            border-left-width: 0px !important;
            padding-top: 0px !important;
            padding-right: 0px !important;
            padding-bottom: 0px !important;
            padding-left: 0px !important
        }

        .vc_custom_1521024199246 {
            margin-top: 0px !important;
            margin-right: 0px !important;
            margin-bottom: 0px !important;
            margin-left: 0px !important;
            border-top-width: 0px !important;
            border-right-width: 0px !important;
            border-bottom-width: 0px !important;
            border-left-width: 0px !important;
            padding-top: 0px !important;
            padding-right: 0px !important;
            padding-bottom: 0px !important;
            padding-left: 0px !important
        }

        .vc_custom_1521024827827 {
            margin-top: 0px !important;
            margin-right: 0px !important;
            margin-bottom: 0px !important;
            margin-left: 0px !important;
            border-top-width: 0px !important;
            border-right-width: 0px !important;
            border-bottom-width: 0px !important;
            border-left-width: 0px !important;
            padding-top: 0px !important;
            padding-right: 0px !important;
            padding-bottom: 0px !important;
            padding-left: 0px !important
        }

        .vc_custom_1521024817118 {
            margin-top: 0px !important;
            margin-right: 0px !important;
            margin-bottom: 0px !important;
            margin-left: 0px !important;
            border-top-width: 0px !important;
            border-right-width: 0px !important;
            border-bottom-width: 0px !important;
            border-left-width: 0px !important;
            padding-top: 0px !important;
            padding-right: 0px !important;
            padding-bottom: 0px !important;
            padding-left: 0px !important
        }

        .vc_custom_1521024806488 {
            margin-top: 0px !important;
            margin-right: 0px !important;
            margin-bottom: 0px !important;
            margin-left: 0px !important;
            border-top-width: 0px !important;
            border-right-width: 0px !important;
            border-bottom-width: 0px !important;
            border-left-width: 0px !important;
            padding-top: 0px !important;
            padding-right: 0px !important;
            padding-bottom: 0px !important;
            padding-left: 0px !important
        }

        .vc_custom_1521025102650 {
            margin-top: 0px !important;
            margin-right: 0px !important;
            margin-bottom: 0px !important;
            margin-left: 0px !important;
            border-top-width: 0px !important;
            border-right-width: 0px !important;
            border-bottom-width: 0px !important;
            border-left-width: 0px !important;
            padding-top: 0px !important;
            padding-right: 0px !important;
            padding-bottom: 0px !important;
            padding-left: 0px !important
        }

        .vc_custom_1519748829966 {
            margin-right: 20px !important
        }

        .vc_custom_1521009156765 {
            margin-right: 20px !important
        }</style>
    <noscript>
        <style type="text/css">.wpb_animate_when_almost_visible {
                opacity: 1
            }</style>
    </noscript>
</head>
<body class="home page-template page-template-page-full-width page-template-page-full-width-php page page-id-19152 wpb-js-composer js-comp-ver-4.12 vc_responsive">
<div id="st-container" class="st-container">
    <div class="st-pusher">
        <div class="st-pusher-after"></div>
        <div class="st-content">
            <div id="page_wrapper" class="sticky_header  transparency_dark">
                <div class="top-headers-wrapper">
                    <div id="site-top-bar">
                        <div class="site-top-bar-inner" style="max-width:100%">
                            <div class="language-and-currency"></div>
                            <div class="site-top-message"></div>
                            <div class="site-social-icons-wrapper">
                                <div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                                    <a class="nostyle"
                                       href="https://www.reviews.co.uk/company-reviews/store/academy-class">
                                        <div itemprop="itemReviewed" itemscope itemtype="http://schema.org/WebPage"
                                             style="display: none; height: 0px; width: 0px;"><span itemprop="name">Academy Classs</span>
                                            <span itemprop="description">Market Leaders in Training Courses for Designers and Developers</span>
                                            <span itemprop="url">http://www.academyclass.com/</span></div>
                                        <div class="header-review-body"><span itemprop="ratingValue"
                                                                              data-content="4.73">4.73</span>/<span
                                                    itemprop="bestRating" data-content="5">5</span>
                                            <div class="header-review-stars"></div>
                                        </div>
                                        <div class="header-review-count"><span itemprop="ratingCount"
                                                                               data-content="278">278</span> reviews in
                                            total
                                        </div>
                                    </a></div>
                            </div>
                            <div id="custom_txt"><span id="siteseal"> <img style=""
                                                                           src="https://www.academyclass.com/wp-content/themes/shopkeeper-child/img/mcafee.png"
                                                                           alt="Mcafee logo"> </span></div>
                            <div id="custom_txt2"
                                 style="visibility: hidden; font-size:94%; float: left; display:inline; padding-left: 20px; margin-top: 15px;">
                                Join Designers Fiesta 2017:<a href="http://designersfiesta.com/" target="_blank"
                                                              style="font-size:90%; color:#bfd432 !important; "> 20+
                                    talks in 4 screens</a></div>
                            <nav id="site-navigation-top-bar" class="main-navigation">
                                <ul id="menu-main-menu-top-bar-en">
                                    <li id="menu-item-20078"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-20078">
                                        <a href="https://www.academyclass.com/info/">About Us</a></li>
                                    <li id="menu-item-8093"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8093">
                                        <a href="https://www.academyclass.com/video/">Videos</a></li>
                                    <li id="menu-item-374"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-374">
                                        <a href="https://www.academyclass.com/blog/">Articles, Resources &#038;
                                            Insights</a></li>
                                    <li id="menu-item-8094"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8094">
                                        <a href="https://www.academyclass.com/events/">Events</a></li>
                                    <li id="menu-item-9257"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9257">
                                        <a href="https://www.academyclass.com/newsletter/">Join our Newsletter</a></li>
                                    <li id="menu-item-5834"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5834">
                                        <a href="https://www.academyclass.com/contact-us/">Contact Us</a></li>
                                    <li id="menu-item-6600"
                                        class="gr-link menu-item menu-item-type-custom menu-item-object-custom menu-item-6600">
                                        <a target="_blank" href="tel:+448000438889">0800 043 8889</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <header id="masthead" class="site-header">
                        <div class="site-header-wrapper" style="max-width:100%">
                            <div class="site-branding"><a href="https://www.academyclass.com/" rel="home"> <img
                                            class="site-logo"
                                            src="https://www.academyclass.com/wp-content/uploads/2018/02/Academy-Class-logo-1.png"
                                            title="First Class Training for Designers and Developers"
                                            alt="Academy Class"/> <img class="sticky-logo"
                                                                       src="https://www.academyclass.com/wp-content/uploads/2018/02/Academy-Class-logo.png"
                                                                       title="First Class Training for Designers and Developers"
                                                                       alt="Academy Class"/> </a></div>
                            <div class="site-tools offset align_right">
                                <ul>
                                    <li class="shopping-bag-button"><a href="https://www.academyclass.com/enquiry/"
                                                                       class="tools_button"> <span
                                                    class="tools_button_icon"> <i
                                                        class="fa fa-shopping-cart"></i> </span> <span
                                                    class="shopping_bag_items_number">0</span> </a></li>
                                    <li class="search-button"><a class="tools_button"> <span class="tools_button_icon"> <i
                                                        class="fa fa-search"></i> </span> </a></li>
                                    <li class="offcanvas-menu-button hide-for-large-up"><a class="tools_button"> <span
                                                    class="menu-button-text">menu</span> <span
                                                    class="tools_button_icon"> <i class="fa fa-bars"></i> </span> </a>
                                    </li>
                                </ul>
                            </div>
                            <nav class="show-for-large-up main-navigation default-navigation align_right">
                                <ul class="menu-main-menu-en">
                                    <li id="shopkeeper-menu-item-260"
                                        class="extended-menu menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children">
                                        <a href="https://www.academyclass.com/training/">Courses</a>
                                        <ul class="sub-menu  level-0">
                                            <li id="shopkeeper-menu-item-6524"
                                                class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children">
                                                <a href="https://www.academyclass.com/courses-category/adobe/">Adobe</a>
                                                <ul class="sub-menu  level-1">
                                                    <li id="shopkeeper-menu-item-13319"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="http://www.academyclass.com/courses/adobe-acrobat-pdf-interactivity-form-design/">Acrobat
                                                            Pro DC</a></li>
                                                    <li id="shopkeeper-menu-item-23340"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/adobe/after-effects/">After
                                                            Effects</a></li>
                                                    <li id="shopkeeper-menu-item-8604"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/adobe/adobe-animate/">Animate</a>
                                                    </li>
                                                    <li id="shopkeeper-menu-item-5900"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/adobe/captivate/">Captivate</a>
                                                    </li>
                                                    <li id="shopkeeper-menu-item-5905"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/adobe/creative-cloud/">Creative
                                                            Cloud</a></li>
                                                    <li id="shopkeeper-menu-item-13358"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="http://www.academyclass.com/courses/adobe-user-experience-xd/">Experience
                                                            Design</a></li>
                                                    <li id="shopkeeper-menu-item-23332"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/adobe/illustrator/">Illustrator</a>
                                                    </li>
                                                    <li id="shopkeeper-menu-item-23329"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="https://www.academyclass.com/training/adobe/indesign/">InDesign</a>
                                                    </li>
                                                    <li id="shopkeeper-menu-item-13316"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="http://www.academyclass.com/courses/lightroom/">Lightroom</a>
                                                    </li>
                                                    <li id="shopkeeper-menu-item-5935"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/adobe/muse/">Muse</a>
                                                    </li>
                                                    <li id="shopkeeper-menu-item-23334"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/adobe/photoshop/">Photoshop
                                                            Courses</a></li>
                                                    <li id="shopkeeper-menu-item-23343"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/adobe/premiere-pro/">Premiere
                                                            Pro</a></li>
                                                </ul>
                                            </li>
                                            <li id="shopkeeper-menu-item-6526"
                                                class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children">
                                                <a href="https://www.academyclass.com/courses-category/autodesk/">Autodesk</a>
                                                <ul class="sub-menu  level-1">
                                                    <li id="shopkeeper-menu-item-5950"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/autodesk/3ds-max/">3ds
                                                            Max</a></li>
                                                    <li id="shopkeeper-menu-item-23346"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/autodesk/autocad/">AutoCAD</a>
                                                    </li>
                                                    <li id="shopkeeper-menu-item-8732"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/autodesk/fusion-360/">Fusion
                                                            360</a></li>
                                                    <li id="shopkeeper-menu-item-5955"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/autodesk/inventor/">Inventor</a>
                                                    </li>
                                                    <li id="shopkeeper-menu-item-5957"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/autodesk/building-information-modelling/">Building
                                                            Information Modelling (BIM)</a></li>
                                                    <li id="shopkeeper-menu-item-5960"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/autodesk/maya/">Maya</a>
                                                    </li>
                                                    <li id="shopkeeper-menu-item-5962"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/autodesk/navisworks/">Navisworks</a>
                                                    </li>
                                                    <li id="shopkeeper-menu-item-5964"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/autodesk/revit/">Revit</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li id="shopkeeper-menu-item-12163"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                                                <a href="/courses-category/3dvr/">3D / VR / Games</a>
                                                <ul class="sub-menu  level-1">
                                                    <li id="shopkeeper-menu-item-6565"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/autodesk/3ds-max/">3ds
                                                            Max</a></li>
                                                    <li id="shopkeeper-menu-item-6576"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/maxon/cinema-4d/">Cinema
                                                            4D</a></li>
                                                    <li id="shopkeeper-menu-item-16013"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="https://www.academyclass.com/courses/hololens-101/">HoloLens</a>
                                                    </li>
                                                    <li id="shopkeeper-menu-item-6572"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/autodesk/maya/">Maya</a>
                                                    </li>
                                                    <li id="shopkeeper-menu-item-23348"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/cad-games-3d/unity-3d/">Unity
                                                            3D</a></li>
                                                    <li id="shopkeeper-menu-item-9731"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/cad-games-3d/unreal_engine_4/">Unreal
                                                            Engine 4</a></li>
                                                    <li id="shopkeeper-menu-item-6017"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/cad-games-3d/sketchup/">SketchUp</a>
                                                    </li>
                                                    <li id="shopkeeper-menu-item-8731"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/autodesk/fusion-360/">Fusion
                                                            360</a></li>
                                                    <li id="shopkeeper-menu-item-13796"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="https://www.academyclass.com/courses/zbrush-jumpstart/">Zbrush</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li id="shopkeeper-menu-item-13409"
                                                class="menu-item menu-item-type-custom menu-item-object-custom"><a
                                                        href="http://www.academyclass.com/osx_ios_support/">OSX
                                                    Support</a></li>
                                            <li id="shopkeeper-menu-item-12162"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                                                <a href="/courses-category/cadbim/">CAD / BIM</a>
                                                <ul class="sub-menu  level-1">
                                                    <li id="shopkeeper-menu-item-23347"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/autodesk/autocad/">AutoCAD</a>
                                                    </li>
                                                    <li id="shopkeeper-menu-item-6570"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/autodesk/building-information-modelling/">Building
                                                            Information Modelling (BIM)</a></li>
                                                    <li id="shopkeeper-menu-item-6569"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/autodesk/inventor/">Inventor</a>
                                                    </li>
                                                    <li id="shopkeeper-menu-item-12888"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/autodesk/fusion-360/">Fusion
                                                            360</a></li>
                                                    <li id="shopkeeper-menu-item-12923"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/cad-games-3d/sketchup/">SketchUp</a>
                                                    </li>
                                                    <li id="shopkeeper-menu-item-6573"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/autodesk/navisworks/">Navisworks</a>
                                                    </li>
                                                    <li id="shopkeeper-menu-item-6574"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/autodesk/revit/">Revit</a>
                                                    </li>
                                                    <li id="shopkeeper-menu-item-13592"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="http://www.academyclass.com/vectorworks/">Vectorworks</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li id="shopkeeper-menu-item-13173"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                                                <a href="http://www.academyclass.com/courses-category/coding/">Coding /
                                                    Web</a>
                                                <ul class="sub-menu  level-1">
                                                    <li id="shopkeeper-menu-item-13207"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="http://www.academyclass.com/courses/angular-4-101-yellow-belt/">Angular
                                                            4</a></li>
                                                    <li id="shopkeeper-menu-item-15797"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="https://www.academyclass.com/courses/articulate-storyline/">Articulate
                                                            Storyline</a></li>
                                                    <li id="shopkeeper-menu-item-15597"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="https://www.academyclass.com/courses/articulate-studio-pro-13/">Articulate
                                                            Studio Pro</a></li>
                                                    <li id="shopkeeper-menu-item-13208"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="http://www.academyclass.com/courses/bootstrap-101-yellow-belt/">Bootstrap</a>
                                                    </li>
                                                    <li id="shopkeeper-menu-item-5992"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/web-fundamentals/cascading-stylesheets-css/">Cascading
                                                            StyleSheets (CSS)</a></li>
                                                    <li id="shopkeeper-menu-item-13269"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="http://www.academyclass.com/courses/ecmascript-6/">ECMAScript</a>
                                                    </li>
                                                    <li id="shopkeeper-menu-item-13179"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="http://www.academyclass.com/training/web-fundamentals/html/">HTML</a>
                                                    </li>
                                                    <li id="shopkeeper-menu-item-13180"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="http://www.academyclass.com/training/web-fundamentals/javascript/">JavaScript</a>
                                                    </li>
                                                    <li id="shopkeeper-menu-item-6560"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/adobe/muse/">Muse</a>
                                                    </li>
                                                    <li id="shopkeeper-menu-item-13181"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="http://www.academyclass.com/training/web-fundamentals/nodejs/">NodeJS</a>
                                                    </li>
                                                    <li id="shopkeeper-menu-item-5996"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/web-fundamentals/php-mysql/">PHP
                                                            / MYSQL</a></li>
                                                    <li id="shopkeeper-menu-item-6582"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/design/ux-design/">UX
                                                            Design</a></li>
                                                    <li id="shopkeeper-menu-item-6000"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/web-fundamentals/wordpress/">WordPress</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li id="shopkeeper-menu-item-6528"
                                                class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children">
                                                <a href="https://www.academyclass.com/courses-category/design/">Design</a>
                                                <ul class="sub-menu  level-1">
                                                    <li id="shopkeeper-menu-item-6546"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/adobe/creative-cloud-2/">Creative
                                                            Cloud</a></li>
                                                    <li id="shopkeeper-menu-item-13381"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="http://www.academyclass.com/digital_art/">Digital
                                                            Art</a></li>
                                                    <li id="shopkeeper-menu-item-13174"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="http://www.academyclass.com/courses-category/design/graphic-design-techniques/">Design
                                                            Techniques</a></li>
                                                    <li id="shopkeeper-menu-item-23333"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/adobe/illustrator/">Illustrator</a>
                                                    </li>
                                                    <li id="shopkeeper-menu-item-23330"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="https://www.academyclass.com/training/adobe/indesign/">InDesign</a>
                                                    </li>
                                                    <li id="shopkeeper-menu-item-23335"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/adobe/photoshop/">Photoshop
                                                            Courses</a></li>
                                                    <li id="shopkeeper-menu-item-6004"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/design/ux-design/">UX
                                                            Design</a></li>
                                                </ul>
                                            </li>
                                            <li id="shopkeeper-menu-item-17088"
                                                class="menu-item menu-item-type-custom menu-item-object-custom"><a
                                                        href="https://www.academyclass.com/training/cad-games-3d/unity-3d">Unity</a>
                                            </li>
                                            <li id="shopkeeper-menu-item-17082"
                                                class="menu-item menu-item-type-custom menu-item-object-custom"><a
                                                        href="/training/maxon/cinema-4d/">Maxon</a></li>
                                            <li id="shopkeeper-menu-item-11428"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children">
                                                <a href="https://www.academyclass.com/marketing/">Marketing Classes</a>
                                                <ul class="sub-menu  level-1">
                                                    <li id="shopkeeper-menu-item-14776"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="https://www.academyclass.com/courses/google_analytics/">Google
                                                            Analytics</a></li>
                                                    <li id="shopkeeper-menu-item-17385"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="https://www.academyclass.com/courses/growth-hacking/">Growth
                                                            Hacking</a></li>
                                                    <li id="shopkeeper-menu-item-16219"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="https://www.academyclass.com/courses/how-to-pitch/">How
                                                            To Pitch</a></li>
                                                    <li id="shopkeeper-menu-item-11609"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="https://www.academyclass.com/courses/online-marketing/">Online
                                                            Marketing</a></li>
                                                    <li id="shopkeeper-menu-item-13160"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="http://www.academyclass.com/courses/seo-101-yellow-belt/">SEO</a>
                                                    </li>
                                                    <li id="shopkeeper-menu-item-13161"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="https://www.academyclass.com/social-media/">Social
                                                            Media</a></li>
                                                    <li id="shopkeeper-menu-item-17382"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="https://www.academyclass.com/courses/social-selling/">Social
                                                            Selling</a></li>
                                                    <li id="shopkeeper-menu-item-16227"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="https://www.academyclass.com/courses/upgrade-our-marketing/">Upgrade
                                                            Our Marketing</a></li>
                                                    <li id="shopkeeper-menu-item-16237"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="https://www.academyclass.com/courses/video-works/">Video
                                                            Works</a></li>
                                                    <li id="shopkeeper-menu-item-16233"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="https://www.academyclass.com/courses/writing-content-sticks/">Writing
                                                            Content That Sticks</a></li>
                                                </ul>
                                            </li>
                                            <li id="shopkeeper-menu-item-8845"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children">
                                                <a href="https://www.academyclass.com/professional_development/">Business
                                                    Skills</a>
                                                <ul class="sub-menu  level-1">
                                                    <li id="shopkeeper-menu-item-17391"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="https://www.academyclass.com/courses/business-start-up/">Business
                                                            Start Up</a></li>
                                                    <li id="shopkeeper-menu-item-11562"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="http://www.academyclass.com/courses/confident-customer-service/">Confident
                                                            Customer Service</a></li>
                                                    <li id="shopkeeper-menu-item-16218"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="https://www.academyclass.com/courses/how-to-pitch/">How
                                                            to pitch</a></li>
                                                    <li id="shopkeeper-menu-item-15096"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="https://www.academyclass.com/courses/introduction-agile-project-management/">Introduction
                                                            To Agile Project Management</a></li>
                                                    <li id="shopkeeper-menu-item-11565"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="http://www.academyclass.com/courses/introduction-project-management/">Introduction
                                                            To Project Management</a></li>
                                                    <li id="shopkeeper-menu-item-16224"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="https://www.academyclass.com/courses/leadership-pulling-together/">Pulling
                                                            Together</a></li>
                                                    <li id="shopkeeper-menu-item-14237"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="https://www.academyclass.com/courses/conflict-management-2/">Making
                                                            Time WORK</a></li>
                                                    <li id="shopkeeper-menu-item-15697"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="https://www.academyclass.com/courses/stepping-up-to-management/">Step
                                                            up to Management and Leadership</a></li>
                                                </ul>
                                            </li>
                                            <li id="shopkeeper-menu-item-6539"
                                                class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children">
                                                <a href="https://www.academyclass.com/courses-category/video/">Video /
                                                    Animation</a>
                                                <ul class="sub-menu  level-1">
                                                    <li id="shopkeeper-menu-item-6566"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/autodesk/3ds-max/">3ds
                                                            Max</a></li>
                                                    <li id="shopkeeper-menu-item-23341"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/adobe/after-effects/">After
                                                            Effects</a></li>
                                                    <li id="shopkeeper-menu-item-6575"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/maxon/cinema-4d/">Cinema
                                                            4D</a></li>
                                                    <li id="shopkeeper-menu-item-12960"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/video/davinci-resolve/">DaVinci
                                                            Resolve</a></li>
                                                    <li id="shopkeeper-menu-item-6577"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/apple/final-cut-pro/">Final
                                                            Cut Pro</a></li>
                                                    <li id="shopkeeper-menu-item-6571"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/autodesk/maya/">Maya</a>
                                                    </li>
                                                    <li id="shopkeeper-menu-item-6579"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/apple/motion/">Motion</a>
                                                    </li>
                                                    <li id="shopkeeper-menu-item-14238"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom">
                                                        <a href="https://www.academyclass.com/courses/nuke_jumpstart/">Nuke</a>
                                                    </li>
                                                    <li id="shopkeeper-menu-item-23344"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/adobe/premiere-pro/">Premiere
                                                            Pro</a></li>
                                                    <li id="shopkeeper-menu-item-23349"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page">
                                                        <a href="https://www.academyclass.com/training/cad-games-3d/unity-3d/">Unity
                                                            3D</a></li>
                                                </ul>
                                            </li>
                                            <li id="shopkeeper-menu-item-18888"
                                                class="menu-item menu-item-type-custom menu-item-object-custom"><a
                                                        title="All courses and dates" href="/all-courses-and-dates/">Quick
                                                    courses search</a></li>
                                        </ul>
                                    </li>
                                    <li id="shopkeeper-menu-item-9485"
                                        class="menu-item menu-item-type-post_type menu-item-object-page"><a
                                                href="https://www.academyclass.com/certification/">Certification</a>
                                    </li>
                                    <li id="shopkeeper-menu-item-12"
                                        class="open-offcanvas menu-item menu-item-type-custom menu-item-object-custom">
                                        <a href="#">LEARNING PATHS</a></li>
                                    <li id="shopkeeper-menu-item-13118"
                                        class="menu-item menu-item-type-custom menu-item-object-custom"><a
                                                href="http://www.academyclass.com/training/bespoke-training">Bespoke</a>
                                    </li>
                                    <li id="shopkeeper-menu-item-15"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                                        <a href="/book-online/">Enquire Now</a>
                                        <ul class="sub-menu  level-0">
                                            <li id="shopkeeper-menu-item-6625"
                                                class="menu-item menu-item-type-custom menu-item-object-custom"><a
                                                        target="_blank" href="tel:+448000438889">or call <span>0800 043 8889</span></a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                            <div style="clear:both"></div>
                        </div>
                    </header>
                    <script>jQuery(document).ready(function ($) {

                            "use strict";

                            $(window).scroll(function () {

                                if ($(window).scrollTop() > 0) {

                                    $('#site-top-bar').addClass("hidden");
                                    $('.site-header').addClass("sticky");
                                    $('.site-logo').attr('src', 'https://www.academyclass.com/wp-content/uploads/2018/02/Academy-Class-logo.png');

                                } else {

                                    $('#site-top-bar').removeClass("hidden");
                                    $('.site-header').removeClass("sticky");
                                    $('.site-logo').attr('src', 'https://www.academyclass.com/wp-content/uploads/2018/02/Academy-Class-logo-1.png');

                                }

                            });

                        });</script>
                </div>
                <div class="full-width-page page-title-hidden">
                    <div id="primary" class="content-area">
                        <div id="content" class="site-content" role="main">
                            <header class="entry-header ">
                                <div class="page_header_overlay"></div>
                                <div class="row">
                                    <div class="large-12 columns"></div>
                                </div>
                            </header>
                            <div class="entry-content">
                                <div style=""
                                     class="row normal_height vc_row wpb_row vc_row-fluid vc_custom_1520596794170 vc_row-has-fill">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner vc_custom_1519747991285">
                                            <div class="wpb_wrapper">
                                                <div class="boxed-row">
                                                    <div class="row vc_row wpb_row vc_inner vc_row-fluid">
                                                        <div class="wpb_column vc_column_container vc_col-sm-12">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <div class="top_slider fullwidthbanner-container">
                                                                                <div class="top_slider_content">
                                                                                    <div class="c_row first_row"><h2>
                                                                                            Real Training for Real
                                                                                            People<br/> by Real
                                                                                            Professionals!</h2></div>
                                                                                    <p>&nbsp;</p>
                                                                                    <div class="c_row fourth_row"
                                                                                         style="min-height: 90px;">
                                                                                        <div class="container-22"><a
                                                                                                    href="/courses">Browse
                                                                                                courses</a></div>
                                                                                        <p>&nbsp;</p>
                                                                                        <div class="container-3">
                                                                                            <form role="search"
                                                                                                  action="/"
                                                                                                  method="get"><input
                                                                                                        id="search_form"
                                                                                                        name="s"
                                                                                                        type="search"
                                                                                                        value=""
                                                                                                        placeholder="Search for...."/>
                                                                                                <input name="post_type"
                                                                                                       type="hidden"
                                                                                                       value="product"/>
                                                                                                <input style="display: none;"
                                                                                                       type="submit"
                                                                                                       value="Search"/>
                                                                                            </form>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="" id="featured-courses-landing"
                                     class="row normal_height vc_row wpb_row vc_row-fluid section-landing featured-courses-landing mobiletwo vc_custom_1524059809030 vc_row-has-fill vc_row-o-equal-height vc_row-o-content-top vc_row-flex">
                                    <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-has-fill">
                                        <div class="vc_column-inner vc_custom_1524059558335">
                                            <div class="wpb_wrapper">
                                                <div class="boxed-row">
                                                    <div class="row vc_row wpb_row vc_inner vc_row-fluid vc_custom_1521034847749 vc_row-has-fill">
                                                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-has-fill">
                                                            <div class="vc_column-inner vc_custom_1524059572520">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element  vc_custom_1524059875989">
                                                                        <div class="wpb_wrapper"><h2
                                                                                    style="text-align: center;">Choose
                                                                                From Our Top Training Courses</h2></div>
                                                                    </div>
                                                                    <div class="wpb_raw_code wpb_content_element wpb_raw_html vc_custom_1524059720917">
                                                                        <div class="wpb_wrapper">
                                                                            <div class='icons-container'><a
                                                                                        href="https://www.academyclass.com/training/adobe/photoshop/">
                                                                                    <div style="background-image: url(https://www.academyclass.com/wp-content/uploads/2016/04/icons_37.jpg)"
                                                                                         class="icon-block">
                                                                                        <div class="icon-text">
                                                                                            Photoshop
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                                <a href="https://www.academyclass.com/training/adobe/indesign/">
                                                                                    <div style="background-image: url(https://www.academyclass.com/wp-content/uploads/2016/04/icons_80-1.jpg)"
                                                                                         class="icon-block">
                                                                                        <div class="icon-text">
                                                                                            InDesign
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                                <a href="https://www.academyclass.com/training/adobe/illustrator/">
                                                                                    <div style="background-image: url(https://www.academyclass.com/wp-content/uploads/2016/04/icons_56-1.jpg)"
                                                                                         class="icon-block">
                                                                                        <div class="icon-text">
                                                                                            Illustrator
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                                <a href="https://www.academyclass.com/training/adobe/after-effects/">
                                                                                    <div style="background-image: url(https://www.academyclass.com/wp-content/uploads/2016/05/icons_47.jpg)"
                                                                                         class="icon-block">
                                                                                        <div class="icon-text">After
                                                                                            Effects
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                                <a href="https://www.academyclass.com/training/adobe/premiere-pro/">
                                                                                    <div style="background-image: url(https://www.academyclass.com/wp-content/uploads/2016/04/icons_51.jpg)"
                                                                                         class="icon-block">
                                                                                        <div class="icon-text">Premiere
                                                                                            Pro
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                                <a href="https://www.academyclass.com/training/autodesk/autocad/">
                                                                                    <div style="background-image: url(https://www.academyclass.com/wp-content/uploads/2016/04/icons_57.jpg)"
                                                                                         class="icon-block">
                                                                                        <div class="icon-text">AutoCAD
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                                <a href="https://www.academyclass.com/training/autodesk/revit/">
                                                                                    <div style="background-image: url(https://www.academyclass.com/wp-content/uploads/2016/04/icons_39-1.jpg)"
                                                                                         class="icon-block">
                                                                                        <div class="icon-text">Revit
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                                <a href="https://www.academyclass.com/training/autodesk/3ds-max/">
                                                                                    <div style="background-image: url(https://www.academyclass.com/wp-content/uploads/2016/04/icons_05.jpg)"
                                                                                         class="icon-block">
                                                                                        <div class="icon-text">3ds Max
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                                <a href="https://www.academyclass.com/training/maxon/cinema-4d/">
                                                                                    <div style="background-image: url(https://www.academyclass.com/wp-content/uploads/2016/04/icons_101-1.jpg)"
                                                                                         class="icon-block">
                                                                                        <div class="icon-text">Cinema
                                                                                            4D
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                                <a href="https://www.academyclass.com/training/design/ux-design/">
                                                                                    <div style="background-image: url(https://www.academyclass.com/wp-content/uploads/2016/04/icons_114.jpg)"
                                                                                         class="icon-block">
                                                                                        <div class="icon-text">UX
                                                                                            Design
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                                <a href="https://www.academyclass.com/training/autodesk/maya/">
                                                                                    <div style="background-image: url(https://www.academyclass.com/wp-content/uploads/2016/04/icons_28-2.jpg)"
                                                                                         class="icon-block">
                                                                                        <div class="icon-text">Maya
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                                <a href="https://www.academyclass.com/training/web-fundamentals/html/">
                                                                                    <div style="background-image: url(https://www.academyclass.com/wp-content/uploads/2016/04/icons_59.jpg)"
                                                                                         class="icon-block">
                                                                                        <div class="icon-text">HTML
                                                                                        </div>
                                                                                    </div>
                                                                                </a></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="vc_btn3-container vc_btn3-center vc_custom_1524059936179"><a
                                                            style="background-color:#e8341a; color:#ffffff;"
                                                            class="vc_general vc_btn3 vc_btn3-size-lg vc_btn3-shape-square vc_btn3-style-custom vc_btn3-icon-left"
                                                            href="/courses/" title=""><i
                                                                class="vc_btn3-icon fa fa-th-large"></i> Browse all
                                                        courses</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style=""
                                     class="row normal_height vc_row wpb_row vc_row-fluid vc_custom_1519747927185 vc_row-has-fill">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <div class="boxed-row">
                                                    <div class="row vc_row wpb_row vc_inner vc_row-fluid">
                                                        <div class="wpb_column vc_column_container vc_col-sm-3">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element  vc_custom_1524552191916 parttext">
                                                                        <div class="wpb_wrapper"><h4
                                                                                    style="text-align: right;"><span
                                                                                        style="color: #272727;">Authorised Training Partner for:</span>
                                                                            </h4></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="wpb_column vc_column_container vc_col-sm-9">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper"><p><img
                                                                                        class="size-full wp-image-19190 alignleft"
                                                                                        src="https://www.academyclass.com/wp-content/uploads/2018/02/AC-partnerships.jpg"
                                                                                        alt="" width="809" height="76"/>
                                                                            </p></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="wpb_text_column wpb_content_element ">
                                                    <div class="wpb_wrapper">
                                                        <hr/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style=""
                                     class="row normal_height vc_row wpb_row vc_row-fluid vc_custom_1519741661376">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <div class="boxed-row">
                                                    <div class="row vc_row wpb_row vc_inner vc_row-fluid">
                                                        <div class="wpb_column vc_column_container vc_col-sm-1">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper"></div>
                                                            </div>
                                                        </div>
                                                        <div class="trainingbox wpb_column vc_column_container vc_col-sm-5">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper"><p><img
                                                                                        class="size-full wp-image-19186 alignleft"
                                                                                        src="https://www.academyclass.com/wp-content/uploads/2018/02/Individual-Training.jpg"
                                                                                        alt="" width="190"
                                                                                        height="191"/></p>
                                                                            <h3></h3><h4>Individual Training</h4>
                                                                            <p>Do you want to tone up your skills?
                                                                                Launch a new career or start your own
                                                                                business?<br/> We can help you.<br/> <a
                                                                                        href="https://www.academyclass.com/career-development-solutions-designed-to-meet-your-needs/">Click
                                                                                    here</a></p></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="trainingbox wpb_column vc_column_container vc_col-sm-5">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper"><p><img
                                                                                        class="wp-image-19187 size-full alignleft"
                                                                                        src="https://www.academyclass.com/wp-content/uploads/2018/02/Corporate-Training.jpg"
                                                                                        alt="" width="190"
                                                                                        height="191"/></p><h4>Corporate
                                                                                Training</h4>
                                                                            <p>Send us your Design &amp; Development
                                                                                teams to unleash their full potential
                                                                                and receive great group discounts. <a
                                                                                        href="https://www.academyclass.com/corporate-training/">Learn
                                                                                    how here</a></p></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="wpb_column vc_column_container vc_col-sm-1">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style=""
                                     class="row normal_height vc_row wpb_row vc_row-fluid vc_custom_1524058684918 vc_row-has-fill vc_row-o-equal-height vc_row-o-content-top vc_row-flex">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <div class="boxed-row">
                                                    <div class="row vc_row wpb_row vc_inner vc_row-fluid vc_custom_1524058821436 vc_row-has-fill">
                                                        <div class="wpb_column vc_column_container vc_col-sm-1">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper"></div>
                                                            </div>
                                                        </div>
                                                        <div class="wpb_column vc_column_container vc_col-sm-10 vc_col-has-fill">
                                                            <div class="vc_column-inner vc_custom_1521034960339">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element  vc_custom_1524058463686">
                                                                        <div class="wpb_wrapper"><h2
                                                                                    style="text-align: center;">ACADEMY
                                                                                CLASS</h2>
                                                                            <h3 style="text-align: center;">Market
                                                                                Leaders in Training Courses for
                                                                                Designers and Developers</h3>
                                                                            <p style="text-align: center;">Our training
                                                                                is specifically developed to keep giving
                                                                                you the edge in your career or business.<br/>
                                                                                This is what we like to call &#8220;the
                                                                                how and the now!&#8221;</p></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="wpb_column vc_column_container vc_col-sm-1">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="boxed-row">
                                    <div style=""
                                         class="row normal_height vc_row wpb_row vc_row-fluid vc_custom_1524059960147 vc_row-has-fill vc_row-o-content-top vc_row-flex">
                                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-has-fill">
                                            <div class="vc_column-inner vc_custom_1524059682142">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                                                        <div class="wpb_wrapper">
                                                            <script src="https://widget.reviews.co.uk/carousel-inline/dist.js"></script>
                                                            <div id="carousel-inline-widget-810"
                                                                 style="width:100%;max-width:1000px;margin:0 auto;"></div>
                                                            <script>carouselInlineWidget('carousel-inline-widget-810', {
                                                                    store: 'academy-class',
                                                                    primaryClr: '#BFD331',
                                                                    neutralClr: '#81CBBF',
                                                                    reviewTextClr: '#2f2f2f',
                                                                    ratingTextClr: '#5E368D',
                                                                    layout: 'fullWidth',
                                                                    numReviews: 21
                                                                });</script>
                                                        </div>
                                                    </div>
                                                    <div class="row vc_row wpb_row vc_inner vc_row-fluid">
                                                        <div class="wpb_column vc_column_container vc_col-sm-2">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper"></div>
                                                            </div>
                                                        </div>
                                                        <div class="wpb_column vc_column_container vc_col-sm-4">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                                                                        <div class="wpb_wrapper">
                                                                            <div class="wpb_video_widget wpb_content_element vc_clearfix   vc_video-aspect-ratio-169 vc_video-el-width-100 vc_video-align-left">
                                                                                <div class="wpb_wrapper">
                                                                                    <div class="wpb_video_wrapper">
                                                                                        <div class="video-container">
                                                                                            <iframe width="900"
                                                                                                    height="506"
                                                                                                    src="https://www.youtube.com/embed/Thhwk9t7fhc?feature=oembed"
                                                                                                    allowfullscreen
                                                                                                    style="border-width: 0px;"
                                                                                                    data-rocket-lazyload="fitvidscompatible"
                                                                                                    data-lazy-src="https://www.youtube.com/embed/Thhwk9t7fhc?feature=oembed"></iframe>
                                                                                            <noscript>
                                                                                                <iframe width="900"
                                                                                                        height="506"
                                                                                                        src="https://www.youtube.com/embed/Thhwk9t7fhc?feature=oembed"
                                                                                                        allowfullscreen
                                                                                                        style="border-width: 0px;"></iframe>
                                                                                            </noscript>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="wpb_column vc_column_container vc_col-sm-4">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                                                                        <div class="wpb_wrapper">
                                                                            <div class="wpb_video_widget wpb_content_element vc_clearfix   vc_video-aspect-ratio-169 vc_video-el-width-100 vc_video-align-left">
                                                                                <div class="wpb_wrapper">
                                                                                    <div class="wpb_video_wrapper">
                                                                                        <div class="video-container">
                                                                                            <iframe width="900"
                                                                                                    height="506"
                                                                                                    src="https://www.youtube.com/embed/J-SgrfGg6io?feature=oembed"
                                                                                                    allowfullscreen
                                                                                                    style="border-width: 0px;"
                                                                                                    data-rocket-lazyload="fitvidscompatible"
                                                                                                    data-lazy-src="https://www.youtube.com/embed/J-SgrfGg6io?feature=oembed"></iframe>
                                                                                            <noscript>
                                                                                                <iframe width="900"
                                                                                                        height="506"
                                                                                                        src="https://www.youtube.com/embed/J-SgrfGg6io?feature=oembed"
                                                                                                        allowfullscreen
                                                                                                        style="border-width: 0px;"></iframe>
                                                                                            </noscript>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="wpb_column vc_column_container vc_col-sm-2">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="wpb_text_column wpb_content_element  vc_custom_1524059425122">
                                                        <div class="wpb_wrapper">
                                                            <div id="pl-20177" class="panel-layout">
                                                                <div id="pg-20177-0" class="panel-grid panel-no-style"
                                                                     data-style="{&quot;background_display&quot;:&quot;tile&quot;,&quot;cell_alignment&quot;:&quot;flex-start&quot;}"
                                                                     data-ratio="1" data-ratio-direction="right">
                                                                    <div id="pgc-20177-0-0" class="panel-grid-cell"
                                                                         data-weight="1">
                                                                        <div id="panel-20177-0-0-0"
                                                                             class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                                                             data-index="0"
                                                                             data-style="{&quot;background_display&quot;:&quot;tile&quot;}">
                                                                            <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                                                                <div class="siteorigin-widget-tinymce textwidget">
                                                                                    <h2 style="text-align: center;">
                                                                                        Class Snapshots</h2></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div id="pg-20177-1" class="panel-grid panel-has-style"
                                                                     data-style="{&quot;padding&quot;:&quot;0px 40px 0px 40px&quot;,&quot;mobile_padding&quot;:&quot;0px 0px 0px 0px&quot;,&quot;background_display&quot;:&quot;tile&quot;,&quot;cell_alignment&quot;:&quot;flex-start&quot;}"
                                                                     data-ratio="1" data-ratio-direction="right">
                                                                    <div class="panel-row-style panel-row-style-for-20177-1">
                                                                        <div id="pgc-20177-1-0" class="panel-grid-cell"
                                                                             data-weight="1">
                                                                            <div id="panel-20177-1-0-0"
                                                                                 class="so-panel widget widget_siteorigin-panels-builder panel-first-child panel-last-child"
                                                                                 data-index="1"
                                                                                 data-style="{&quot;background_image_attachment&quot;:false,&quot;background_display&quot;:&quot;tile&quot;}">
                                                                                <div id="pl-w5aaa2926e6075"
                                                                                     class="panel-layout">
                                                                                    <div id="pg-w5aaa2926e6075-0"
                                                                                         class="panel-grid panel-no-style"
                                                                                         data-style="{&quot;id&quot;:&quot;&quot;,&quot;class&quot;:&quot;&quot;,&quot;cell_class&quot;:&quot;&quot;,&quot;row_css&quot;:&quot;&quot;,&quot;mobile_css&quot;:&quot;&quot;,&quot;iw-visible-screen&quot;:&quot;iw-all&quot;,&quot;iw-visible-layout&quot;:&quot;iw-all&quot;,&quot;bottom_margin&quot;:&quot;&quot;,&quot;gutter&quot;:&quot;15px&quot;,&quot;padding&quot;:&quot;&quot;,&quot;mobile_padding&quot;:&quot;&quot;,&quot;row_stretch&quot;:&quot;&quot;,&quot;collapse_behaviour&quot;:&quot;&quot;,&quot;collapse_order&quot;:&quot;&quot;,&quot;cell_alignment&quot;:&quot;flex-start&quot;,&quot;background&quot;:&quot;&quot;,&quot;background_image_attachment&quot;:&quot;0&quot;,&quot;background_display&quot;:&quot;tile&quot;,&quot;border_color&quot;:&quot;&quot;}"
                                                                                         data-ratio="1"
                                                                                         data-ratio-direction="right">
                                                                                        <div id="pgc-w5aaa2926e6075-0-0"
                                                                                             class="panel-grid-cell"
                                                                                             data-weight="0.25">
                                                                                            <div id="panel-w5aaa2926e6075-0-0-0"
                                                                                                 class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                                                                                 data-index="0"
                                                                                                 data-style="{&quot;id&quot;:&quot;&quot;,&quot;class&quot;:&quot;&quot;,&quot;widget_css&quot;:&quot;cursor: pointer!important;&quot;,&quot;mobile_css&quot;:&quot;cursor: pointer!important;&quot;,&quot;padding&quot;:&quot;&quot;,&quot;mobile_padding&quot;:&quot;&quot;,&quot;background&quot;:&quot;&quot;,&quot;background_image_attachment&quot;:&quot;0&quot;,&quot;background_display&quot;:&quot;tile&quot;,&quot;border_color&quot;:&quot;&quot;,&quot;font_color&quot;:&quot;&quot;,&quot;link_color&quot;:&quot;&quot;}">
                                                                                                <div class="panel-widget-style panel-widget-style-for-w5aaa2926e6075-0-0-0">
                                                                                                    <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                                                                                        <div class="siteorigin-widget-tinymce textwidget">
                                                                                                            <span class="su-lightbox"
                                                                                                                  data-mfp-src="https://www.academyclass.com/wp-content/uploads/2018/02/photo-clas-01-1.jpg"
                                                                                                                  data-mfp-type="image"><img
                                                                                                                        src="https://www.academyclass.com/wp-content/uploads/2018/02/photo-clas-01-1.jpg"
                                                                                                                        alt=""
                                                                                                                        width="1000"
                                                                                                                        height="1000"
                                                                                                                        class="alignnone size-full wp-image-19352"/></span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div id="pgc-w5aaa2926e6075-0-1"
                                                                                             class="panel-grid-cell"
                                                                                             data-weight="0.25">
                                                                                            <div id="panel-w5aaa2926e6075-0-1-0"
                                                                                                 class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                                                                                 data-index="1"
                                                                                                 data-style="{&quot;id&quot;:&quot;&quot;,&quot;class&quot;:&quot;&quot;,&quot;widget_css&quot;:&quot;cursor: pointer!important;&quot;,&quot;mobile_css&quot;:&quot;cursor: pointer!important;&quot;,&quot;padding&quot;:&quot;&quot;,&quot;mobile_padding&quot;:&quot;&quot;,&quot;background&quot;:&quot;&quot;,&quot;background_image_attachment&quot;:&quot;0&quot;,&quot;background_display&quot;:&quot;tile&quot;,&quot;border_color&quot;:&quot;&quot;,&quot;font_color&quot;:&quot;&quot;,&quot;link_color&quot;:&quot;&quot;}">
                                                                                                <div class="panel-widget-style panel-widget-style-for-w5aaa2926e6075-0-1-0">
                                                                                                    <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                                                                                        <div class="siteorigin-widget-tinymce textwidget">
                                                                                                            <span class="su-lightbox"
                                                                                                                  data-mfp-src="https://www.academyclass.com/wp-content/uploads/2018/02/photo-clas-02.jpg"
                                                                                                                  data-mfp-type="image"><img
                                                                                                                        src="https://www.academyclass.com/wp-content/uploads/2018/02/photo-clas-02.jpg"
                                                                                                                        alt=""
                                                                                                                        width="1000"
                                                                                                                        height="1000"
                                                                                                                        class="alignnone size-full wp-image-19352"/></span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div id="pgc-w5aaa2926e6075-0-2"
                                                                                             class="panel-grid-cell"
                                                                                             data-weight="0.25">
                                                                                            <div id="panel-w5aaa2926e6075-0-2-0"
                                                                                                 class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                                                                                 data-index="2"
                                                                                                 data-style="{&quot;id&quot;:&quot;&quot;,&quot;class&quot;:&quot;&quot;,&quot;widget_css&quot;:&quot;cursor: pointer!important;&quot;,&quot;mobile_css&quot;:&quot;cursor: pointer!important;&quot;,&quot;padding&quot;:&quot;&quot;,&quot;mobile_padding&quot;:&quot;&quot;,&quot;background&quot;:&quot;&quot;,&quot;background_image_attachment&quot;:&quot;0&quot;,&quot;background_display&quot;:&quot;tile&quot;,&quot;border_color&quot;:&quot;&quot;,&quot;font_color&quot;:&quot;&quot;,&quot;link_color&quot;:&quot;&quot;}">
                                                                                                <div class="panel-widget-style panel-widget-style-for-w5aaa2926e6075-0-2-0">
                                                                                                    <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                                                                                        <div class="siteorigin-widget-tinymce textwidget">
                                                                                                            <span class="su-lightbox"
                                                                                                                  data-mfp-src="https://www.academyclass.com/wp-content/uploads/2018/02/photo-clas-03-1.jpg"
                                                                                                                  data-mfp-type="image"><img
                                                                                                                        src="https://www.academyclass.com/wp-content/uploads/2018/02/photo-clas-03-1.jpg"
                                                                                                                        alt=""
                                                                                                                        width="1000"
                                                                                                                        height="1000"
                                                                                                                        class="alignnone size-full wp-image-19352"/></span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div id="pgc-w5aaa2926e6075-0-3"
                                                                                             class="panel-grid-cell"
                                                                                             data-weight="0.25">
                                                                                            <div id="panel-w5aaa2926e6075-0-3-0"
                                                                                                 class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                                                                                 data-index="3"
                                                                                                 data-style="{&quot;id&quot;:&quot;&quot;,&quot;class&quot;:&quot;&quot;,&quot;widget_css&quot;:&quot;cursor: pointer!important;&quot;,&quot;mobile_css&quot;:&quot;cursor: pointer!important;&quot;,&quot;padding&quot;:&quot;&quot;,&quot;mobile_padding&quot;:&quot;&quot;,&quot;background&quot;:&quot;&quot;,&quot;background_image_attachment&quot;:&quot;0&quot;,&quot;background_display&quot;:&quot;tile&quot;,&quot;border_color&quot;:&quot;&quot;,&quot;font_color&quot;:&quot;&quot;,&quot;link_color&quot;:&quot;&quot;}">
                                                                                                <div class="panel-widget-style panel-widget-style-for-w5aaa2926e6075-0-3-0">
                                                                                                    <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                                                                                        <div class="siteorigin-widget-tinymce textwidget">
                                                                                                            <span class="su-lightbox"
                                                                                                                  data-mfp-src="https://www.academyclass.com/wp-content/uploads/2018/02/photo-clas-09.jpg"
                                                                                                                  data-mfp-type="image"><img
                                                                                                                        src="https://www.academyclass.com/wp-content/uploads/2018/02/photo-clas-09.jpg"
                                                                                                                        alt=""
                                                                                                                        width="1000"
                                                                                                                        height="1000"
                                                                                                                        class="alignnone size-full wp-image-19352"/></span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="pg-w5aaa2926e6075-1"
                                                                                         class="panel-grid panel-no-style"
                                                                                         data-style="{&quot;id&quot;:&quot;&quot;,&quot;class&quot;:&quot;&quot;,&quot;cell_class&quot;:&quot;&quot;,&quot;row_css&quot;:&quot;&quot;,&quot;mobile_css&quot;:&quot;&quot;,&quot;iw-visible-screen&quot;:&quot;iw-all&quot;,&quot;iw-visible-layout&quot;:&quot;iw-all&quot;,&quot;bottom_margin&quot;:&quot;&quot;,&quot;gutter&quot;:&quot;15px&quot;,&quot;padding&quot;:&quot;&quot;,&quot;mobile_padding&quot;:&quot;&quot;,&quot;row_stretch&quot;:&quot;&quot;,&quot;collapse_behaviour&quot;:&quot;&quot;,&quot;collapse_order&quot;:&quot;&quot;,&quot;cell_alignment&quot;:&quot;flex-start&quot;,&quot;background&quot;:&quot;&quot;,&quot;background_image_attachment&quot;:&quot;0&quot;,&quot;background_display&quot;:&quot;tile&quot;,&quot;border_color&quot;:&quot;&quot;}"
                                                                                         data-ratio="1"
                                                                                         data-ratio-direction="right">
                                                                                        <div id="pgc-w5aaa2926e6075-1-0"
                                                                                             class="panel-grid-cell"
                                                                                             data-weight="0.25">
                                                                                            <div id="panel-w5aaa2926e6075-1-0-0"
                                                                                                 class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                                                                                 data-index="4"
                                                                                                 data-style="{&quot;id&quot;:&quot;&quot;,&quot;class&quot;:&quot;&quot;,&quot;widget_css&quot;:&quot;cursor: pointer!important;&quot;,&quot;mobile_css&quot;:&quot;cursor: pointer!important;&quot;,&quot;padding&quot;:&quot;&quot;,&quot;mobile_padding&quot;:&quot;&quot;,&quot;background&quot;:&quot;&quot;,&quot;background_image_attachment&quot;:&quot;0&quot;,&quot;background_display&quot;:&quot;tile&quot;,&quot;border_color&quot;:&quot;&quot;,&quot;font_color&quot;:&quot;&quot;,&quot;link_color&quot;:&quot;&quot;}">
                                                                                                <div class="panel-widget-style panel-widget-style-for-w5aaa2926e6075-1-0-0">
                                                                                                    <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                                                                                        <div class="siteorigin-widget-tinymce textwidget">
                                                                                                            <span class="su-lightbox"
                                                                                                                  data-mfp-src="https://www.academyclass.com/wp-content/uploads/2018/02/photo-clas-04.jpg"
                                                                                                                  data-mfp-type="image"><img
                                                                                                                        src="https://www.academyclass.com/wp-content/uploads/2018/02/photo-clas-04.jpg"
                                                                                                                        alt=""
                                                                                                                        width="1000"
                                                                                                                        height="1000"
                                                                                                                        class="alignnone size-full wp-image-19352"/></span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div id="pgc-w5aaa2926e6075-1-1"
                                                                                             class="panel-grid-cell"
                                                                                             data-weight="0.25">
                                                                                            <div id="panel-w5aaa2926e6075-1-1-0"
                                                                                                 class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                                                                                 data-index="5"
                                                                                                 data-style="{&quot;id&quot;:&quot;&quot;,&quot;class&quot;:&quot;&quot;,&quot;widget_css&quot;:&quot;cursor: pointer!important;&quot;,&quot;mobile_css&quot;:&quot;cursor: pointer!important;&quot;,&quot;padding&quot;:&quot;&quot;,&quot;mobile_padding&quot;:&quot;&quot;,&quot;background&quot;:&quot;&quot;,&quot;background_image_attachment&quot;:&quot;0&quot;,&quot;background_display&quot;:&quot;tile&quot;,&quot;border_color&quot;:&quot;&quot;,&quot;font_color&quot;:&quot;&quot;,&quot;link_color&quot;:&quot;&quot;}">
                                                                                                <div class="panel-widget-style panel-widget-style-for-w5aaa2926e6075-1-1-0">
                                                                                                    <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                                                                                        <div class="siteorigin-widget-tinymce textwidget">
                                                                                                            <span class="su-lightbox"
                                                                                                                  data-mfp-src="https://www.academyclass.com/wp-content/uploads/2018/02/photo-clas-05.jpg"
                                                                                                                  data-mfp-type="image"><img
                                                                                                                        src="https://www.academyclass.com/wp-content/uploads/2018/02/photo-clas-05.jpg"
                                                                                                                        alt=""
                                                                                                                        width="1000"
                                                                                                                        height="1000"
                                                                                                                        class="alignnone size-full wp-image-19352"/></span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div id="pgc-w5aaa2926e6075-1-2"
                                                                                             class="panel-grid-cell"
                                                                                             data-weight="0.25">
                                                                                            <div id="panel-w5aaa2926e6075-1-2-0"
                                                                                                 class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                                                                                 data-index="6"
                                                                                                 data-style="{&quot;id&quot;:&quot;&quot;,&quot;class&quot;:&quot;&quot;,&quot;widget_css&quot;:&quot;cursor: pointer!important;&quot;,&quot;mobile_css&quot;:&quot;cursor: pointer!important;&quot;,&quot;padding&quot;:&quot;&quot;,&quot;mobile_padding&quot;:&quot;&quot;,&quot;background&quot;:&quot;&quot;,&quot;background_image_attachment&quot;:&quot;0&quot;,&quot;background_display&quot;:&quot;tile&quot;,&quot;border_color&quot;:&quot;&quot;,&quot;font_color&quot;:&quot;&quot;,&quot;link_color&quot;:&quot;&quot;}">
                                                                                                <div class="panel-widget-style panel-widget-style-for-w5aaa2926e6075-1-2-0">
                                                                                                    <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                                                                                        <div class="siteorigin-widget-tinymce textwidget">
                                                                                                            <span class="su-lightbox"
                                                                                                                  data-mfp-src="https://www.academyclass.com/wp-content/uploads/2018/02/photo-clas-06.jpg"
                                                                                                                  data-mfp-type="image"><img
                                                                                                                        src="https://www.academyclass.com/wp-content/uploads/2018/02/photo-clas-06.jpg"
                                                                                                                        alt=""
                                                                                                                        width="1000"
                                                                                                                        height="1000"
                                                                                                                        class="alignnone size-full wp-image-19352"/></span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div id="pgc-w5aaa2926e6075-1-3"
                                                                                             class="panel-grid-cell"
                                                                                             data-weight="0.25">
                                                                                            <div id="panel-w5aaa2926e6075-1-3-0"
                                                                                                 class="so-panel widget widget_sow-image panel-first-child panel-last-child"
                                                                                                 data-index="7"
                                                                                                 data-style="{&quot;id&quot;:&quot;&quot;,&quot;class&quot;:&quot;&quot;,&quot;widget_css&quot;:&quot;&quot;,&quot;mobile_css&quot;:&quot;&quot;,&quot;iw-visible-screen&quot;:&quot;iw-all&quot;,&quot;iw-visible-layout&quot;:&quot;iw-all&quot;,&quot;padding&quot;:&quot;&quot;,&quot;mobile_padding&quot;:&quot;&quot;,&quot;background&quot;:&quot;&quot;,&quot;background_image_attachment&quot;:&quot;0&quot;,&quot;background_display&quot;:&quot;tile&quot;,&quot;border_color&quot;:&quot;&quot;,&quot;font_color&quot;:&quot;&quot;,&quot;link_color&quot;:&quot;&quot;}">
                                                                                                <div class="so-widget-sow-image so-widget-sow-image-default-b37b538aacbf">
                                                                                                    <div class="sow-image-container">
                                                                                                        <a href="https://www.academyclass.com/courses/">
                                                                                                            <img src="https://www.academyclass.com/wp-content/uploads/2018/02/photo-clas-08.jpg"
                                                                                                                 width="500"
                                                                                                                 height="500"
                                                                                                                 sizes="(max-width: 500px) 100vw, 500px"
                                                                                                                 class="so-widget-image"/>
                                                                                                        </a></div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <style type="text/css"
                                                                                       class="panels-style"
                                                                                       data-panels-style-for-post="w5aaa2926e6075">@import url(https://www.academyclass.com/wp-content/plugins/siteorigin-panels/css/front-flex.min.css);

                                                                                    #pgc-w5aaa2926e6075-0-0, #pgc-w5aaa2926e6075-0-1, #pgc-w5aaa2926e6075-0-2, #pgc-w5aaa2926e6075-0-3, #pgc-w5aaa2926e6075-1-0, #pgc-w5aaa2926e6075-1-1, #pgc-w5aaa2926e6075-1-2, #pgc-w5aaa2926e6075-1-3 {
                                                                                        width: 25%;
                                                                                        width: calc(25% - (0.75 * 15px))
                                                                                    }

                                                                                    #pg-w5aaa2926e6075-0, #pl-w5aaa2926e6075 .so-panel {
                                                                                        margin-bottom: 30px
                                                                                    }

                                                                                    #pl-w5aaa2926e6075 .so-panel:last-child {
                                                                                        margin-bottom: 0
                                                                                    }

                                                                                    #pg-w5aaa2926e6075-0.panel-no-style, #pg-w5aaa2926e6075-0.panel-has-style > .panel-row-style, #pg-w5aaa2926e6075-1.panel-no-style, #pg-w5aaa2926e6075-1.panel-has-style > .panel-row-style {
                                                                                        -webkit-align-items: flex-start;
                                                                                        align-items: flex-start
                                                                                    }

                                                                                    #panel-w5aaa2926e6075-0-0-0 > .panel-widget-style, #panel-w5aaa2926e6075-0-1-0 > .panel-widget-style, #panel-w5aaa2926e6075-0-2-0 > .panel-widget-style, #panel-w5aaa2926e6075-0-3-0 > .panel-widget-style, #panel-w5aaa2926e6075-1-0-0 > .panel-widget-style, #panel-w5aaa2926e6075-1-1-0 > .panel-widget-style, #panel-w5aaa2926e6075-1-2-0 > .panel-widget-style {
                                                                                        cursor: pointer !important
                                                                                    }

                                                                                    @media (max-width: 780px) {
                                                                                        #pg-w5aaa2926e6075-0.panel-no-style, #pg-w5aaa2926e6075-0.panel-has-style > .panel-row-style, #pg-w5aaa2926e6075-1.panel-no-style, #pg-w5aaa2926e6075-1.panel-has-style > .panel-row-style {
                                                                                            -webkit-flex-direction: column;
                                                                                            -ms-flex-direction: column;
                                                                                            flex-direction: column
                                                                                        }

                                                                                        #pg-w5aaa2926e6075-0 .panel-grid-cell, #pg-w5aaa2926e6075-1 .panel-grid-cell {
                                                                                            margin-right: 0
                                                                                        }

                                                                                        #pg-w5aaa2926e6075-0 .panel-grid-cell, #pg-w5aaa2926e6075-1 .panel-grid-cell {
                                                                                            width: 100%
                                                                                        }

                                                                                        #pgc-w5aaa2926e6075-0-0, #pgc-w5aaa2926e6075-0-1, #pgc-w5aaa2926e6075-0-2, #pgc-w5aaa2926e6075-1-0, #pgc-w5aaa2926e6075-1-1, #pgc-w5aaa2926e6075-1-2 {
                                                                                            margin-bottom: 30px
                                                                                        }

                                                                                        #pl-w5aaa2926e6075 .panel-grid-cell {
                                                                                            padding: 0
                                                                                        }

                                                                                        #pl-w5aaa2926e6075 .panel-grid .panel-grid-cell-empty {
                                                                                            display: none
                                                                                        }

                                                                                        #pl-w5aaa2926e6075 .panel-grid .panel-grid-cell-mobile-last {
                                                                                            margin-bottom: 0
                                                                                        }

                                                                                        #panel-w5aaa2926e6075-0-0-0 > .panel-widget-style, #panel-w5aaa2926e6075-0-1-0 > .panel-widget-style, #panel-w5aaa2926e6075-0-2-0 > .panel-widget-style, #panel-w5aaa2926e6075-0-3-0 > .panel-widget-style, #panel-w5aaa2926e6075-1-0-0 > .panel-widget-style, #panel-w5aaa2926e6075-1-1-0 > .panel-widget-style, #panel-w5aaa2926e6075-1-2-0 > .panel-widget-style {
                                                                                            cursor: pointer !important
                                                                                        }
                                                                                    }</style>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <style type="text/css" class="panels-style"
                                                                   data-panels-style-for-post="20177">@import url(https://www.academyclass.com/wp-content/plugins/siteorigin-panels/css/front-flex.min.css);

                                                                #pgc-20177-0-0, #pgc-20177-1-0 {
                                                                    width: 100%;
                                                                    width: calc(100% - (0 * 30px))
                                                                }

                                                                #pg-20177-0, #pl-20177 .so-panel {
                                                                    margin-bottom: 30px
                                                                }

                                                                #pl-20177 .so-panel:last-child {
                                                                    margin-bottom: 0
                                                                }

                                                                #pg-20177-0.panel-no-style, #pg-20177-0.panel-has-style > .panel-row-style, #pg-20177-1.panel-no-style, #pg-20177-1.panel-has-style > .panel-row-style {
                                                                    -webkit-align-items: flex-start;
                                                                    align-items: flex-start
                                                                }

                                                                #pg-20177-1 > .panel-row-style {
                                                                    padding: 0 40px 0 40px
                                                                }

                                                                @media (max-width: 780px) {
                                                                    #pg-20177-0.panel-no-style, #pg-20177-0.panel-has-style > .panel-row-style, #pg-20177-1.panel-no-style, #pg-20177-1.panel-has-style > .panel-row-style {
                                                                        -webkit-flex-direction: column;
                                                                        -ms-flex-direction: column;
                                                                        flex-direction: column
                                                                    }

                                                                    #pg-20177-0 .panel-grid-cell, #pg-20177-1 .panel-grid-cell {
                                                                        margin-right: 0
                                                                    }

                                                                    #pg-20177-0 .panel-grid-cell, #pg-20177-1 .panel-grid-cell {
                                                                        width: 100%
                                                                    }

                                                                    #pl-20177 .panel-grid-cell {
                                                                        padding: 0
                                                                    }

                                                                    #pl-20177 .panel-grid .panel-grid-cell-empty {
                                                                        display: none
                                                                    }

                                                                    #pl-20177 .panel-grid .panel-grid-cell-mobile-last {
                                                                        margin-bottom: 0
                                                                    }

                                                                    #pg-20177-1 > .panel-row-style {
                                                                        padding: 0 0 0 0
                                                                    }
                                                                }</style>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style=""
                                     class="row normal_height vc_row wpb_row vc_row-fluid vc_custom_1524059239797">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element ">
                                                    <div class="wpb_wrapper"><h2 style="text-align: center;"><span
                                                                    style="color: #333333;">Our Clients</span></h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="" class="row normal_height vc_row wpb_row vc_row-fluid partners-slider">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1480082500745">
                                                    <figure class="wpb_wrapper vc_figure">
                                                        <div class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                                    width="150" height="150"
                                                                    src="https://www.academyclass.com/wp-content/uploads/2016/03/bbc-big-logo.jpg"
                                                                    class="vc_single_image-img attachment-thumbnail"
                                                                    alt=""/></div>
                                                    </figure>
                                                </div>
                                                <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1480082530336">
                                                    <figure class="wpb_wrapper vc_figure">
                                                        <div class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                                    width="150" height="150"
                                                                    src="https://www.academyclass.com/wp-content/uploads/2016/11/client_slider13.png"
                                                                    class="vc_single_image-img attachment-thumbnail"
                                                                    alt=""/></div>
                                                    </figure>
                                                </div>
                                                <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1480082561140">
                                                    <figure class="wpb_wrapper vc_figure">
                                                        <div class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                                    width="150" height="150"
                                                                    src="https://www.academyclass.com/wp-content/uploads/2016/11/client_slider8.png"
                                                                    class="vc_single_image-img attachment-thumbnail"
                                                                    alt=""/></div>
                                                    </figure>
                                                </div>
                                                <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1480082583233">
                                                    <figure class="wpb_wrapper vc_figure">
                                                        <div class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                                    width="150" height="150"
                                                                    src="https://www.academyclass.com/wp-content/uploads/2016/11/client_slider7.png"
                                                                    class="vc_single_image-img attachment-thumbnail"
                                                                    alt=""/></div>
                                                    </figure>
                                                </div>
                                                <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1480082600430">
                                                    <figure class="wpb_wrapper vc_figure">
                                                        <div class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                                    width="150" height="150"
                                                                    src="https://www.academyclass.com/wp-content/uploads/2016/03/nvidia-big-logo.jpg"
                                                                    class="vc_single_image-img attachment-thumbnail"
                                                                    alt=""/></div>
                                                    </figure>
                                                </div>
                                                <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1480082613142">
                                                    <figure class="wpb_wrapper vc_figure">
                                                        <div class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                                    width="150" height="150"
                                                                    src="https://www.academyclass.com/wp-content/uploads/2016/11/client_slider3.png"
                                                                    class="vc_single_image-img attachment-thumbnail"
                                                                    alt=""/></div>
                                                    </figure>
                                                </div>
                                                <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1480082626207">
                                                    <figure class="wpb_wrapper vc_figure">
                                                        <div class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                                    width="150" height="150"
                                                                    src="https://www.academyclass.com/wp-content/uploads/2016/03/winkworth-big-logo.jpg"
                                                                    class="vc_single_image-img attachment-thumbnail"
                                                                    alt=""/></div>
                                                    </figure>
                                                </div>
                                                <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1480082641876">
                                                    <figure class="wpb_wrapper vc_figure">
                                                        <div class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                                    width="150" height="150"
                                                                    src="https://www.academyclass.com/wp-content/uploads/2016/11/client_slider14.png"
                                                                    class="vc_single_image-img attachment-thumbnail"
                                                                    alt=""/></div>
                                                    </figure>
                                                </div>
                                                <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1480082703393">
                                                    <figure class="wpb_wrapper vc_figure">
                                                        <div class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                                    width="150" height="150"
                                                                    src="https://www.academyclass.com/wp-content/uploads/2016/11/client_slider10.png"
                                                                    class="vc_single_image-img attachment-thumbnail"
                                                                    alt=""/></div>
                                                    </figure>
                                                </div>
                                                <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1480082715270">
                                                    <figure class="wpb_wrapper vc_figure">
                                                        <div class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                                    width="150" height="150"
                                                                    src="https://www.academyclass.com/wp-content/uploads/2016/11/client_slider11.png"
                                                                    class="vc_single_image-img attachment-thumbnail"
                                                                    alt=""/></div>
                                                    </figure>
                                                </div>
                                                <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1480082769154">
                                                    <figure class="wpb_wrapper vc_figure">
                                                        <div class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                                    width="150" height="150"
                                                                    src="https://www.academyclass.com/wp-content/uploads/2016/11/client_slider12.png"
                                                                    class="vc_single_image-img attachment-thumbnail"
                                                                    alt=""/></div>
                                                    </figure>
                                                </div>
                                                <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1480082787455">
                                                    <figure class="wpb_wrapper vc_figure">
                                                        <div class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                                    width="150" height="150"
                                                                    src="https://www.academyclass.com/wp-content/uploads/2016/11/client_slider5.png"
                                                                    class="vc_single_image-img attachment-thumbnail"
                                                                    alt=""/></div>
                                                    </figure>
                                                </div>
                                                <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1480082801892">
                                                    <figure class="wpb_wrapper vc_figure">
                                                        <div class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                                    width="150" height="150"
                                                                    src="https://www.academyclass.com/wp-content/uploads/2016/11/client_slider6.png"
                                                                    class="vc_single_image-img attachment-thumbnail"
                                                                    alt=""/></div>
                                                    </figure>
                                                </div>
                                                <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1480082814615">
                                                    <figure class="wpb_wrapper vc_figure">
                                                        <div class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                                    width="150" height="150"
                                                                    src="https://www.academyclass.com/wp-content/uploads/2016/11/client_slider1.png"
                                                                    class="vc_single_image-img attachment-thumbnail"
                                                                    alt=""/></div>
                                                    </figure>
                                                </div>
                                                <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1480082836370">
                                                    <figure class="wpb_wrapper vc_figure">
                                                        <div class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                                    width="150" height="150"
                                                                    src="https://www.academyclass.com/wp-content/uploads/2016/11/client_slider2.png"
                                                                    class="vc_single_image-img attachment-thumbnail"
                                                                    alt=""/></div>
                                                    </figure>
                                                </div>
                                                <div class="wpb_single_image wpb_content_element vc_align_center">
                                                    <figure class="wpb_wrapper vc_figure">
                                                        <div class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                                    width="150" height="150"
                                                                    src="https://www.academyclass.com/wp-content/uploads/2016/11/client_slider9.png"
                                                                    class="vc_single_image-img attachment-thumbnail"
                                                                    alt=""/></div>
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="boxed-row">
                                    <div style=""
                                         class="row normal_height vc_row wpb_row vc_row-fluid vc_custom_1524059123118 vc_row-o-content-middle vc_row-flex">
                                        <div class="wpb_column vc_column_container vc_col-sm-12">
                                            <div class="vc_column-inner ">
                                                <div class="wpb_wrapper">
                                                    <div class="row vc_row wpb_row vc_inner vc_row-fluid">
                                                        <div class="wpb_column vc_column_container vc_col-sm-12">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper"><h2
                                                                                    style="text-align: center;">
                                                                                Whichever Way You Want To Learn</h2>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="boxed-row">
                                                        <div class="row vc_row wpb_row vc_inner vc_row-fluid vc_custom_1521025116116 vc_row-has-fill">
                                                            <div class="learnbox wpb_column vc_column_container vc_col-sm-3 vc_col-has-fill">
                                                                <div class="vc_column-inner vc_custom_1521024841845">
                                                                    <div class="wpb_wrapper">
                                                                        <div class="wpb_text_column wpb_content_element  vc_custom_1521024827827">
                                                                            <div class="wpb_wrapper"><p><img
                                                                                            class="alignnone size-full wp-image-19414"
                                                                                            src="https://www.academyclass.com/wp-content/uploads/2018/02/cb.jpg"
                                                                                            alt="" width="23"
                                                                                            height="19"/> <strong>Classroom</strong>
                                                                                </p></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="learnbox wpb_column vc_column_container vc_col-sm-3 vc_col-has-fill">
                                                                <div class="vc_column-inner vc_custom_1521024187747">
                                                                    <div class="wpb_wrapper">
                                                                        <div class="wpb_text_column wpb_content_element  vc_custom_1521024817118">
                                                                            <div class="wpb_wrapper"><p><img
                                                                                            class="alignnone size-full wp-image-19414"
                                                                                            src="https://www.academyclass.com/wp-content/uploads/2018/02/cb.jpg"
                                                                                            alt="" width="23"
                                                                                            height="19"/> <strong>Customised</strong>
                                                                                </p></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="learnbox wpb_column vc_column_container vc_col-sm-3 vc_col-has-fill">
                                                                <div class="vc_column-inner vc_custom_1521024768651">
                                                                    <div class="wpb_wrapper">
                                                                        <div class="wpb_text_column wpb_content_element  vc_custom_1521024806488">
                                                                            <div class="wpb_wrapper"><p><img
                                                                                            class="alignnone size-full wp-image-19414"
                                                                                            src="https://www.academyclass.com/wp-content/uploads/2018/02/cb.jpg"
                                                                                            alt="" width="23"
                                                                                            height="19"/> <strong>Live-Online</strong>
                                                                                </p></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="learnbox wpb_column vc_column_container vc_col-sm-3 vc_col-has-fill">
                                                                <div class="vc_column-inner vc_custom_1521024199246">
                                                                    <div class="wpb_wrapper">
                                                                        <div class="wpb_text_column wpb_content_element  vc_custom_1521025102650">
                                                                            <div class="wpb_wrapper"><p><img
                                                                                            class="alignnone size-full wp-image-19414"
                                                                                            src="https://www.academyclass.com/wp-content/uploads/2018/02/cb.jpg"
                                                                                            alt="" width="23"
                                                                                            height="19"/> <strong>Individual
                                                                                        / Corporate</strong></p></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style=""
                                     class="row normal_height vc_row wpb_row vc_row-fluid vc_custom_1492074820118 vc_row-has-fill">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <div class="row vc_row wpb_row vc_inner vc_row-fluid">
                                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                                        <div class="vc_column-inner ">
                                                            <div class="wpb_wrapper">
                                                                <div class="wpb_text_column wpb_content_element ">
                                                                    <div class="wpb_wrapper"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="boxed-row">
                                    <div style="" class="row normal_height vc_row wpb_row vc_row-fluid">
                                        <div class="wpb_column vc_column_container vc_col-sm-12">
                                            <div class="vc_column-inner ">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element ">
                                                        <div class="wpb_wrapper">
                                                            <div id="pl-20164" class="panel-layout">
                                                                <div id="pg-20164-0" class="panel-grid panel-has-style"
                                                                     data-style="{&quot;background&quot;:&quot;#749f00&quot;,&quot;background_display&quot;:&quot;tile&quot;,&quot;cell_alignment&quot;:&quot;flex-start&quot;}">
                                                                    <div class="panel-row-style panel-row-style-for-20164-0">
                                                                        <div id="pgc-20164-0-0" class="panel-grid-cell"
                                                                             data-weight="0.5">
                                                                            <div id="panel-20164-0-0-0"
                                                                                 class="so-panel widget widget_sow-image panel-first-child panel-last-child"
                                                                                 data-index="0"
                                                                                 data-style="{&quot;background_display&quot;:&quot;tile&quot;}">
                                                                                <div class="so-widget-sow-image so-widget-sow-image-default-9e90369adebe">
                                                                                    <div class="sow-image-container">
                                                                                        <img src="https://www.academyclass.com/wp-content/uploads/2018/02/After-the-course.jpg"
                                                                                             width="609" height="463"
                                                                                             sizes="(max-width: 609px) 100vw, 609px"
                                                                                             class="so-widget-image"/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div id="pgc-20164-0-1" class="panel-grid-cell"
                                                                             data-weight="0.5">
                                                                            <div id="panel-20164-0-1-0"
                                                                                 class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                                                                 data-index="1"
                                                                                 data-style="{&quot;padding&quot;:&quot;30px 0px 0px 0px&quot;,&quot;mobile_padding&quot;:&quot;0px 0px 0px 0px&quot;,&quot;background_image_attachment&quot;:false,&quot;background_display&quot;:&quot;tile&quot;,&quot;font_color&quot;:&quot;#ffffff&quot;,&quot;link_color&quot;:&quot;#ffffff&quot;}">
                                                                                <div class="panel-widget-style panel-widget-style-for-20164-0-1-0">
                                                                                    <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                                                                        <div class="siteorigin-widget-tinymce textwidget">
                                                                                            <h3 class="headingwhite">
                                                                                                After our courses you
                                                                                                will:</h3>
                                                                                            <ul class="b">
                                                                                                <li>Improve your
                                                                                                    professional skill
                                                                                                    set
                                                                                                </li>
                                                                                                <li>Build and grow your
                                                                                                    portfolio
                                                                                                </li>
                                                                                                <li>Solve complex
                                                                                                    challenges people
                                                                                                    typically face
                                                                                                </li>
                                                                                                <li>Get great tips and
                                                                                                    tricks from
                                                                                                    practicing,
                                                                                                    professional
                                                                                                    instructors
                                                                                                </li>
                                                                                                <li>Increase your
                                                                                                    confidence to
                                                                                                    develop your own
                                                                                                    business
                                                                                                </li>
                                                                                                <li><strong>Become an
                                                                                                        accredited
                                                                                                        specialist</strong>
                                                                                                    (ace, aca, acu, acp,
                                                                                                    etc).<br/> Read more
                                                                                                    here
                                                                                                </li>
                                                                                                <li><strong>Get digital
                                                                                                        certificates</strong><br/>
                                                                                                    (Great for uploading
                                                                                                    to your LinkedIn
                                                                                                    profile and other
                                                                                                    social media)
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <style type="text/css" class="panels-style"
                                                                   data-panels-style-for-post="20164">@import url(https://www.academyclass.com/wp-content/plugins/siteorigin-panels/css/front-flex.min.css);

                                                                #pgc-20164-0-0, #pgc-20164-0-1 {
                                                                    width: 50%;
                                                                    width: calc(50% - (0.5 * 30px))
                                                                }

                                                                #pl-20164 .so-panel {
                                                                    margin-bottom: 30px
                                                                }

                                                                #pl-20164 .so-panel:last-child {
                                                                    margin-bottom: 0
                                                                }

                                                                #pg-20164-0 > .panel-row-style {
                                                                    background-color: #749f00
                                                                }

                                                                #pg-20164-0.panel-no-style, #pg-20164-0.panel-has-style > .panel-row-style {
                                                                    -webkit-align-items: flex-start;
                                                                    align-items: flex-start
                                                                }

                                                                #panel-20164-0-1-0 > .panel-widget-style {
                                                                    color: #fff;
                                                                    padding: 30px 0 0 0
                                                                }

                                                                #panel-20164-0-1-0 a {
                                                                    color: #fff
                                                                }

                                                                @media (max-width: 780px) {
                                                                    #pg-20164-0.panel-no-style, #pg-20164-0.panel-has-style > .panel-row-style {
                                                                        -webkit-flex-direction: column;
                                                                        -ms-flex-direction: column;
                                                                        flex-direction: column
                                                                    }

                                                                    #pg-20164-0 .panel-grid-cell {
                                                                        margin-right: 0
                                                                    }

                                                                    #pg-20164-0 .panel-grid-cell {
                                                                        width: 100%
                                                                    }

                                                                    #pgc-20164-0-0 {
                                                                        margin-bottom: 30px
                                                                    }

                                                                    #pl-20164 .panel-grid-cell {
                                                                        padding: 0
                                                                    }

                                                                    #pl-20164 .panel-grid .panel-grid-cell-empty {
                                                                        display: none
                                                                    }

                                                                    #pl-20164 .panel-grid .panel-grid-cell-mobile-last {
                                                                        margin-bottom: 0
                                                                    }

                                                                    #panel-20164-0-1-0 > .panel-widget-style {
                                                                        padding: 0 0 0 0
                                                                    }
                                                                }</style>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style=" min-height:400px; "
                                     class="row normal_height vc_row wpb_row vc_row-fluid courserowcopy vc_custom_1520952630086 vc_row-has-fill">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <div class="boxed-row">
                                                    <div class="row vc_row wpb_row vc_inner vc_row-fluid vc_column-gap-15">
                                                        <div class="courseplace wpb_column vc_column_container vc_col-sm-6">
                                                            <div class="vc_column-inner vc_custom_1519748829966">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper"><p><strong><img
                                                                                            class="alignnone wp-image-19329"
                                                                                            src="https://www.academyclass.com/wp-content/uploads/2018/02/courser-place-icon-1.png"
                                                                                            alt="" width="23"
                                                                                            height="19"/>LOCATIONS</strong>
                                                                            </p>
                                                                            <ul class="a">
                                                                                <li><strong>London </strong> Elizabeth
                                                                                    House, 39 York Road SE1 7NQ
                                                                                </li>
                                                                                <li><strong>Glasgow </strong> 111 Union
                                                                                    St, Glasgow G1 3TA
                                                                                </li>
                                                                                <li><strong>Manchester </strong>Universal
                                                                                    Square, Devonshire Street, North
                                                                                    Ardwick, M12 6JH
                                                                                </li>
                                                                                <li><strong>Leeds </strong> 26 Whitehall
                                                                                    Road, Leeds LS12 1BE
                                                                                </li>
                                                                                <li><strong>Vilnius </strong> Palangos
                                                                                    g. 4 Vilnius 01402
                                                                                </li>
                                                                            </ul>
                                                                            <p>
                                                                                <a href="https://www.academyclass.com/contact-us/">Contact
                                                                                    us</a> for remote options!</p></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="wpb_column vc_column_container vc_col-sm-6">
                                                            <div class="vc_column-inner vc_custom_1521009156765">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper"><p><img
                                                                                        class="alignnone wp-image-19331"
                                                                                        src="https://www.academyclass.com/wp-content/uploads/2018/02/course-time.png"
                                                                                        alt="" width="25"
                                                                                        height="19"/><strong>COURSE
                                                                                    TIME</strong></p>
                                                                            <p><strong>9.30 &#8211; 16.30</strong><br/>
                                                                                (Including coffee break and 1 hr lunch)
                                                                            </p></div>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper"><p><strong><img
                                                                                            class="alignnone wp-image-19332"
                                                                                            src="https://www.academyclass.com/wp-content/uploads/2018/02/course-payment.png"
                                                                                            alt="" width="25"
                                                                                            height="19"/>PAYMENT OPTIONS</strong>
                                                                            </p>
                                                                            <ul class="a">
                                                                                <li><strong>Stripe</strong></li>
                                                                                <li><strong>Bank transfer</strong></li>
                                                                                <li><strong>Invoice</strong></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="boxed-row">
                                    <div style=""
                                         class="row normal_height vc_row wpb_row vc_row-fluid vc_custom_1521108302156">
                                        <div class="wpb_column vc_column_container vc_col-sm-12">
                                            <div class="vc_column-inner ">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element ">
                                                        <div class="wpb_wrapper">
                                                            <div id="pl-20105" class="panel-layout">
                                                                <div id="pg-20105-0" class="panel-grid panel-has-style"
                                                                     data-style="{&quot;padding&quot;:&quot;0px 0px 20px 0px&quot;,&quot;mobile_padding&quot;:&quot;0px 0px 0px 0px&quot;,&quot;background_display&quot;:&quot;tile&quot;,&quot;cell_alignment&quot;:&quot;flex-start&quot;}"
                                                                     data-ratio="1" data-ratio-direction="right">
                                                                    <div class="panel-row-style panel-row-style-for-20105-0">
                                                                        <div id="pgc-20105-0-0" class="panel-grid-cell"
                                                                             data-weight="1">
                                                                            <div id="panel-20105-0-0-0"
                                                                                 class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                                                                 data-index="0"
                                                                                 data-style="{&quot;background_display&quot;:&quot;tile&quot;}">
                                                                                <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                                                                    <div class="siteorigin-widget-tinymce textwidget">
                                                                                        <h2 style="text-align: center;">
                                                                                            Still Not Convinced?</h2>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div id="pg-20105-1" class="panel-grid panel-no-style"
                                                                     data-style="{&quot;background_display&quot;:&quot;tile&quot;,&quot;cell_alignment&quot;:&quot;flex-start&quot;}">
                                                                    <div id="pgc-20105-1-0" class="panel-grid-cell"
                                                                         data-weight="0.5">
                                                                        <div id="panel-20105-1-0-0"
                                                                             class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                                                             data-index="1"
                                                                             data-style="{&quot;background_image_attachment&quot;:false,&quot;background_display&quot;:&quot;tile&quot;}">
                                                                            <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                                                                <div class="siteorigin-widget-tinymce textwidget">
                                                                                    <h4 style="text-align: center;">
                                                                                        <span style="color: #008080;">18-Month Free</span><br/>
                                                                                        <span style="color: #008080;"> Class Retake</span>
                                                                                    </h4>
                                                                                    <p>If you have any gaps in your
                                                                                        knowledge or want to refresh
                                                                                        your skills, you are more than
                                                                                        welcome to come back and retake
                                                                                        the live online class free of
                                                                                        charge up to 18 months after you
                                                                                        have taken the class.</p></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div id="pgc-20105-1-1" class="panel-grid-cell"
                                                                         data-weight="0.5">
                                                                        <div id="panel-20105-1-1-0"
                                                                             class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                                                             data-index="2"
                                                                             data-style="{&quot;background_image_attachment&quot;:false,&quot;background_display&quot;:&quot;tile&quot;}">
                                                                            <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                                                                <div class="siteorigin-widget-tinymce textwidget">
                                                                                    <h4 style="text-align: center;">
                                                                                        <span style="color: #008080;">Money-Back</span><br/>
                                                                                        <span style="color: #008080;"> Guarantee</span>
                                                                                    </h4>
                                                                                    <p>If you don’t absolutely LOVE your
                                                                                        class, we’ll give you a full
                                                                                        refund! Let us know on the FIRST
                                                                                        day of your training if
                                                                                        something isn’t quite right and
                                                                                        give us a chance to fix it or
                                                                                        give you your money back.</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div id="pg-20105-2" class="panel-grid panel-no-style"
                                                                     data-style="{&quot;background_display&quot;:&quot;tile&quot;,&quot;cell_alignment&quot;:&quot;flex-start&quot;}">
                                                                    <div id="pgc-20105-2-0" class="panel-grid-cell"
                                                                         data-weight="0.5">
                                                                        <div id="panel-20105-2-0-0"
                                                                             class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                                                             data-index="3"
                                                                             data-style="{&quot;background_image_attachment&quot;:false,&quot;background_display&quot;:&quot;tile&quot;}">
                                                                            <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                                                                <div class="siteorigin-widget-tinymce textwidget">
                                                                                    <h4 style="text-align: center;">
                                                                                        <span style="color: #008080;">Lower Price</span><br/>
                                                                                        <span style="color: #008080;"> Guarantee</span>
                                                                                    </h4>
                                                                                    <p>We think our prices are pretty
                                                                                        fair but we won’t be beaten on
                                                                                        our fee. We’ll match and
                                                                                        discount by 10% any
                                                                                        like-for-like Training course
                                                                                        price.</p></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div id="pgc-20105-2-1" class="panel-grid-cell"
                                                                         data-weight="0.5">
                                                                        <div id="panel-20105-2-1-0"
                                                                             class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                                                             data-index="4"
                                                                             data-style="{&quot;background_image_attachment&quot;:false,&quot;background_display&quot;:&quot;tile&quot;}">
                                                                            <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                                                                <div class="siteorigin-widget-tinymce textwidget">
                                                                                    <h4 style="text-align: center;">
                                                                                        <span style="color: #008080;">Experienced<br/> Instructors</span>
                                                                                    </h4>
                                                                                    <p>Equipped with years of industry
                                                                                        experience our instructors will
                                                                                        assure a successful leap in your
                                                                                        knowledge, improvement and
                                                                                        preparation.</p></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div id="pg-20105-3" class="panel-grid panel-has-style"
                                                                     data-style="{&quot;padding&quot;:&quot;20px 0px 20px 0px&quot;,&quot;background_display&quot;:&quot;tile&quot;,&quot;cell_alignment&quot;:&quot;flex-start&quot;}"
                                                                     data-ratio="1" data-ratio-direction="right">
                                                                    <div class="panel-row-style panel-row-style-for-20105-3">
                                                                        <div id="pgc-20105-3-0" class="panel-grid-cell"
                                                                             data-weight="1">
                                                                            <div id="panel-20105-3-0-0"
                                                                                 class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                                                                 data-index="5"
                                                                                 data-style="{&quot;background_display&quot;:&quot;tile&quot;}">
                                                                                <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                                                                    <div class="siteorigin-widget-tinymce textwidget">
                                                                                        <p style="text-align: center;">
                                                                                            <strong>“The investment in
                                                                                                knowledge pays the best
                                                                                                interests.”</strong><br/>
                                                                                            ~ Benjamin Franklin</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <style type="text/css" class="panels-style"
                                                                   data-panels-style-for-post="20105">@import url(https://www.academyclass.com/wp-content/plugins/siteorigin-panels/css/front-flex.min.css);

                                                                #pgc-20105-0-0, #pgc-20105-3-0 {
                                                                    width: 100%;
                                                                    width: calc(100% - (0 * 30px))
                                                                }

                                                                #pg-20105-0, #pg-20105-1, #pg-20105-2, #pl-20105 .so-panel, #pl-20105 .so-panel:last-child {
                                                                    margin-bottom: 0
                                                                }

                                                                #pgc-20105-1-0, #pgc-20105-1-1, #pgc-20105-2-0, #pgc-20105-2-1 {
                                                                    width: 50%;
                                                                    width: calc(50% - (0.5 * 30px))
                                                                }

                                                                #pg-20105-0 > .panel-row-style {
                                                                    padding: 0 0 20px 0
                                                                }

                                                                #pg-20105-0.panel-no-style, #pg-20105-0.panel-has-style > .panel-row-style, #pg-20105-1.panel-no-style, #pg-20105-1.panel-has-style > .panel-row-style, #pg-20105-2.panel-no-style, #pg-20105-2.panel-has-style > .panel-row-style, #pg-20105-3.panel-no-style, #pg-20105-3.panel-has-style > .panel-row-style {
                                                                    -webkit-align-items: flex-start;
                                                                    align-items: flex-start
                                                                }

                                                                #pg-20105-3 > .panel-row-style {
                                                                    padding: 20px 0 20px 0
                                                                }

                                                                @media (max-width: 780px) {
                                                                    #pg-20105-0.panel-no-style, #pg-20105-0.panel-has-style > .panel-row-style, #pg-20105-1.panel-no-style, #pg-20105-1.panel-has-style > .panel-row-style, #pg-20105-2.panel-no-style, #pg-20105-2.panel-has-style > .panel-row-style, #pg-20105-3.panel-no-style, #pg-20105-3.panel-has-style > .panel-row-style {
                                                                        -webkit-flex-direction: column;
                                                                        -ms-flex-direction: column;
                                                                        flex-direction: column
                                                                    }

                                                                    #pg-20105-0 .panel-grid-cell, #pg-20105-1 .panel-grid-cell, #pg-20105-2 .panel-grid-cell, #pg-20105-3 .panel-grid-cell {
                                                                        margin-right: 0
                                                                    }

                                                                    #pg-20105-0 .panel-grid-cell, #pg-20105-1 .panel-grid-cell, #pg-20105-2 .panel-grid-cell, #pg-20105-3 .panel-grid-cell {
                                                                        width: 100%
                                                                    }

                                                                    #pgc-20105-1-0, #pgc-20105-2-0, #pl-20105 .panel-grid .panel-grid-cell-mobile-last {
                                                                        margin-bottom: 0
                                                                    }

                                                                    #pl-20105 .panel-grid-cell {
                                                                        padding: 0
                                                                    }

                                                                    #pl-20105 .panel-grid .panel-grid-cell-empty {
                                                                        display: none
                                                                    }

                                                                    #pg-20105-0 > .panel-row-style {
                                                                        padding: 0 0 0 0
                                                                    }
                                                                }</style>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style=""
                                     class="row normal_height vc_row wpb_row vc_row-fluid vc_custom_1521016914790 vc_row-has-fill">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element ">
                                                    <div class="wpb_wrapper"><h3
                                                                style="text-align: center; color: #000000;">That’s us.
                                                            Now it&#8217;s up to you. Enquire now!</h3></div>
                                                </div>
                                                <div role="form" class="wpcf7" id="wpcf7-f190-p19152-o1" lang="en-US"
                                                     dir="ltr">
                                                    <div class="screen-reader-response"></div>
                                                    <form action="/#wpcf7-f190-p19152-o1" method="post"
                                                          class="wpcf7-form" novalidate="novalidate">
                                                        <div style="display: none;"><input type="hidden" name="_wpcf7"
                                                                                           value="190"/> <input
                                                                    type="hidden" name="_wpcf7_version" value="5.0.2"/>
                                                            <input type="hidden" name="_wpcf7_locale" value="en_US"/>
                                                            <input type="hidden" name="_wpcf7_unit_tag"
                                                                   value="wpcf7-f190-p19152-o1"/> <input type="hidden"
                                                                                                         name="_wpcf7_container_post"
                                                                                                         value="19152"/>
                                                        </div>
                                                        <div id="form">
                                                            <div id="contentleft">
                                                                <p>
                                                                    <span class="wpcf7-form-control-wrap your-city"><select
                                                                                name="your-city"
                                                                                class="wpcf7-form-control wpcf7-select"
                                                                                aria-invalid="false"><option
                                                                                    value="Choose location">Choose location</option><option
                                                                                    value="London">London</option><option
                                                                                    value="Manchester">Manchester</option><option
                                                                                    value="Glasgow">Glasgow</option><option
                                                                                    value="Edinburgh">Edinburgh</option><option
                                                                                    value="Other">Other</option></select></span>
                                                                </p>
                                                                <p>
                                                                    <span class="wpcf7-form-control-wrap your-name"><input
                                                                                type="text" name="your-name" value=""
                                                                                size="40"
                                                                                class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                aria-required="true"
                                                                                aria-invalid="false"
                                                                                placeholder="Your name*"/></span></p>
                                                                <p>
                                                                    <span class="wpcf7-form-control-wrap your-phone"><input
                                                                                type="tel" name="your-phone" value=""
                                                                                size="40"
                                                                                class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-tel"
                                                                                aria-invalid="false"
                                                                                placeholder="Your phone"/></span></p>
                                                                <p style="margin-top:20px">
                                                                <div class="wpcf7-form-control-wrap">
                                                                    <div data-sitekey="6LfeAx0TAAAAANHLabZIBVrZeEY8sP_RmLwX79Rj"
                                                                         data-size="compact" data-theme="light"
                                                                         class="wpcf7-form-control g-recaptcha wpcf7-recaptcha"></div>
                                                                    <noscript>
                                                                        <div style="width: 302px; height: 422px;">
                                                                            <div style="width: 302px; height: 422px; position: relative;">
                                                                                <div style="width: 302px; height: 422px; position: absolute;">
                                                                                    <iframe src="https://www.google.com/recaptcha/api/fallback?k=6LfeAx0TAAAAANHLabZIBVrZeEY8sP_RmLwX79Rj"
                                                                                            frameborder="0"
                                                                                            scrolling="no"
                                                                                            style="width: 302px; height:422px; border-style: none;"></iframe>
                                                                                </div>
                                                                                <div style="width: 300px; height: 60px; border-style: none; bottom: 12px; left: 25px; margin: 0px; padding: 0px; right: 25px; background: #f9f9f9; border: 1px solid #c1c1c1; border-radius: 3px;"><textarea
                                                                                            id="g-recaptcha-response"
                                                                                            name="g-recaptcha-response"
                                                                                            class="g-recaptcha-response"
                                                                                            style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;">
				</textarea></div>
                                                                            </div>
                                                                        </div>
                                                                    </noscript>
                                                                </div>
                                                            </div>
                                                            <p><input type="hidden" id="gclid_field" name="gclid_field"
                                                                      value=""></p>
                                                            <div id="contentright"><p><span
                                                                            class="wpcf7-form-control-wrap your-company"><input
                                                                                type="text" name="your-company" value=""
                                                                                size="40"
                                                                                class="wpcf7-form-control wpcf7-text"
                                                                                aria-invalid="false"
                                                                                placeholder="Company"/></span></p>
                                                                <p><span class="wpcf7-form-control-wrap email"><input
                                                                                type="email" name="email" value=""
                                                                                size="40"
                                                                                class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                                aria-required="true"
                                                                                aria-invalid="false"
                                                                                placeholder="Your email*"/></span></p>
                                                                <p>
                                                                    <span class="wpcf7-form-control-wrap your-message"><textarea
                                                                                name="your-message" cols="40" rows="3"
                                                                                class="wpcf7-form-control wpcf7-textarea"
                                                                                aria-invalid="false"
                                                                                placeholder="Your message*"></textarea></span>
                                                                </p>
                                                                <p>
                                                                    <span class="wpcf7-form-control-wrap checkbox-842"><span
                                                                                class="wpcf7-form-control wpcf7-checkbox"
                                                                                id="add-subs-list"><span
                                                                                    class="wpcf7-list-item first last"><label><input
                                                                                            type="checkbox"
                                                                                            name="checkbox-842[]"
                                                                                            value="I would like to get news about courses and special offers"/><span
                                                                                            class="wpcf7-list-item-label">I would like to get news about courses and special offers</span></label></span></span></span>
                                                                </p>
                                                                <p>
                                                                    <span class="wpcf7-form-control-wrap referer-page"><input
                                                                                type="text" name="referer-page" value=""
                                                                                size="40"
                                                                                class="wpcf7-form-control wpcf7-text referer-page"
                                                                                aria-invalid="false"/></span></p>
                                                                <p><input type="submit" value="Enquire Now"
                                                                          class="wpcf7-form-control wpcf7-submit"/></p>
                                                            </div>
                                                        </div>
                                                        <div class="wpcf7-response-output wpcf7-display-none"></div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="boxed-row">
                                    <div style=""
                                         class="row normal_height vc_row wpb_row vc_row-fluid vc_custom_1519741632341 vc_row-has-fill">
                                        <div class="wpb_column vc_column_container vc_col-sm-12">
                                            <div class="vc_column-inner ">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_single_image wpb_content_element vc_align_center">
                                                        <figure class="wpb_wrapper vc_figure">
                                                            <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                                <img width="900" height="149"
                                                                     src="https://www.academyclass.com/wp-content/uploads/2018/02/loving-bar-2.jpg"
                                                                     class="vc_single_image-img attachment-large"
                                                                     alt=""/></div>
                                                        </figure>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer id="site-footer">
                    <div class="trigger-footer-widget-area"><span class="trigger-footer-widget-icon"></span></div>
                    <div class="site-footer-widget-area">
                        <div class="row">
                            <div class="large-3 columns">
                                <aside id="text-4" class="widget widget_text"><h3 class="widget-title">About Us</h3>
                                    <div class="textwidget"><p>Our classes are all hands-on and led by Certified Adobe,
                                            Autodesk, Maxon and Unity instructors who are also designers and developers.
                                            You’re in good hands!</p>
                                        <p><a href="/terms_and_conditions/">Website Terms</a><br/> <a
                                                    href="/privacy_policy/">Privacy Policy</a><br/> <a
                                                    href="/terms_of_business/">Terms of Business</a></p>
                                        <table style="width: auto; float:left; align:center; border:0; cellspacing:2;">
                                            <tbody>
                                            <tr>
                                                <td style="padding-right: 5px;"><a href="http://www.americanexpress.com"
                                                                                   target="_blank"><br/> <img
                                                                src="/wp-content/uploads/icons/WP_AMEX.gif"
                                                                alt="American Express" style="border: 0;"/><br/> </a>
                                                </td>
                                                <td style="padding-right: 5px;"><a href="http://www.mastercard.com"
                                                                                   target="_blank"><br/> <img
                                                                src="/wp-content/uploads/icons/WP_ECMC.gif"
                                                                alt="MasterCard" style="border: 0;"/><br/> </a></td>
                                                <td style="padding-right: 5px;"><a href="http://www.jcbusa.com"
                                                                                   target="_blank"><br/> <img
                                                                src="/wp-content/uploads/icons/WP_JCB.jpg" alt="JCB"
                                                                style="border: 0;"/><br/> </a></td>
                                                <td style="padding-right: 5px;"><a href="http://www.maestrocard.com"
                                                                                   target="_blank"><br/> <img
                                                                src="/wp-content/uploads/icons/WP_MAESTRO.gif"
                                                                alt="Maestro" style="border: 0;"/><br/> </a></td>
                                                <td><a href="http://www.visa.com" target="_blank"><br/> <img
                                                                src="/wp-content/uploads/icons/WP_VISA_DELTA.jpg"
                                                                alt="Visa Debit" style="border: 0;"/><br/> </a></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: center ;" colspan="5"><a
                                                            href="http://www.worldpay.com/support/index.php?CMP=BA22713"
                                                            target="_blank"><br/> <img
                                                                src="/wp-content/uploads/icons/poweredByWorldPay.gif"
                                                                alt="Powered by WorldPay" style="border: 0;"/><br/> </a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </aside>
                            </div>
                            <div class="large-3 columns">
                                <aside id="nav_menu-2" class="widget widget_nav_menu"><h3 class="widget-title">
                                        Links</h3>
                                    <div class="menu-footer-menu-en-container">
                                        <ul id="menu-footer-menu-en" class="menu">
                                            <li id="menu-item-106"
                                                class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-106">
                                                <a href="/">Home</a></li>
                                            <li id="menu-item-7700"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7700">
                                                <a href="https://www.academyclass.com/courses/">Courses</a></li>
                                            <li id="menu-item-7701"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7701">
                                                <a href="https://www.academyclass.com/packages/">Packages</a></li>
                                            <li id="menu-item-7702"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7702">
                                                <a href="https://www.academyclass.com/certification/">Certification</a>
                                            </li>
                                            <li id="menu-item-7704"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7704">
                                                <a href="https://www.academyclass.com/video/">Video</a></li>
                                        </ul>
                                    </div>
                                </aside>
                            </div>
                            <div class="large-3 columns">
                                <aside id="text-5" class="widget widget_text"><h3 class="widget-title">Contact</h3>
                                    <div class="textwidget">
                                        <div><p><span>Elizabeth House, 39 York Road</span><br/> <span>SE1 7NQ</span>,
                                                <span>London</span><br/> <span>United Kingdom</span><br/> Company No:
                                                5878400</p></div>
                                        <p></p>
                                        <p><a class="gr-link" href="mailto:training@academyclass.com">training@academyclass.com</a><br/>
                                            Tel. <a class="gr-link" href="tel:+448000438889">0800 043 8889</a><br/> Fax:
                                            0870 330 5722</p>
                                        <aside id="shopkeeper_social_media-2"
                                               class="widget widget_shopkeeper_social_media"><h3 class="widget-title">
                                                Follow us</h3>
                                            <p><a href="https://www.facebook.com/AcademyClass" target="_blank"
                                                  class="widget_connect_facebook">Facebook</a><a
                                                        href="https://www.linkedin.com/company-beta/650839/"
                                                        target="_blank" class="widget_connect_linkedin">Linkedin</a><a
                                                        href="https://twitter.com/academyclass" target="_blank"
                                                        class="widget_connect_twitter">Twitter</a><a
                                                        href="https://plus.google.com/+AcademyClassPodcasts/about"
                                                        target="_blank" class="widget_connect_googleplus">Google+</a><a
                                                        href="https://www.instagram.com/academy_class/?hl=en"
                                                        target="_blank" class="widget_connect_instagram">Instagram</a><a
                                                        href="https://www.youtube.com/user/AcademyClassPodcasts"
                                                        target="_blank" class="widget_connect_youtube">Youtube</a><a
                                                        href="http://www.meetup.com/Creative-Class/" target="_blank"
                                                        class="widget_connect_vk">VK</a></aside>
                                    </div>
                                </aside>
                            </div>
                            <aside id="text-66" class="widget widget_text"><h3 class="widget-title">Stay in touch with
                                    Academy Class</h3>
                                <div class="large-3 columns">
                                    <div id="mc_embed_signup">
                                        <form action="https://academyclass.us17.list-manage.com/subscribe/post?u=8fec92477866d5e60ce3f8adf&amp;id=35eb81f802"
                                              method="post" id="mc-embedded-subscribe-form"
                                              name="mc-embedded-subscribe-form" class="validate" target="_blank"
                                              novalidate>
                                            <div id="mc_embed_signup_scroll">
                                                <div class="mc-field-group"><label for="mce-EMAIL">Email Address <span
                                                                class="asterisk">*</span> </label> <input type="email"
                                                                                                          value=""
                                                                                                          name="EMAIL"
                                                                                                          class="required email"
                                                                                                          id="mce-EMAIL"
                                                                                                          style="width: 70%;">
                                                </div>
                                                <div id="mce-responses" class="clear">
                                                    <div class="response" id="mce-error-response"
                                                         style="display:none"></div>
                                                    <div class="response" id="mce-success-response"
                                                         style="display:none"></div>
                                                </div>
                                                <div style="position: absolute; left: -5000px;" aria-hidden="true">
                                                    <input type="text" name="b_8fec92477866d5e60ce3f8adf_35eb81f802"
                                                           tabindex="-1" value=""></div>
                                                <div class="clear"><input
                                                            style="line-height: 0px; margin-top: 5px;width: 70%; height: 44px;"
                                                            type="submit" value="Subscribe" name="subscribe"
                                                            id="mc-embedded-subscribe" class="button"></div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </aside>
                            <div class="large-3 columns">
                                <aside id="text-6" class="widget widget_text">
                                    <div class="textwidget"></div>
                                </aside>
                            </div>
                        </div>
                    </div>
                    <div class="site-footer-copyright-area">
                        <div class="row">
                            <div class="large-12 columns">
                                <nav class="footer-navigation-wrapper"></nav>
                                <div class="copyright_text">
                                    <div class="footer-socket"><p class="footer-socket-left">All Rights reserved by
                                            Academy Class 2018.</p>
                                        <p class="footer-socket-right"></p></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </div>
    <nav class="st-menu slide-from-left //hide-for-large-up shop-has-sidebar">
        <div class="nano">
            <div class="content">
                <div class="offcanvas_content_left wpb_widgetised_column">
                    <div id="filters-offcanvas">
                        <aside id="woocommerce_product_categories-2"
                               class="widget woocommerce widget_product_categories"><h3 class="widget-title">Course
                                Categories</h3>
                            <ul class="product-categories">
                                <li class="cat-item cat-item-1100 cat-parent"><a
                                            href="https://www.academyclass.com/courses-category/3dvr/">3d/VR</a> <span
                                            class="count">(24)</span></li>
                                <li class="cat-item cat-item-9 cat-parent"><a
                                            href="https://www.academyclass.com/courses-category/adobe/">Adobe
                                        Courses</a> <span class="count">(44)</span></li>
                                <li class="cat-item cat-item-19 cat-parent"><a
                                            href="https://www.academyclass.com/courses-category/apple/">Apple</a> <span
                                            class="count">(7)</span></li>
                                <li class="cat-item cat-item-17 cat-parent"><a
                                            href="https://www.academyclass.com/courses-category/autodesk/">Autodesk</a>
                                    <span class="count">(24)</span></li>
                                <li class="cat-item cat-item-1099 cat-parent"><a
                                            href="https://www.academyclass.com/courses-category/cadbim/">CAD/BIM</a>
                                    <span class="count">(16)</span></li>
                                <li class="cat-item cat-item-1239"><a
                                            href="https://www.academyclass.com/courses-category/coding/">Coding</a>
                                    <span class="count">(12)</span></li>
                                <li class="cat-item cat-item-24 cat-parent"><a
                                            href="https://www.academyclass.com/courses-category/design/">Design</a>
                                    <span class="count">(34)</span></li>
                                <li class="cat-item cat-item-797"><a
                                            href="https://www.academyclass.com/courses-category/leadership/">Leadership</a>
                                    <span class="count">(9)</span></li>
                                <li class="cat-item cat-item-1240"><a
                                            href="https://www.academyclass.com/courses-category/marketing/">Marketing</a>
                                    <span class="count">(11)</span></li>
                                <li class="cat-item cat-item-18 cat-parent"><a
                                            href="https://www.academyclass.com/courses-category/maxon/">Maxon</a> <span
                                            class="count">(5)</span></li>
                                <li class="cat-item cat-item-1538 cat-parent"><a
                                            href="https://www.academyclass.com/courses-category/mentorship/">Mentorship</a>
                                    <span class="count">(1)</span></li>
                                <li class="cat-item cat-item-20 cat-parent"><a
                                            href="https://www.academyclass.com/courses-category/mobile-development/">Mobile
                                        Development</a> <span class="count">(1)</span></li>
                                <li class="cat-item cat-item-27 cat-parent"><a
                                            href="https://www.academyclass.com/courses-category/video/">Video</a> <span
                                            class="count">(28)</span></li>
                                <li class="cat-item cat-item-1142"><a
                                            href="https://www.academyclass.com/courses-category/vouchers-vouchers/">Vouchers</a>
                                    <span class="count">(3)</span></li>
                                <li class="cat-item cat-item-21 cat-parent"><a
                                            href="https://www.academyclass.com/courses-category/web-fundamentals/">Web
                                        fundamentals</a> <span class="count">(25)</span></li>
                            </ul>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <nav class="st-menu slide-from-right">
        <div class="nano">
            <div class="content">
                <div class="offcanvas_content_right">
                    <div id="mobiles-menu-offcanvas">
                        <div class="mobile-search hide-for-large-up">
                            <div class="widget woocommerce widget_product_search">
                                <form role="search" method="get" class="woocommerce-product-search"
                                      action="https://www.academyclass.com/"><label class="screen-reader-text"
                                                                                    for="woocommerce-product-search-field-0">Search
                                        for:</label> <input type="search" id="woocommerce-product-search-field-0"
                                                            class="search-field" placeholder="Search products&hellip;"
                                                            value="" name="s"/> <input type="submit" value="Search"/>
                                    <input type="hidden" name="post_type" value="product"/></form>
                            </div>
                            <div class="mobile_search_submit"><i class="fa fa-search"></i></div>
                        </div>
                        <nav class="mobile-navigation primary-navigation hide-for-large-up">
                            <ul id="menu-main-menu-en-1">
                                <li id="menu-item-260"
                                    class="extended-menu menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-260">
                                    <a href="https://www.academyclass.com/training/">Courses</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-6524"
                                            class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-item-6524">
                                            <a href="https://www.academyclass.com/courses-category/adobe/">Adobe</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-13319"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13319">
                                                    <a href="http://www.academyclass.com/courses/adobe-acrobat-pdf-interactivity-form-design/">Acrobat
                                                        Pro DC</a></li>
                                                <li id="menu-item-23340"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23340">
                                                    <a href="https://www.academyclass.com/training/adobe/after-effects/">After
                                                        Effects</a></li>
                                                <li id="menu-item-8604"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8604">
                                                    <a href="https://www.academyclass.com/training/adobe/adobe-animate/">Animate</a>
                                                </li>
                                                <li id="menu-item-5900"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5900">
                                                    <a href="https://www.academyclass.com/training/adobe/captivate/">Captivate</a>
                                                </li>
                                                <li id="menu-item-5905"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5905">
                                                    <a href="https://www.academyclass.com/training/adobe/creative-cloud/">Creative
                                                        Cloud</a></li>
                                                <li id="menu-item-13358"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13358">
                                                    <a href="http://www.academyclass.com/courses/adobe-user-experience-xd/">Experience
                                                        Design</a></li>
                                                <li id="menu-item-23332"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23332">
                                                    <a href="https://www.academyclass.com/training/adobe/illustrator/">Illustrator</a>
                                                </li>
                                                <li id="menu-item-23329"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-23329">
                                                    <a href="https://www.academyclass.com/training/adobe/indesign/">InDesign</a>
                                                </li>
                                                <li id="menu-item-13316"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13316">
                                                    <a href="http://www.academyclass.com/courses/lightroom/">Lightroom</a>
                                                </li>
                                                <li id="menu-item-5935"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5935">
                                                    <a href="https://www.academyclass.com/training/adobe/muse/">Muse</a>
                                                </li>
                                                <li id="menu-item-23334"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23334">
                                                    <a href="https://www.academyclass.com/training/adobe/photoshop/">Photoshop
                                                        Courses</a></li>
                                                <li id="menu-item-23343"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23343">
                                                    <a href="https://www.academyclass.com/training/adobe/premiere-pro/">Premiere
                                                        Pro</a></li>
                                            </ul>
                                        </li>
                                        <li id="menu-item-6526"
                                            class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-item-6526">
                                            <a href="https://www.academyclass.com/courses-category/autodesk/">Autodesk</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-5950"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5950">
                                                    <a href="https://www.academyclass.com/training/autodesk/3ds-max/">3ds
                                                        Max</a></li>
                                                <li id="menu-item-23346"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23346">
                                                    <a href="https://www.academyclass.com/training/autodesk/autocad/">AutoCAD</a>
                                                </li>
                                                <li id="menu-item-8732"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8732">
                                                    <a href="https://www.academyclass.com/training/autodesk/fusion-360/">Fusion
                                                        360</a></li>
                                                <li id="menu-item-5955"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5955">
                                                    <a href="https://www.academyclass.com/training/autodesk/inventor/">Inventor</a>
                                                </li>
                                                <li id="menu-item-5957"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5957">
                                                    <a href="https://www.academyclass.com/training/autodesk/building-information-modelling/">Building
                                                        Information Modelling (BIM)</a></li>
                                                <li id="menu-item-5960"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5960">
                                                    <a href="https://www.academyclass.com/training/autodesk/maya/">Maya</a>
                                                </li>
                                                <li id="menu-item-5962"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5962">
                                                    <a href="https://www.academyclass.com/training/autodesk/navisworks/">Navisworks</a>
                                                </li>
                                                <li id="menu-item-5964"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5964">
                                                    <a href="https://www.academyclass.com/training/autodesk/revit/">Revit</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li id="menu-item-12163"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-12163">
                                            <a href="/courses-category/3dvr/">3D / VR / Games</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-6565"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6565">
                                                    <a href="https://www.academyclass.com/training/autodesk/3ds-max/">3ds
                                                        Max</a></li>
                                                <li id="menu-item-6576"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6576">
                                                    <a href="https://www.academyclass.com/training/maxon/cinema-4d/">Cinema
                                                        4D</a></li>
                                                <li id="menu-item-16013"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-16013">
                                                    <a href="https://www.academyclass.com/courses/hololens-101/">HoloLens</a>
                                                </li>
                                                <li id="menu-item-6572"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6572">
                                                    <a href="https://www.academyclass.com/training/autodesk/maya/">Maya</a>
                                                </li>
                                                <li id="menu-item-23348"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23348">
                                                    <a href="https://www.academyclass.com/training/cad-games-3d/unity-3d/">Unity
                                                        3D</a></li>
                                                <li id="menu-item-9731"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9731">
                                                    <a href="https://www.academyclass.com/training/cad-games-3d/unreal_engine_4/">Unreal
                                                        Engine 4</a></li>
                                                <li id="menu-item-6017"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6017">
                                                    <a href="https://www.academyclass.com/training/cad-games-3d/sketchup/">SketchUp</a>
                                                </li>
                                                <li id="menu-item-8731"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8731">
                                                    <a href="https://www.academyclass.com/training/autodesk/fusion-360/">Fusion
                                                        360</a></li>
                                                <li id="menu-item-13796"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13796">
                                                    <a href="https://www.academyclass.com/courses/zbrush-jumpstart/">Zbrush</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li id="menu-item-13409"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13409">
                                            <a href="http://www.academyclass.com/osx_ios_support/">OSX Support</a></li>
                                        <li id="menu-item-12162"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-12162">
                                            <a href="/courses-category/cadbim/">CAD / BIM</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-23347"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23347">
                                                    <a href="https://www.academyclass.com/training/autodesk/autocad/">AutoCAD</a>
                                                </li>
                                                <li id="menu-item-6570"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6570">
                                                    <a href="https://www.academyclass.com/training/autodesk/building-information-modelling/">Building
                                                        Information Modelling (BIM)</a></li>
                                                <li id="menu-item-6569"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6569">
                                                    <a href="https://www.academyclass.com/training/autodesk/inventor/">Inventor</a>
                                                </li>
                                                <li id="menu-item-12888"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12888">
                                                    <a href="https://www.academyclass.com/training/autodesk/fusion-360/">Fusion
                                                        360</a></li>
                                                <li id="menu-item-12923"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12923">
                                                    <a href="https://www.academyclass.com/training/cad-games-3d/sketchup/">SketchUp</a>
                                                </li>
                                                <li id="menu-item-6573"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6573">
                                                    <a href="https://www.academyclass.com/training/autodesk/navisworks/">Navisworks</a>
                                                </li>
                                                <li id="menu-item-6574"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6574">
                                                    <a href="https://www.academyclass.com/training/autodesk/revit/">Revit</a>
                                                </li>
                                                <li id="menu-item-13592"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13592">
                                                    <a href="http://www.academyclass.com/vectorworks/">Vectorworks</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li id="menu-item-13173"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-13173">
                                            <a href="http://www.academyclass.com/courses-category/coding/">Coding /
                                                Web</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-13207"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13207">
                                                    <a href="http://www.academyclass.com/courses/angular-4-101-yellow-belt/">Angular
                                                        4</a></li>
                                                <li id="menu-item-15797"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-15797">
                                                    <a href="https://www.academyclass.com/courses/articulate-storyline/">Articulate
                                                        Storyline</a></li>
                                                <li id="menu-item-15597"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-15597">
                                                    <a href="https://www.academyclass.com/courses/articulate-studio-pro-13/">Articulate
                                                        Studio Pro</a></li>
                                                <li id="menu-item-13208"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13208">
                                                    <a href="http://www.academyclass.com/courses/bootstrap-101-yellow-belt/">Bootstrap</a>
                                                </li>
                                                <li id="menu-item-5992"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5992">
                                                    <a href="https://www.academyclass.com/training/web-fundamentals/cascading-stylesheets-css/">Cascading
                                                        StyleSheets (CSS)</a></li>
                                                <li id="menu-item-13269"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13269">
                                                    <a href="http://www.academyclass.com/courses/ecmascript-6/">ECMAScript</a>
                                                </li>
                                                <li id="menu-item-13179"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13179">
                                                    <a href="http://www.academyclass.com/training/web-fundamentals/html/">HTML</a>
                                                </li>
                                                <li id="menu-item-13180"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13180">
                                                    <a href="http://www.academyclass.com/training/web-fundamentals/javascript/">JavaScript</a>
                                                </li>
                                                <li id="menu-item-6560"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6560">
                                                    <a href="https://www.academyclass.com/training/adobe/muse/">Muse</a>
                                                </li>
                                                <li id="menu-item-13181"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13181">
                                                    <a href="http://www.academyclass.com/training/web-fundamentals/nodejs/">NodeJS</a>
                                                </li>
                                                <li id="menu-item-5996"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5996">
                                                    <a href="https://www.academyclass.com/training/web-fundamentals/php-mysql/">PHP
                                                        / MYSQL</a></li>
                                                <li id="menu-item-6582"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6582">
                                                    <a href="https://www.academyclass.com/training/design/ux-design/">UX
                                                        Design</a></li>
                                                <li id="menu-item-6000"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6000">
                                                    <a href="https://www.academyclass.com/training/web-fundamentals/wordpress/">WordPress</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li id="menu-item-6528"
                                            class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-item-6528">
                                            <a href="https://www.academyclass.com/courses-category/design/">Design</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-6546"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6546">
                                                    <a href="https://www.academyclass.com/training/adobe/creative-cloud-2/">Creative
                                                        Cloud</a></li>
                                                <li id="menu-item-13381"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13381">
                                                    <a href="http://www.academyclass.com/digital_art/">Digital Art</a>
                                                </li>
                                                <li id="menu-item-13174"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13174">
                                                    <a href="http://www.academyclass.com/courses-category/design/graphic-design-techniques/">Design
                                                        Techniques</a></li>
                                                <li id="menu-item-23333"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23333">
                                                    <a href="https://www.academyclass.com/training/adobe/illustrator/">Illustrator</a>
                                                </li>
                                                <li id="menu-item-23330"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-23330">
                                                    <a href="https://www.academyclass.com/training/adobe/indesign/">InDesign</a>
                                                </li>
                                                <li id="menu-item-23335"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23335">
                                                    <a href="https://www.academyclass.com/training/adobe/photoshop/">Photoshop
                                                        Courses</a></li>
                                                <li id="menu-item-6004"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6004">
                                                    <a href="https://www.academyclass.com/training/design/ux-design/">UX
                                                        Design</a></li>
                                            </ul>
                                        </li>
                                        <li id="menu-item-17088"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-17088">
                                            <a href="https://www.academyclass.com/training/cad-games-3d/unity-3d">Unity</a>
                                        </li>
                                        <li id="menu-item-17082"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-17082">
                                            <a href="/training/maxon/cinema-4d/">Maxon</a></li>
                                        <li id="menu-item-11428"
                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-11428">
                                            <a href="https://www.academyclass.com/marketing/">Marketing Classes</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-14776"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-14776">
                                                    <a href="https://www.academyclass.com/courses/google_analytics/">Google
                                                        Analytics</a></li>
                                                <li id="menu-item-17385"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-17385">
                                                    <a href="https://www.academyclass.com/courses/growth-hacking/">Growth
                                                        Hacking</a></li>
                                                <li id="menu-item-16219"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-16219">
                                                    <a href="https://www.academyclass.com/courses/how-to-pitch/">How To
                                                        Pitch</a></li>
                                                <li id="menu-item-11609"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11609">
                                                    <a href="https://www.academyclass.com/courses/online-marketing/">Online
                                                        Marketing</a></li>
                                                <li id="menu-item-13160"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13160">
                                                    <a href="http://www.academyclass.com/courses/seo-101-yellow-belt/">SEO</a>
                                                </li>
                                                <li id="menu-item-13161"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13161">
                                                    <a href="https://www.academyclass.com/social-media/">Social
                                                        Media</a></li>
                                                <li id="menu-item-17382"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-17382">
                                                    <a href="https://www.academyclass.com/courses/social-selling/">Social
                                                        Selling</a></li>
                                                <li id="menu-item-16227"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-16227">
                                                    <a href="https://www.academyclass.com/courses/upgrade-our-marketing/">Upgrade
                                                        Our Marketing</a></li>
                                                <li id="menu-item-16237"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-16237">
                                                    <a href="https://www.academyclass.com/courses/video-works/">Video
                                                        Works</a></li>
                                                <li id="menu-item-16233"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-16233">
                                                    <a href="https://www.academyclass.com/courses/writing-content-sticks/">Writing
                                                        Content That Sticks</a></li>
                                            </ul>
                                        </li>
                                        <li id="menu-item-8845"
                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-8845">
                                            <a href="https://www.academyclass.com/professional_development/">Business
                                                Skills</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-17391"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-17391">
                                                    <a href="https://www.academyclass.com/courses/business-start-up/">Business
                                                        Start Up</a></li>
                                                <li id="menu-item-11562"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11562">
                                                    <a href="http://www.academyclass.com/courses/confident-customer-service/">Confident
                                                        Customer Service</a></li>
                                                <li id="menu-item-16218"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-16218">
                                                    <a href="https://www.academyclass.com/courses/how-to-pitch/">How to
                                                        pitch</a></li>
                                                <li id="menu-item-15096"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-15096">
                                                    <a href="https://www.academyclass.com/courses/introduction-agile-project-management/">Introduction
                                                        To Agile Project Management</a></li>
                                                <li id="menu-item-11565"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11565">
                                                    <a href="http://www.academyclass.com/courses/introduction-project-management/">Introduction
                                                        To Project Management</a></li>
                                                <li id="menu-item-16224"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-16224">
                                                    <a href="https://www.academyclass.com/courses/leadership-pulling-together/">Pulling
                                                        Together</a></li>
                                                <li id="menu-item-14237"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-14237">
                                                    <a href="https://www.academyclass.com/courses/conflict-management-2/">Making
                                                        Time WORK</a></li>
                                                <li id="menu-item-15697"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-15697">
                                                    <a href="https://www.academyclass.com/courses/stepping-up-to-management/">Step
                                                        up to Management and Leadership</a></li>
                                            </ul>
                                        </li>
                                        <li id="menu-item-6539"
                                            class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-item-6539">
                                            <a href="https://www.academyclass.com/courses-category/video/">Video /
                                                Animation</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-6566"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6566">
                                                    <a href="https://www.academyclass.com/training/autodesk/3ds-max/">3ds
                                                        Max</a></li>
                                                <li id="menu-item-23341"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23341">
                                                    <a href="https://www.academyclass.com/training/adobe/after-effects/">After
                                                        Effects</a></li>
                                                <li id="menu-item-6575"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6575">
                                                    <a href="https://www.academyclass.com/training/maxon/cinema-4d/">Cinema
                                                        4D</a></li>
                                                <li id="menu-item-12960"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12960">
                                                    <a href="https://www.academyclass.com/training/video/davinci-resolve/">DaVinci
                                                        Resolve</a></li>
                                                <li id="menu-item-6577"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6577">
                                                    <a href="https://www.academyclass.com/training/apple/final-cut-pro/">Final
                                                        Cut Pro</a></li>
                                                <li id="menu-item-6571"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6571">
                                                    <a href="https://www.academyclass.com/training/autodesk/maya/">Maya</a>
                                                </li>
                                                <li id="menu-item-6579"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6579">
                                                    <a href="https://www.academyclass.com/training/apple/motion/">Motion</a>
                                                </li>
                                                <li id="menu-item-14238"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-14238">
                                                    <a href="https://www.academyclass.com/courses/nuke_jumpstart/">Nuke</a>
                                                </li>
                                                <li id="menu-item-23344"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23344">
                                                    <a href="https://www.academyclass.com/training/adobe/premiere-pro/">Premiere
                                                        Pro</a></li>
                                                <li id="menu-item-23349"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23349">
                                                    <a href="https://www.academyclass.com/training/cad-games-3d/unity-3d/">Unity
                                                        3D</a></li>
                                            </ul>
                                        </li>
                                        <li id="menu-item-18888"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18888">
                                            <a title="All courses and dates" href="/all-courses-and-dates/">Quick
                                                courses search</a></li>
                                    </ul>
                                </li>
                                <li id="menu-item-9485"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9485"><a
                                            href="https://www.academyclass.com/certification/">Certification</a></li>
                                <li id="menu-item-12"
                                    class="open-offcanvas menu-item menu-item-type-custom menu-item-object-custom menu-item-12">
                                    <a href="#">LEARNING PATHS</a></li>
                                <li id="menu-item-13118"
                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13118"><a
                                            href="http://www.academyclass.com/training/bespoke-training">Bespoke</a>
                                </li>
                                <li id="menu-item-15"
                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-15">
                                    <a href="/book-online/">Enquire Now</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-6625"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-6625">
                                            <a target="_blank" href="tel:+448000438889">or call
                                                <span>0800 043 8889</span></a></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                        <nav class="mobile-navigation hide-for-large-up">
                            <ul id="menu-main-menu-top-bar-en-1">
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-20078"><a
                                            href="https://www.academyclass.com/info/">About Us</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8093"><a
                                            href="https://www.academyclass.com/video/">Videos</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-374"><a
                                            href="https://www.academyclass.com/blog/">Articles, Resources &#038;
                                        Insights</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8094"><a
                                            href="https://www.academyclass.com/events/">Events</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9257"><a
                                            href="https://www.academyclass.com/newsletter/">Join our Newsletter</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5834"><a
                                            href="https://www.academyclass.com/contact-us/">Contact Us</a></li>
                                <li class="gr-link menu-item menu-item-type-custom menu-item-object-custom menu-item-6600">
                                    <a target="_blank" href="tel:+448000438889">0800 043 8889</a></li>
                            </ul>
                        </nav>
                        <div class="language-and-currency-offcanvas hide-for-large-up"></div>
                    </div>
                    <div class="shop_sidebar wpb_widgetised_column">
                        <aside id="nav_menu-3" class="widget widget_nav_menu">
                            <div class="menu-i-want-to-become-container">
                                <ul id="menu-i-want-to-become" class="menu">
                                    <li id="menu-item-11021"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-11021">
                                        <a title="Access All Areas"
                                           href="https://www.academyclass.com/packages/access-all-areas/">Access All
                                            Areas</a>
                                        <ul class="sub-menu">
                                            <li id="menu-item-13342"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13342">
                                                <a href="https://www.academyclass.com/packages/training-vouchers/">Training
                                                    vouchers</a></li>
                                        </ul>
                                    </li>
                                    <li id="menu-item-8059"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-8059">
                                        <a href="https://www.academyclass.com/packages/i-want-to-become-training-courses-interior-designers-london/">Learning
                                            Paths</a>
                                        <ul class="sub-menu">
                                            <li id="menu-item-8074"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8074">
                                                <a href="https://www.academyclass.com/packages/i-want-to-become-training-courses-interior-designers-london/a-3d-animator/">A
                                                    3D Animator</a></li>
                                            <li id="menu-item-8951"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8951">
                                                <a href="https://www.academyclass.com/packages/i-want-to-become-training-courses-interior-designers-london/a-cad-visualiser/">A
                                                    CAD Visualiser</a></li>
                                            <li id="menu-item-8060"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8060">
                                                <a href="https://www.academyclass.com/packages/i-want-to-become-training-courses-interior-designers-london/a-designer/">A
                                                    Designer</a></li>
                                            <li id="menu-item-9000"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9000">
                                                <a href="https://www.academyclass.com/packages/i-want-to-become-training-courses-interior-designers-london/an-elearning-specialist/">An
                                                    eLearning Specialist</a></li>
                                            <li id="menu-item-8064"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8064">
                                                <a href="https://www.academyclass.com/packages/i-want-to-become-training-courses-interior-designers-london/an-interactive-designer/">An
                                                    Interactive Designer</a></li>
                                            <li id="menu-item-8999"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8999">
                                                <a href="https://www.academyclass.com/packages/i-want-to-become-training-courses-interior-designers-london/an-interior-designer-2/">An
                                                    Interior Designer</a></li>
                                            <li id="menu-item-8953"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8953">
                                                <a href="https://www.academyclass.com/packages/i-want-to-become-training-courses-interior-designers-london/a-vfx-specialist/">A
                                                    VFX Specialist</a></li>
                                            <li id="menu-item-8061"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8061">
                                                <a href="https://www.academyclass.com/packages/i-want-to-become-training-courses-interior-designers-london/a-video-production-specialist/">A
                                                    Video Production Specialist</a></li>
                                            <li id="menu-item-8062"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8062">
                                                <a href="https://www.academyclass.com/packages/i-want-to-become-training-courses-interior-designers-london/a-web-designer/">A
                                                    Web Designer</a></li>
                                            <li id="menu-item-8063"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8063">
                                                <a href="https://www.academyclass.com/packages/i-want-to-become-training-courses-interior-designers-london/a-web-developer/">A
                                                    Web Developer</a></li>
                                        </ul>
                                    </li>
                                    <li id="menu-item-354"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-354">
                                        <a href="https://www.academyclass.com/packages/zero-to-hero/">Zero to HERO</a>
                                        <ul class="sub-menu">
                                            <li id="menu-item-7676"
                                                class="menu-item menu-item-type-post_type menu-item-object-product menu-item-7676">
                                                <a href="https://www.academyclass.com/courses/3ds-max-zero-to-hero/">3ds
                                                    Max</a></li>
                                            <li id="menu-item-7689"
                                                class="menu-item menu-item-type-post_type menu-item-object-product menu-item-7689">
                                                <a href="https://www.academyclass.com/courses/after-effects-course-london/">After
                                                    Effects</a></li>
                                            <li id="menu-item-7686"
                                                class="menu-item menu-item-type-post_type menu-item-object-product menu-item-7686">
                                                <a href="https://www.academyclass.com/courses/autocad-courses-london/">AutoCAD
                                                    Certification</a></li>
                                            <li id="menu-item-7684"
                                                class="menu-item menu-item-type-post_type menu-item-object-product menu-item-7684">
                                                <a href="https://www.academyclass.com/courses/captivate-zero-to-hero/">Captivate</a>
                                            </li>
                                            <li id="menu-item-7685"
                                                class="menu-item menu-item-type-post_type menu-item-object-product menu-item-7685">
                                                <a href="https://www.academyclass.com/courses/cinema-4d-course-london/">Cinema
                                                    4D</a></li>
                                            <li id="menu-item-6034"
                                                class="menu-item menu-item-type-post_type menu-item-object-product menu-item-6034">
                                                <a href="https://www.academyclass.com/courses/adobe-illustrator-course-london/">Illustrator</a>
                                            </li>
                                            <li id="menu-item-7678"
                                                class="menu-item menu-item-type-post_type menu-item-object-product menu-item-7678">
                                                <a href="https://www.academyclass.com/courses/indesign-course-london/">InDesign</a>
                                            </li>
                                            <li id="menu-item-7681"
                                                class="menu-item menu-item-type-post_type menu-item-object-product menu-item-7681">
                                                <a href="https://www.academyclass.com/courses/javascript-course-london/">JavaScript</a>
                                            </li>
                                            <li id="menu-item-7677"
                                                class="menu-item menu-item-type-post_type menu-item-object-product menu-item-7677">
                                                <a href="https://www.academyclass.com/courses/maya-zero-to-hero/">Maya</a>
                                            </li>
                                            <li id="menu-item-7675"
                                                class="menu-item menu-item-type-post_type menu-item-object-product menu-item-7675">
                                                <a href="https://www.academyclass.com/courses/nodejs-zero-to-hero/">NodeJS</a>
                                            </li>
                                            <li id="menu-item-7687"
                                                class="menu-item menu-item-type-post_type menu-item-object-product menu-item-7687">
                                                <a href="https://www.academyclass.com/courses/photoshop-courses-london/">Photoshop</a>
                                            </li>
                                            <li id="menu-item-7679"
                                                class="menu-item menu-item-type-post_type menu-item-object-product menu-item-7679">
                                                <a href="https://www.academyclass.com/courses/premiere-pro-aca-zero-to-hero/">Premiere
                                                    Pro</a></li>
                                            <li id="menu-item-7688"
                                                class="menu-item menu-item-type-post_type menu-item-object-product menu-item-7688">
                                                <a href="https://www.academyclass.com/courses/revit-courses-london/">Revit
                                                    Architecture</a></li>
                                            <li id="menu-item-7682"
                                                class="menu-item menu-item-type-post_type menu-item-object-product menu-item-7682">
                                                <a href="https://www.academyclass.com/courses/ux-design-course-london/">UX
                                                    / UI Design</a></li>
                                        </ul>
                                    </li>
                                    <li id="menu-item-17566"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-17566">
                                        <a href="/courses-tag/mastery/">Ultimate Mastery</a>
                                        <ul class="sub-menu">
                                            <li id="menu-item-17567"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-17567">
                                                <a href="/courses/after-effects-creative-license/">After Effects:
                                                    Ultimate Mastery</a></li>
                                            <li id="menu-item-17571"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-17571">
                                                <a href="/courses/cinema-4d-creative-license/">Cinema 4D: Ultimate
                                                    Mastery</a></li>
                                            <li id="menu-item-17568"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-17568">
                                                <a href="/courses/indesign-creative-license/">InDesign: Ultimate
                                                    Mastery</a></li>
                                            <li id="menu-item-17570"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-17570">
                                                <a href="/courses/illustrator-creative-license/">Illustrator: Ultimate
                                                    Mastery</a></li>
                                            <li id="menu-item-17569"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-17569">
                                                <a href="/courses/photoshop-creative-license-2/">Photoshop: Ultimate
                                                    Mastery</a></li>
                                            <li id="menu-item-17572"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-17572">
                                                <a href="/courses/premiere-pro-creative-license/">Premiere Pro: Ultimate
                                                    Mastery</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</div>
<div class="site-search">
    <div class="site-search-inner">
        <div class="widget woocommerce widget_product_search">
            <form role="search" method="get" class="woocommerce-product-search" action="https://www.academyclass.com/"
                  style="margin-left: -420px;"><label class="screen-reader-text"
                                                      for="woocommerce-product-search-field-1">Search for:</label>
                <input type="search" id="woocommerce-product-search-field-1" class="search-field"
                       placeholder="Search courses..." value="" name="s"> <input type="submit" value="Search"> <input
                        type="hidden" name="post_type" value="product">
                <div class="submit_icon"><i class="spk-icon-search"></i></div>
            </form>
        </div>
    </div>
</div>
<a href="#0" class="cd-top"></a>
<div id="quick_view_container">
    <div id="placeholder_product_quick_view" class="woocommerce"></div>
</div>
<script async type="text/javascript" src="//cdns.canddi.com/p/fa332c8953f6a18944e20f0e8cc9754b.js"></script>
<noscript style='position: absolute; left: -10px;'><img
            src='https://i.canddi.com/i.gif?A=fa332c8953f6a18944e20f0e8cc9754b' alt='Canddi'/></noscript>
<script type="text/javascript">setTimeout(function () {
        var a = document.createElement("script");
        var b = document.getElementsByTagName("script")[0];
        a.src = document.location.protocol + "//script.crazyegg.com/pages/scripts/0053/2957.js?" + Math.floor(new Date().getTime() / 3600000);
        a.async = true;
        a.type = "text/javascript";
        b.parentNode.insertBefore(a, b)
    }, 1);</script>
<script type="text/javascript">jQuery(document).ready(function () {

        jQuery(".open-chat-click").click(function () {
            jQuery("#LoAvailableBtn").click();
            return false;
        });

        jQuery('head').prepend('<style>li#shopkeeper-menu-item-18888 a {color: #bfd432 !important;}</style>');

        jQuery.urlParam = function (name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            if (results == null) {
                return null;
            }
            else {
                return results[1] || 0;
            }
        }

        if (jQuery.urlParam('result') == "success") {
            jQuery('#confirmation_s_f').show();
            jQuery('#confirmation_s_p').show();
        }

        jQuery('.single-product .add_to_cart_button, .single-product  .single_add_to_cart_button').html('Add to Cart');
        if (jQuery('body.single-product.single').length && jQuery('.product_description').length) {
            if (true) {
                jQuery('.single_add_to_cart_button').before('<div class="button alt enq-form-button">Enquire Now</div>');

                /*var courseName = jQuery('.product_title.entry-title').text();
                courseName = courseName.replace(/ /g, "-");
                courseName = courseName.replace(/:/g, "");
                courseName = courseName.toLowerCase();

                document.addEventListener( 'wpcf7mailsent', function( event ) {
                    location = 'https://www.academyclass.com/thank-you/?course=' + courseName;
                }, false );*/

                jQuery('.single-product .product_infos .product_description').addClass('enq-form-desc').attr('id', 'enq-form').insertBefore(jQuery('.single-product .product_infos .product_description').parents('.product.type-product').find('.product_meta'));

                jQuery(document).on('click', '.enq-form-button', function () {
                    if (!jQuery(this).hasClass('disabled')) {

                        enqformcurrloc = jQuery('#pa_city').find(':selected').text();

                        jQuery('#enq-form-loc-v').val(enqformcurrloc);

                        enqformcurrdate = jQuery('#pa_dates').find(':selected').text();

                        jQuery('#enq-form-date-v').val(enqformcurrdate);

                        jQuery('#e_form_title').text('Enquire');
                        jQuery('.enq-form-desc').slideDown('medium');

                        jQuery('html, body').animate({
                            scrollTop: jQuery('#enq-form').offset().top - 150
                        }, 500);


                    }
                });

                /*jQuery(document).on('change','#pa_city', function(){
                    enqformcurrloc = jQuery('#pa_city').find(':selected').text();
                    jQuery('.enq-form-loc').val(enqformcurrloc);
                });

                jQuery(document).on('change','#pa_dates', function(){
                    enqformcurrdate = jQuery('#pa_dates').find(':selected').text();
                    jQuery('.enq-form-date').val(enqformcurrdate);
                });

                jQuery(document).on('click change','#pa_city, #pa_city option, #pa_dates, #pa_dates option', function(){
                    if (!jQuery('.single_add_to_cart_button').is(':disabled') ) {
                        jQuery('#e_form_title').text('Enquire');
                        jQuery('.enq-form-desc').slideDown('medium');
                        jQuery('.enq-form-button').removeClass('disabled');
                    } else {
                        jQuery('.enq-form-desc').hide();
                        jQuery('.enq-form-button').addClass('disabled');
                    }
                });*/

            }
        }

        if (jQuery('body.woocommerce-page.archive').length || jQuery('body.page-child').length) {
            jQuery('.product_after_shop_loop_buttons a:contains("Enquire Now")').each(function () {
                jQuery(this).html('Learn more');
            });

            if (jQuery('h3:contains("Information on courses")').length) {
                jQuery('#full-description').addClass('vc_active');
            }

        }

        if (jQuery('body.woocommerce-checkout').length) {


            jQuery('h3#order_review_heading').html('Your Order');


            /*

             var currcheckoutcity = jQuery('dd.variation-City p').html();
             var currcheckoutadd = '121 Vicar Lane';
             jQuery('#billing_address_1').val(currcheckoutadd);
             if (jQuery(currcheckoutcity).length ) {
                jQuery('#billing_city').val(currcheckoutcity);
             } else {
                jQuery('#billing_city').val('London');
             }

             jQuery(document).on('click','.payment_method_worldpay label', function(){
                 jQuery('#billing_address_1_field label, #billing_address_1_field input, #billing_city_field label, #billing_city_field input').slideDown('medium');
                 jQuery('#billing_address_1_field input, #billing_city_field input').val('');
                 jQuery( "#place_order" ).addClass( "worldpay_form" );
             });


             if(jQuery("#payment_method_worldpay").attr("checked") != 'checked') {
                 jQuery('#billing_address_1_field label, #billing_address_1_field input, #billing_city_field label, #billing_city_field input').hide();
             }

            jQuery(document).on('click','.payment_method_bacs label', function(){
                 jQuery('#billing_address_1_field label, #billing_address_1_field input, #billing_city_field label, #billing_city_field input').hide();
                 jQuery('#billing_address_1_field input').val('121 Vicar Lane');
                 jQuery('#billing_city_field input').val('London');
                 jQuery( "#place_order" ).removeClass( "worldpay_form" );
             }); */

        }

        if (jQuery('body.woocommerce-order-received').length) {
            jQuery('.woocommerce-order-received ul.order_details > .order span').html('Order Number:');
            jQuery('h2:contains("Enquiry Details")').html('Order Details');
            jQuery('th:contains("Enquiry Method:")').html('Order Method:');
        }

        jQuery('.archive-product-rating span:contains("(0)")').remove();

        jQuery('.product-title-link:contains("AutoCAD 301 Creating 3D Models")').parent().siblings('.product_after_shop_loop').find('a.product_type_simple').attr('href', '/courses/autocad-301-creating-3d-models/').removeClass('add_to_cart_button ajax_add_to_cart');


        if (jQuery('.product-overlay-duration').length) {
            jQuery('.product-overlay-duration:contains("Personal schedule days")').html('<span>Duration: </span><br/>Personal schedule')
        }

        if (jQuery('.land-courses-items').length) {
            jQuery('#tab-additional-options, #tab-training-options, #tab-funding-discounts, #tab-reviews').hide();
            jQuery('li.description_tab').addClass('active');

            jQuery(document).on('click', '.lct-open', function () {

                var currcourseprice = jQuery(this).parent().find('td:nth-child(4) ins').html();
                var currcoursedate = jQuery(this).parent().find('td:nth-child(2)').html();
                var currcourseloc = jQuery(this).parent().find('td:nth-child(3)').html();
                var currcourseid = jQuery(this).attr('data-course');
                var currcourse = jQuery(".land-courses-items").find("[data-course='" + currcourseid + "']");
                jQuery(currcourse).find(".single_add_to_cart_button").prop("disabled", false);
                jQuery(currcourse).find("#pa_city option:contains('" + currcourseloc + "')").prop('selected', 'true');
                jQuery(currcourse).find("#pa_dates option:contains('" + currcoursedate + "')").prop('selected', 'true');
                jQuery(".land-courses-item").hide();
                currcourse.slideDown('medium');
                jQuery(currcourse).find('p.price').html(currcourseprice);
                jQuery(currcourse).find('.single_add_to_cart_button.button').removeClass('disabled');
                jQuery('html, body').animate({
                    scrollTop: jQuery(currcourse).offset().top - 150
                }, 500);
            });
        }

        jQuery(document).on('click', '.cont-cta-mes', function () {
            jQuery('html, body').animate({
                scrollTop: jQuery('#wpcf7-f553-p509-o3').offset().top - 250
            }, 500);
        });

        if (jQuery('.page-id-261').length) {

            jQuery(document).on('click', '.blog-soc-sharing-e', function () {
                jQuery('.blog-soc-email').slideDown('medium');
            });

            jQuery(document).on('click', '.vc_custom_heading h4', function () {
                window.location = jQuery(this).parent().parent().find('.vc_btn3-container .vc_gitem-link').attr('href');
            });

            jQuery('.blog-search-form').insertBefore('.vc_grid-filter-dropdown .vc_grid-styled-select');
            jQuery('.blog-search-form').delay('5000').show('medium');

        }

        jQuery('.post-type-archive-product .woocommerce-Price-amount.amount, .tax-product_cat .woocommerce-Price-amount.amount, .page-child .woocommerce-Price-amount.amount, .single-product .woocommerce-Price-amount.amount').each(function () {
            if (!jQuery(this).next().length) {
                jQuery(this).after('<small class="woocommerce-price-suffix">+ VAT</small>');
            }
        });

        if (jQuery('body.woocommerce-cart').length) {
            jQuery('.cart_totals h2').html('Cart Totals').show();
            jQuery('tr.fee .woocommerce-price-suffix, .order-total .woocommerce-price-suffix, .product-price .woocommerce-price-suffix').html('(incl. VAT)');
        }

        jQuery(document).on('click', '.become-links a', function (e) {
            e.preventDefault();
            var becomedest = jQuery(this).attr("class");
            jQuery('html, body').animate({
                scrollTop: jQuery('div.' + becomedest).offset().top
            }, 500);
        });

        if (jQuery('.parent-pageid-324').length) {
            jQuery('.vc_tta-panel-body').css('display', 'block');
            jQuery('.vc_tta-panel').removeClass('vc_active');
            jQuery('.vc_tta-panel').removeAttr('data-vc-content');
        }

        jQuery(document).on('click', '.parent-pageid-324 .vc_tta-panel-title a', function (e) {
            e.preventDefault();
        });


        jQuery(document).on('change paste keyup', '#site-footer .wpcf7-email', function () {
            jQuery(this).parents('.wpcf7-form').find('.newsletter-int-options, .newsletter-int-label').slideDown('medium');
        });

        jQuery('.partners-slider > .wpb_column > .vc_column-inner > .wpb_wrapper').owlCarousel({
            items: 8,
            autoPlay: 1200
        });


    });</script>
<script type="application/ld+json">{
    "@context": "http://schema.org",
    "@type": "LocalBusiness",
  "url": "http://www.academyclass.com/",
  "logo": "http://www.academyclass.com/wp-content/uploads/2016/03/logo-aclass.png",
  "image": "http://www.academyclass.com/wp-content/uploads/2016/03/logo-aclass.png",
  "priceRange": "From £199",
  "hasMap": "http://www.academyclass.com/contact-us/",
  "email": "mailto:info@academyclass.com",
    "address": {
      "@type": "PostalAddress",
      "addressLocality": "London",
      "postalCode":"SE17NQ",
      "streetAddress": "39 York Road"
    },
    "geo": {
      "@type": "GeoCoordinates",
      "latitude": "51.503740",
      "longitude": "-0.101748"
    },
    "description": "Market Leaders in Training Courses for Designers and Developers",
    "name": "Academy Class",
    "telephone": "448000438889",
    "openingHours": "Mo,Tu,We,Th,Fr 09:00-17:00",

    "sameAs" : [ "https://www.facebook.com/AcademyClass",
      "https://twitter.com/academyclass",
      "https://plus.google.com/+AcademyClassPodcasts"]
  }
</script>
<script type="text/javascript">_linkedin_data_partner_id = "120480";</script>
<script type="text/javascript">(function () {
        var s = document.getElementsByTagName("script")[0];
        var b = document.createElement("script");
        b.type = "text/javascript";
        b.async = true;
        b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
        s.parentNode.insertBefore(b, s);
    })();</script>
<noscript><img height="1" width="1" style="display:none;" alt=""
               src="https://dc.ads.linkedin.com/collect/?pid=120480&fmt=gif"/></noscript>
<script>(function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-904326-7', 'auto');
    ga('send', 'pageview');</script>
<style type="text/css">.so-widget-sow-image-default-b37b538aacbf .sow-image-container {
        text-align: center
    }

    .so-widget-sow-image-default-b37b538aacbf .sow-image-container .so-widget-image {
        display: inline-block;
        max-width: 100%;
        width: inherit;
        height: auto
    }</style>
<style type="text/css">.so-widget-sow-image-default-9e90369adebe .sow-image-container {
        text-align: right
    }

    .so-widget-sow-image-default-9e90369adebe .sow-image-container .so-widget-image {
        display: inline-block;
        max-width: 100%;
        width: inherit;
        height: auto
    }</style>
<script type="text/javascript">var recaptchaWidgets = [];
    var recaptchaCallback = function () {
        var forms = document.getElementsByTagName('form');
        var pattern = /(^|\s)g-recaptcha(\s|$)/;

        for (var i = 0; i < forms.length; i++) {
            var divs = forms[i].getElementsByTagName('div');

            for (var j = 0; j < divs.length; j++) {
                var sitekey = divs[j].getAttribute('data-sitekey');

                if (divs[j].className && divs[j].className.match(pattern) && sitekey) {
                    var params = {
                        'sitekey': sitekey,
                        'type': divs[j].getAttribute('data-type'),
                        'size': divs[j].getAttribute('data-size'),
                        'theme': divs[j].getAttribute('data-theme'),
                        'badge': divs[j].getAttribute('data-badge'),
                        'tabindex': divs[j].getAttribute('data-tabindex')
                    };

                    var callback = divs[j].getAttribute('data-callback');

                    if (callback && 'function' == typeof window[callback]) {
                        params['callback'] = window[callback];
                    }

                    var expired_callback = divs[j].getAttribute('data-expired-callback');

                    if (expired_callback && 'function' == typeof window[expired_callback]) {
                        params['expired-callback'] = window[expired_callback];
                    }

                    var widget_id = grecaptcha.render(divs[j], params);
                    recaptchaWidgets.push(widget_id);
                    break;
                }
            }
        }
    };

    document.addEventListener('wpcf7submit', function (event) {
        switch (event.detail.status) {
            case 'spam':
            case 'mail_sent':
            case 'mail_failed':
                for (var i = 0; i < recaptchaWidgets.length; i++) {
                    grecaptcha.reset(recaptchaWidgets[i]);
                }
        }
    }, false);</script>
<script type="text/javascript">document.addEventListener('wpcf7mailsent', function (event) {
        // Main contact forms
        if ('6263' == event.detail.contactFormId || '190' == event.detail.contactFormId || '9577' == event.detail.contactFormId) {
            //fbq('track', 'Lead');
        }


        // Autocad form
        if ('13035' == event.detail.contactFormId) {
            var sn_email = jQuery('#fem_adr').val();
            var sn_fnm = jQuery('#fem_fnm').val();
            after_ok_submit(sn_email, sn_fnm);
            //fbq('track', 'Lead');

            /*jQuery.post( "https://t.trackedlink.net/signup.ashx",
            { addressbookid: "31432189",
              userid: "44909",
              ReturnURL: "",
              Email: sn_email,
              cd_FIRSTNAME: sn_fnm } );*/
        }

        // After Effects form
        if ('15091' == event.detail.contactFormId) {
            var sn_email = jQuery('#fem_adr').val();
            var sn_fnm = jQuery('#fem_fnm').val();
            after_ok_submit(sn_email, sn_fnm);
            //fbq('track', 'Lead');

            /*jQuery.post( "https://t.trackedlink.net/signup.ashx",
            { addressbookid: "31642402",
              userid: "44909",
              ReturnURL: "",
              Email: sn_email,
              cd_FIRSTNAME: sn_fnm } );*/
        }

        // Photoshop form
        if ('15717' == event.detail.contactFormId) {
            var sn_email = jQuery('#fem_adr').val();
            var sn_fnm = jQuery('#fem_fnm').val();
            after_ok_submit(sn_email, sn_fnm);
            //fbq('track', 'Lead');

            /*jQuery.post( "https://t.trackedlink.net/signup.ashx",
            { addressbookid: "32235909",
              userid: "44909",
              ReturnURL: "",
              Email: sn_email,
              cd_FIRSTNAME: sn_fnm } );*/
        }

        // Illustrator form
        if ('15959' == event.detail.contactFormId) {
            var sn_email = jQuery('#fem_adr').val();
            var sn_fnm = jQuery('#fem_fnm').val();
            after_ok_submit(sn_email, sn_fnm);
            //fbq('track', 'Lead');

            /*jQuery.post( "https://t.trackedlink.net/signup.ashx",
            { addressbookid: "32882878",
              userid: "44909",
              ReturnURL: "",
              Email: sn_email,
              cd_FIRSTNAME: sn_fnm } );*/
        }

        // Revit form
        if ('15979' == event.detail.contactFormId) {
            var sn_email = jQuery('#fem_adr').val();
            var sn_fnm = jQuery('#fem_fnm').val();
            after_ok_submit(sn_email, sn_fnm);
            //fbq('track', 'Lead');

            /*jQuery.post( "https://t.trackedlink.net/signup.ashx",
            { addressbookid: "32999893",
              userid: "44909",
              ReturnURL: "",
              Email: sn_email,
              cd_FIRSTNAME: sn_fnm } );*/
        }

        // InDesign form
        if ('16132' == event.detail.contactFormId) {
            var sn_email = jQuery('#fem_adr').val();
            var sn_fnm = jQuery('#fem_fnm').val();
            after_ok_submit(sn_email, sn_fnm);
            //fbq('track', 'Lead');

            /*jQuery.post( "https://t.trackedlink.net/signup.ashx",
            { addressbookid: "33222054",
              userid: "44909",
              ReturnURL: "",
              Email: sn_email,
              cd_FIRSTNAME: sn_fnm } );*/
        }

        // CSS3 form
        if ('16299' == event.detail.contactFormId) {
            var sn_email = jQuery('#fem_adr').val();
            var sn_fnm = jQuery('#fem_fnm').val();
            after_ok_submit(sn_email, sn_fnm);
            //fbq('track', 'Lead');

            /*jQuery.post( "https://t.trackedlink.net/signup.ashx",
            { addressbookid: "33695357",
              userid: "44909",
              ReturnURL: "",
              Email: sn_email,
              cd_FIRSTNAME: sn_fnm } );*/
        }

        // Cinema 4D form
        if ('16400' == event.detail.contactFormId) {
            var sn_email = jQuery('#fem_adr').val();
            var sn_fnm = jQuery('#fem_fnm').val();
            after_ok_submit(sn_email, sn_fnm);
            //fbq('track', 'Lead');

            /*jQuery.post( "https://t.trackedlink.net/signup.ashx",
            { addressbookid: "33870141",
              userid: "44909",
              ReturnURL: "",
              Email: sn_email,
              cd_FIRSTNAME: sn_fnm } );*/
        }

        // Course enquire form
        if ('9577' == event.detail.contactFormId
            || '553' == event.detail.contactFormId
            || '190' == event.detail.contactFormId
            || '6232' == event.detail.contactFormId) {

            var sn_name = '';
            var sn_email = '';
            var sn_course = jQuery('.product_title.entry-title').text();
            var sn_phone = '';
            var sn_location = jQuery('select[name=your-city]').val();

            if (typeof sn_location === 'undefined') {
                sn_location = jQuery('#pa_city').find(':selected').text();
                if (typeof sn_location === 'undefined' || sn_location == 'Choose an option') {
                    sn_location = '';
                }
            }

            var inputs = event.detail.inputs;

            for (var i = 0; i < inputs.length; i++) {
                if ('your-name' == inputs[i].name) {
                    sn_name = inputs[i].value;
                }
                if ('email' == inputs[i].name) {
                    sn_email = inputs[i].value;
                }
                if ('your-phone' == inputs[i].name) {
                    sn_phone = inputs[i].value;
                }
                if ('your-city' == inputs[i].name) {
                    sn_location = inputs[i].value;
                }
            }

            if (jQuery('input[name="checkbox-842[]"]:checked').length > 0) {

                jQuery.post("https://academyclass.us17.list-manage.com/subscribe/post?u=8fec92477866d5e60ce3f8adf&amp;id=603393ae35",
                    {
                        EMAIL: sn_email,
                        FNAME: sn_name,
                        COURSE: sn_course,
                        PHONE: sn_phone,
                        LOCATION: sn_location
                    });
            }


            var courseName = jQuery('.product_title.entry-title').text();

            if (courseName == '') {
                courseName = jQuery('h1:first').text();
            }

            courseName = courseName.replace(/ /g, "-");
            courseName = courseName.replace(/:/g, "");
            courseName = courseName.toLowerCase();

            location = 'https://www.academyclass.com/thank-you-for-enquire/?course=' + courseName;
        }

    }, false);</script>
<div id="om-w3c0hrmq7y14lycszekg-holder"></div>
<script>var w3c0hrmq7y14lycszekg, w3c0hrmq7y14lycszekg_poll = function () {
        var r = 0;
        return function (n, l) {
            clearInterval(r), r = setInterval(n, l)
        }
    }();
    !function (e, t, n) {
        if (e.getElementById(n)) {
            w3c0hrmq7y14lycszekg_poll(function () {
                if (window['om_loaded']) {
                    if (!w3c0hrmq7y14lycszekg) {
                        w3c0hrmq7y14lycszekg = new OptinMonsterApp();
                        return w3c0hrmq7y14lycszekg.init({"u": "21964.684752", "staging": 0, "dev": 0, "beta": 0});
                    }
                }
            }, 25);
            return;
        }
        var d = false, o = e.createElement(t);
        o.id = n, o.src = "https://a.optmstr.com/app/js/api.min.js", o.async = true, o.onload = o.onreadystatechange = function () {
            if (!d) {
                if (!this.readyState || this.readyState === "loaded" || this.readyState === "complete") {
                    try {
                        d = om_loaded = true;
                        w3c0hrmq7y14lycszekg = new OptinMonsterApp();
                        w3c0hrmq7y14lycszekg.init({"u": "21964.684752", "staging": 0, "dev": 0, "beta": 0});
                        o.onload = o.onreadystatechange = null;
                    } catch (t) {
                    }
                }
            }
        };
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(o)
    }(document, "script", "omapi-script");</script>
<div id="om-qyvtygzzpupcnnkhpk1t-holder"></div>
<script>var qyvtygzzpupcnnkhpk1t, qyvtygzzpupcnnkhpk1t_poll = function () {
        var r = 0;
        return function (n, l) {
            clearInterval(r), r = setInterval(n, l)
        }
    }();
    !function (e, t, n) {
        if (e.getElementById(n)) {
            qyvtygzzpupcnnkhpk1t_poll(function () {
                if (window['om_loaded']) {
                    if (!qyvtygzzpupcnnkhpk1t) {
                        qyvtygzzpupcnnkhpk1t = new OptinMonsterApp();
                        return qyvtygzzpupcnnkhpk1t.init({"u": "21964.684749", "staging": 0, "dev": 0, "beta": 0});
                    }
                }
            }, 25);
            return;
        }
        var d = false, o = e.createElement(t);
        o.id = n, o.src = "https://a.optmstr.com/app/js/api.min.js", o.async = true, o.onload = o.onreadystatechange = function () {
            if (!d) {
                if (!this.readyState || this.readyState === "loaded" || this.readyState === "complete") {
                    try {
                        d = om_loaded = true;
                        qyvtygzzpupcnnkhpk1t = new OptinMonsterApp();
                        qyvtygzzpupcnnkhpk1t.init({"u": "21964.684749", "staging": 0, "dev": 0, "beta": 0});
                        o.onload = o.onreadystatechange = null;
                    } catch (t) {
                    }
                }
            }
        };
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(o)
    }(document, "script", "omapi-script");</script>
<div id="om-ec4ip8lrofqkj6lw1pny-holder"></div>
<script>var ec4ip8lrofqkj6lw1pny, ec4ip8lrofqkj6lw1pny_poll = function () {
        var r = 0;
        return function (n, l) {
            clearInterval(r), r = setInterval(n, l)
        }
    }();
    !function (e, t, n) {
        if (e.getElementById(n)) {
            ec4ip8lrofqkj6lw1pny_poll(function () {
                if (window['om_loaded']) {
                    if (!ec4ip8lrofqkj6lw1pny) {
                        ec4ip8lrofqkj6lw1pny = new OptinMonsterApp();
                        return ec4ip8lrofqkj6lw1pny.init({"u": "21964.684745", "staging": 0, "dev": 0, "beta": 0});
                    }
                }
            }, 25);
            return;
        }
        var d = false, o = e.createElement(t);
        o.id = n, o.src = "https://a.optmstr.com/app/js/api.min.js", o.async = true, o.onload = o.onreadystatechange = function () {
            if (!d) {
                if (!this.readyState || this.readyState === "loaded" || this.readyState === "complete") {
                    try {
                        d = om_loaded = true;
                        ec4ip8lrofqkj6lw1pny = new OptinMonsterApp();
                        ec4ip8lrofqkj6lw1pny.init({"u": "21964.684745", "staging": 0, "dev": 0, "beta": 0});
                        o.onload = o.onreadystatechange = null;
                    } catch (t) {
                    }
                }
            }
        };
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(o)
    }(document, "script", "omapi-script");</script>
<div id="om-tmpzdcycy8mi3t8b-holder"></div>
<script>var tmpzdcycy8mi3t8b, tmpzdcycy8mi3t8b_poll = function () {
        var r = 0;
        return function (n, l) {
            clearInterval(r), r = setInterval(n, l)
        }
    }();
    !function (e, t, n) {
        if (e.getElementById(n)) {
            tmpzdcycy8mi3t8b_poll(function () {
                if (window['om_loaded']) {
                    if (!tmpzdcycy8mi3t8b) {
                        tmpzdcycy8mi3t8b = new OptinMonsterApp();
                        return tmpzdcycy8mi3t8b.init({"u": "21964.403338", "staging": 0, "dev": 0, "beta": 0});
                    }
                }
            }, 25);
            return;
        }
        var d = false, o = e.createElement(t);
        o.id = n, o.src = "https://a.optmstr.com/app/js/api.min.js", o.async = true, o.onload = o.onreadystatechange = function () {
            if (!d) {
                if (!this.readyState || this.readyState === "loaded" || this.readyState === "complete") {
                    try {
                        d = om_loaded = true;
                        tmpzdcycy8mi3t8b = new OptinMonsterApp();
                        tmpzdcycy8mi3t8b.init({"u": "21964.403338", "staging": 0, "dev": 0, "beta": 0});
                        o.onload = o.onreadystatechange = null;
                    } catch (t) {
                    }
                }
            }
        };
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(o)
    }(document, "script", "omapi-script");</script>
<div id="om-cneiybknp3porw3e-holder"></div>
<script>var cneiybknp3porw3e, cneiybknp3porw3e_poll = function () {
        var r = 0;
        return function (n, l) {
            clearInterval(r), r = setInterval(n, l)
        }
    }();
    !function (e, t, n) {
        if (e.getElementById(n)) {
            cneiybknp3porw3e_poll(function () {
                if (window['om_loaded']) {
                    if (!cneiybknp3porw3e) {
                        cneiybknp3porw3e = new OptinMonsterApp();
                        return cneiybknp3porw3e.init({"u": "21964.403345", "staging": 0, "dev": 0, "beta": 0});
                    }
                }
            }, 25);
            return;
        }
        var d = false, o = e.createElement(t);
        o.id = n, o.src = "https://a.optmstr.com/app/js/api.min.js", o.async = true, o.onload = o.onreadystatechange = function () {
            if (!d) {
                if (!this.readyState || this.readyState === "loaded" || this.readyState === "complete") {
                    try {
                        d = om_loaded = true;
                        cneiybknp3porw3e = new OptinMonsterApp();
                        cneiybknp3porw3e.init({"u": "21964.403345", "staging": 0, "dev": 0, "beta": 0});
                        o.onload = o.onreadystatechange = null;
                    } catch (t) {
                    }
                }
            }
        };
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(o)
    }(document, "script", "omapi-script");</script>
<div id="om-xowhzpxe42o8vrdw-holder"></div>
<script>var xowhzpxe42o8vrdw, xowhzpxe42o8vrdw_poll = function () {
        var r = 0;
        return function (n, l) {
            clearInterval(r), r = setInterval(n, l)
        }
    }();
    !function (e, t, n) {
        if (e.getElementById(n)) {
            xowhzpxe42o8vrdw_poll(function () {
                if (window['om_loaded']) {
                    if (!xowhzpxe42o8vrdw) {
                        xowhzpxe42o8vrdw = new OptinMonsterApp();
                        return xowhzpxe42o8vrdw.init({"u": "21964.402057", "staging": 0, "dev": 0, "beta": 0});
                    }
                }
            }, 25);
            return;
        }
        var d = false, o = e.createElement(t);
        o.id = n, o.src = "https://a.optmstr.com/app/js/api.min.js", o.async = true, o.onload = o.onreadystatechange = function () {
            if (!d) {
                if (!this.readyState || this.readyState === "loaded" || this.readyState === "complete") {
                    try {
                        d = om_loaded = true;
                        xowhzpxe42o8vrdw = new OptinMonsterApp();
                        xowhzpxe42o8vrdw.init({"u": "21964.402057", "staging": 0, "dev": 0, "beta": 0});
                        o.onload = o.onreadystatechange = null;
                    } catch (t) {
                    }
                }
            }
        };
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(o)
    }(document, "script", "omapi-script");</script>
<script type="text/javascript">var w3c0hrmq7y14lycszekg_shortcode = true;
    var qyvtygzzpupcnnkhpk1t_shortcode = true;
    var ec4ip8lrofqkj6lw1pny_shortcode = true;
    var tmpzdcycy8mi3t8b_shortcode = true;
    var cneiybknp3porw3e_shortcode = true;
    var xowhzpxe42o8vrdw_shortcode = true;</script>
<link rel='stylesheet' id='font-awesome-css'
      href='https://www.academyclass.com/wp-content/plugins/js_composer/assets/lib/bower/font-awesome/css/font-awesome.min.css'
      type='text/css' media='all'/>
<link rel='stylesheet' id='magnific-popup-css'
      href='https://www.academyclass.com/wp-content/plugins/shortcodes-ultimate/assets/css/magnific-popup.css'
      type='text/css' media='all'/>
<script type='text/javascript'>/* <![CDATA[ */
    var wpcf7 = {
        "apiSettings": {
            "root": "https:\/\/www.academyclass.com\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }, "recaptcha": {"messages": {"empty": "Please verify that you are not a robot."}}, "cached": "1"
    };
    /* ]]> */</script>
<script type='text/javascript'
        src='https://www.academyclass.com/wp-content/plugins/contact-form-7/includes/js/scripts.js'></script>
<script type='text/javascript'
        src='https://www.academyclass.com/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js'></script>
<script type='text/javascript'
        src='https://www.academyclass.com/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js'></script>
<script type='text/javascript'>/* <![CDATA[ */
    var woocommerce_params = {"ajax_url": "\/wp-admin\/admin-ajax.php", "wc_ajax_url": "\/?wc-ajax=%%endpoint%%"};
    /* ]]> */</script>
<script type='text/javascript'
        src='https://www.academyclass.com/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js'></script>
<script type='text/javascript'>/* <![CDATA[ */
    var wc_cart_fragments_params = {
        "ajax_url": "\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
        "fragment_name": "wc_fragments_f3142d26797a04c03492624ffd37665d"
    };
    /* ]]> */</script>
<script type='text/javascript'
        src='https://www.academyclass.com/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js'></script>
<script type='text/javascript'
        src='https://chimpstatic.com/mcjs-connected/js/users/8fec92477866d5e60ce3f8adf/51d487720b50568278f414e60.js'></script>
<script type='text/javascript'>/* <![CDATA[ */
    var coursesdisc_array = {"admin_ajax": "https:\/\/www.academyclass.com\/wp-admin\/admin-ajax.php"};
    /* ]]> */</script>
<script type='text/javascript'
        src='https://www.academyclass.com/wp-content/themes/shopkeeper-child//js/discountcourses.js'></script>
<script type='text/javascript'>/* <![CDATA[ */
    var courses_array = {"admin_ajax": "https:\/\/www.academyclass.com\/wp-admin\/admin-ajax.php"};
    /* ]]> */</script>
<script type='text/javascript'
        src='https://www.academyclass.com/wp-content/themes/shopkeeper-child//js/mailcourses.js'></script>
<script type='text/javascript' src='https://www.academyclass.com/wp-includes/js/underscore.min.js'></script>
<script type='text/javascript'>/* <![CDATA[ */
    var _wpUtilSettings = {"ajax": {"url": "\/wp-admin\/admin-ajax.php"}};
    /* ]]> */</script>
<script type='text/javascript' src='https://www.academyclass.com/wp-includes/js/wp-util.min.js'></script>
<script type='text/javascript'>/* <![CDATA[ */
    var wc_add_to_cart_variation_params = {
        "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
        "i18n_no_matching_variations_text": "Sorry, no products matched your selection. Please choose a different combination.",
        "i18n_make_a_selection_text": "Please select some product options before adding this product to your cart.",
        "i18n_unavailable_text": "Sorry, this product is unavailable. Please choose a different combination."
    };
    /* ]]> */</script>
<script type='text/javascript'
        src='https://www.academyclass.com/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart-variation.min.js'></script>
<script type='text/javascript'
        src='https://www.academyclass.com/wp-content/themes/shopkeeper/js/scripts-dist.js'></script>
<script type='text/javascript'
        src='https://www.academyclass.com/wp-content/plugins/wordpress-countdown-widget/js/jquery.countdown.min.js'></script>
<script type='text/javascript'>/* <![CDATA[ */
    var fcaPcEvents = [];
    var fcaPcDebug = {"debug": ""};
    var fcaPcPost = {
        "title": "Home 2018",
        "type": "page",
        "id": "19152",
        "categories": [],
        "utm_support": "",
        "user_parameters": "",
        "edd_delay": "0",
        "woo_delay": "0",
        "edd_enabled": "",
        "woo_enabled": ""
    };
    /* ]]> */</script>
<script type='text/javascript'
        src='https://www.academyclass.com/wp-content/plugins/facebook-conversion-pixel/pixel-cat.min.js'></script>
<script type='text/javascript'
        src='https://www.academyclass.com/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js'></script>
<script type='text/javascript'
        src='https://www.google.com/recaptcha/api.js?onload=recaptchaCallback&#038;render=explicit&#038;ver=2.0'></script>
<script type='text/javascript'>/* <![CDATA[ */
    var su_magnific_popup = {
        "close": "Close (Esc)",
        "loading": "Loading...",
        "prev": "Previous (Left arrow key)",
        "next": "Next (Right arrow key)",
        "counter": "%curr% of %total%",
        "error": "Failed to load this link. <a href=\"%url%\" target=\"_blank\"><u>Open link<\/u><\/a>."
    };
    /* ]]> */</script>
<script type='text/javascript'
        src='https://www.academyclass.com/wp-content/plugins/shortcodes-ultimate/assets/js/magnific-popup.js'></script>
<script type='text/javascript'>/* <![CDATA[ */
    var su_other_shortcodes = {"no_preview": "This shortcode doesn't work in live preview. Please insert it into editor and preview on the site."};
    /* ]]> */</script>
<script type='text/javascript'
        src='https://www.academyclass.com/wp-content/plugins/shortcodes-ultimate/assets/js/other-shortcodes.js'></script>
<script type="text/javascript">jQuery(function ($) {
        tvc_lc = "GBP";

        homepage_json_ATC_link = [];

        tvc_fp = [];

        tvc_rcp = [];

        tvc_rdp = [];

        prodpage_json_ATC_link = [];

        tvc_pgc = [];

        catpage_json_ATC_link = [];


        var items = [];
        //set local currencies
        gtag("set", {"currency": tvc_lc});

        function t_products_impre_clicks(t_json_name, t_action) {
            t_send_threshold = 0;
            t_prod_pos = 0;

            t_json_length = Object.keys(t_json_name).length;

            for (var t_item in t_json_name) {
                t_send_threshold++;
                t_prod_pos++;
                items.push({
                    "id": t_json_name[t_item].tvc_i,
                    "name": t_json_name[t_item].tvc_n,
                    "category": t_json_name[t_item].tvc_c,
                    "price": t_json_name[t_item].tvc_p,
                    "list_position": t_json_name[t_item].tvc_po,
                });

                if (t_json_length > 20) {
                    if ((t_send_threshold % 20) == 0) {
                        t_json_length = t_json_length - 20;
                        gtag("event", "view_item_list", {
                            "event_category": "Enhanced-Ecommerce",
                            "event_label": "product_impression_" + t_action, "items": items, "non_interaction": true
                        });
                    }
                } else {

                    t_json_length--;
                    if (t_json_length == 0) {
                        gtag("event", "view_item_list", {
                            "event_category": "Enhanced-Ecommerce",
                            "event_label": "product_impression_" + t_action, "items": items, "non_interaction": true
                        });
                    }
                }
            }
        }

        //function for comparing urls in json object
        function prod_exists_in_JSON(t_url, t_json_name, t_action) {
            if (t_json_name.hasOwnProperty(t_url)) {
                t_call_fired = true;
                gtag("event", "select_content", {
                    "event_category": "Enhanced-Ecommerce",
                    "event_label": "product_click_" + t_action,
                    "content_type": "product",
                    "items": [
                        {
                            "id": t_json_name[t_url].tvc_i,
                            "name": t_json_name[t_url].tvc_n,
                            "category": t_json_name[t_url].tvc_c,
                            "price": t_json_name[t_url].tvc_p,
                            "list_position": t_json_name[t_url].tvc_po,
                        }
                    ],
                    "non_interaction": true
                });
            } else {
                t_call_fired = false;
            }
            return t_call_fired;
        }

        function prod_ATC_link_exists(t_url, t_ATC_json_name, t_prod_data_json, t_qty) {
            t_prod_url_key = t_ATC_json_name[t_url]["ATC-link"];

            if (t_prod_data_json.hasOwnProperty(t_prod_url_key)) {
                t_call_fired = true;
                // Enhanced E-commerce Add to cart clicks
                gtag("event", "add_to_cart", {
                    "event_category": "Enhanced-Ecommerce",
                    "event_label": "add_to_cart_click",
                    "non_interaction": true,
                    "items": [{
                        "id": t_prod_data_json[t_prod_url_key].tvc_i,
                        "name": t_prod_data_json[t_prod_url_key].tvc_i,
                        "category": t_prod_data_json[t_prod_url_key].tvc_c,
                        "price": t_prod_data_json[t_prod_url_key].tvc_p,
                        "quantity": t_qty
                    }]
                });

            } else {
                t_call_fired = false;
            }
            return t_call_fired;

        }


        if (tvc_fp.length !== 0) {
            t_products_impre_clicks(tvc_fp, "fp");
        }
        if (tvc_rcp.length !== 0) {
            t_products_impre_clicks(tvc_rcp, "rp");
        }
        jQuery("a:not([href*=add-to-cart],.product_type_variable, .product_type_grouped)").on("click", function () {
            t_url = jQuery(this).attr("href");
            //home page call for click
            t_call_fired = prod_exists_in_JSON(t_url, tvc_fp, "fp");
            if (!t_call_fired) {
                prod_exists_in_JSON(t_url, tvc_rcp, "rp");
            }
        });
        //ATC click
        jQuery("a[href*=add-to-cart]").on("click", function () {
            t_url = jQuery(this).attr("href");
            t_qty = $(this).parent().find("input[name=quantity]").val();
            //default quantity 1 if quantity box is not there
            if (t_qty == "" || t_qty === undefined) {
                t_qty = "1";
            }
            t_call_fired = prod_ATC_link_exists(t_url, homepage_json_ATC_link, tvc_fp, t_qty);
            if (!t_call_fired) {
                prod_ATC_link_exists(t_url, homepage_json_ATC_link, tvc_rcp, t_qty);
            }
        });


        tvc_smd = {
            "tvc_wcv": "3.1.1",
            "tvc_wpv": "4.8.1",
            "tvc_eev": "2.0.0",
            "tvc_cnf": {"t_ee": "yes", "t_df": false, "t_gUser": false, "t_UAen": "yes", "t_thr": "20"}
        };
    });</script>
<style type="text/css">@media only screen and (max-width: 768px) {
        .su-column {
            width: 48% !important;
            margin: 0 0 1.5em 1% !important;
            float: left !important
        }

        .su-row .su-column:last-child {
            margin-bottom: 0 !important;
            float: left
        }
    }</style>
<script type="text/javascript">(function ($) {
        $.countdown.regional['custom'] = {
            labels: ['Years', 'Months', 'Weeks', 'Days', 'Hours', 'Minutes', 'Seconds'],
            labels1: ['Year', 'Month', 'Week', 'Day', 'Hour', 'Minute', 'Second'],
            compactLabels: ['y', 'a', 'h', 'g'],
            whichLabels: null,
            timeSeparator: ':', isRTL: false
        };
        $.countdown.setDefaults($.countdown.regional['custom']);
    })(jQuery);</script>
<script type="text/javascript">var omapi_localized = {
        ajax: 'https://www.academyclass.com/wp-admin/admin-ajax.php?optin-monster-ajax-route=1',
        nonce: '761d6e1599',
        slugs: {
            "w3c0hrmq7y14lycszekg": {"slug": "w3c0hrmq7y14lycszekg", "mailpoet": false},
            "qyvtygzzpupcnnkhpk1t": {"slug": "qyvtygzzpupcnnkhpk1t", "mailpoet": false},
            "ec4ip8lrofqkj6lw1pny": {"slug": "ec4ip8lrofqkj6lw1pny", "mailpoet": false},
            "tmpzdcycy8mi3t8b": {"slug": "tmpzdcycy8mi3t8b", "mailpoet": false},
            "cneiybknp3porw3e": {"slug": "cneiybknp3porw3e", "mailpoet": false},
            "xowhzpxe42o8vrdw": {"slug": "xowhzpxe42o8vrdw", "mailpoet": false}
        }
    };</script>
<script>(function (w, d) {
        var b = d.getElementsByTagName("body")[0];
        var s = d.createElement("script");
        s.async = true;
        var v = !("IntersectionObserver" in w) ? "8.7.1" : "10.5.2";
        s.src = "https://www.academyclass.com/wp-content/plugins/wp-rocket/inc/front/js/lazyload-" + v + ".min.js";
        w.lazyLoadOptions = {
            elements_selector: "img, iframe",
            data_src: "lazy-src",
            data_srcset: "lazy-srcset",
            skip_invisible: false,
            class_loading: "lazyloading",
            class_loaded: "lazyloaded",
            threshold: 300,
            callback_load: function (element) {
                if (element.tagName === "IFRAME" && element.dataset.rocketLazyload == "fitvidscompatible") {
                    if (element.classList.contains("lazyloaded")) {
                        if (typeof window.jQuery != "undefined") {
                            if (jQuery.fn.fitVids) {
                                jQuery(element).parent().fitVids();
                            }
                        }
                    }
                }
            }
        }; // Your options here. See "recipes" for more information about async.
        b.appendChild(s);
    }(window, document));

    // Listen to the Initialized event
    window.addEventListener('LazyLoad::Initialized', function (e) {
        // Get the instance and puts it in the lazyLoadInstance variable
        var lazyLoadInstance = e.detail.instance;

        var observer = new MutationObserver(function (mutations) {
            mutations.forEach(function (mutation) {
                lazyLoadInstance.update();
            });
        });

        var b = document.getElementsByTagName("body")[0];
        var config = {childList: true, subtree: true};

        observer.observe(b, config);
    }, false);</script>
<script type="text/javascript">function setCookie(name, value, days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
        document.cookie = name + "=" + value + expires + ";path=/";
    }

    function getParam(p) {
        var match = RegExp('[?&]' + p + '=([^&]*)').exec(window.location.search);
        return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
    }

    var gclid = getParam('gclid');
    if (gclid) {
        var gclsrc = getParam('gclsrc');
        if (!gclsrc || gclsrc.indexOf('aw') !== -1) {
            setCookie('gclid', gclid, 90);
        }
    }</script>
<script>function readCookie(name) {
        var n = name + "=";
        var cookie = document.cookie.split(';');
        for (var i = 0; i < cookie.length; i++) {
            var c = cookie[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1, c.length);
            }
            if (c.indexOf(n) == 0) {
                return c.substring(n.length, c.length);
            }
        }
        return null;
    }

    window.onload = function () {
        document.getElementById('gclid_field').value = readCookie('gclid');
    }</script>
</body>
</html>
<!-- This website is like a Rocket, isn't it? Performance optimized by WP Rocket. Learn more: https://wp-rocket.me - Debug: cached@1528968394 -->