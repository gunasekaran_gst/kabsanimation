@extends('layout.main')
@section('title', 'Home | Animation Center in Krishnagiri')
@section('keywords', 'Animation Center in Krishnagiri')
@section('description', 'Animation Center in Krishnagiri')
@section('content')
    <div class="container-fluid sanpshot home_bg common_bg_style">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="container">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 professionals">
                    <h2><strong>Real Training for Real People by Real Professionals!</strong></h2>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_bottom_ten_em">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " href="{{ url('/') }}">
                        <button class="browser courses_button">BROWSES COURSES</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid sanpshot home_bg_one common_bg_style">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 course">
                <h2>Choose From Our Top Training Courses!</h2>
            </div>
        </div>
        <div class="container-fluid ss_sanpshot">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="container">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 training_courses">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/photoshop') }}">
                                <img src="{{URL::asset('image/home_image/adobecer.jpg') }}">
                                <div class="absolute">Photoshop</div>
                            </a>

                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 training_courses">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/indesign') }}">
                                <img src="{{URL::asset('image/home_image/icons_80-1.jpg') }}">
                                <div class="absolute">InDesign</div>
                            </a>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 training_courses">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/illustrator') }}">
                                <img src="{{URL::asset('image/home_image/icons_56-1.jpg') }}">
                                <div class="absolute">Illustrator</div>
                            </a>

                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 training_courses">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/after_effects') }}">
                                <img src="{{URL::asset('image/home_image/icons_47.jpg') }}">
                                <div class="absolute">AfterEffects</div>
                            </a>

                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 training_courses">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/premierepro') }}">
                                <img src="{{URL::asset('image/home_image/icons_51.jpg') }}">
                                <div class="absolute">premierePro</div>
                            </a>

                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 training_courses">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/autocad') }}">
                                <img src="{{URL::asset('image/home_image/icons_57.jpg') }}">
                                <div class="absolute">AutoCAD</div>
                            </a>

                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 training_courses">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href=" {{ url('/revit') }}">
                                <img src="{{URL::asset('image/home_image/icons_39-1.jpg') }}">
                                <div class="absolute">Revit</div>
                            </a>

                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 training_courses">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href=" {{ url('/dsmax') }}">
                                <img src="{{URL::asset('image/home_image/icons_05.jpg') }}">
                                <div class="absolute">3dsMax</div>
                            </a>

                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 training_courses">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href=" {{ url('/cinema') }}">
                                <img src="{{URL::asset('image/home_image/icons_101-1.jpg') }}">
                                <div class="absolute">Cinema4D</div>
                            </a>

                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 training_courses">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/uxdesign') }}">
                                <img src="{{URL::asset('image/home_image/icons_114.jpg') }}">
                                <div class="absolute">UxDesign</div>
                            </a>

                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 training_courses">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/maya') }}">
                                <img src="{{URL::asset('image/home_image/icons_28-2.jpg') }}">
                                <div class="absolute">Maya</div>
                            </a>

                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 training_courses">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/creativecloud') }}">
                                <img src="{{URL::asset('image/home_image/icons_59.jpg') }}">
                                <div class="absolute">HTML</div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                <a href="{{ url('/') }}">
                    <button class="browser courses_button">BROWSES ALL COURSES</button>
                </a>
            </div>
        </div>
    </div>

    <div class="container-fluid indival">
        <div class="container">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 courses_img">
                    <img src="{{URL::asset('image/training/Individual.jpg') }}">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 courses_img ">
                    <h6><strong>Individual Training</strong></h6>
                    <h5> Do you want to tone up your </h5>
                    <h5> skills? Launch a new career </h5>
                    <h5> or start your own business? </h5>
                    <h5> We can help you.</h5>
                    {{--<a href="#contact"><h5><font color="#daa520"> Click here </font></h5></a>--}}
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 courses_img">
                    <img src="{{URL::asset('image/training/Corporate.jpg') }}">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 courses_img">
                    <h6><strong>Corporate Training</strong></h6>
                    <h5> Send us your Design & </h5>
                    <h5> Development teams to </h5>
                    <h5> unleash their full potential </h5>
                    <h5> and receive great group </h5>
                    <h5> discounts.
                        {{--<a href="#contact"> <span style="color:#daa520"> Learn how here </span> </a>--}}
                    </h5>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid indival">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 academy_class">
                    <h2>KABS ANIMATION</h2>
                    <div class="three"><strong>Market Leaders in Training Courses for Designers and Developers</strong>
                    </div>
                    <p>Our training is specifically developed to keep giving you the edge in your career or business.</p>
                    <p>This is what we like to call “the how and the now!”</p>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid ss_sanpshot">
        <div class="container">
            <h1><strong>Class Snapshots</strong></h1>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                        <img src="{{URL::asset('image/sanpshot/1.jpg') }}">
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                        <img src="{{URL::asset('image/sanpshot/2.jpg') }}">
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                        <img src="{{URL::asset('image/sanpshot/3.jpg') }}">
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                        <img src="{{URL::asset('image/sanpshot/4.jpg') }}">
                    </a>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                        <img src="{{URL::asset('image/sanpshot/5.jpg') }}">
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                        <img src="{{URL::asset('image/sanpshot/6.jpg') }}">

                    </a>

                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                        <img src="{{URL::asset('image/sanpshot/7.jpg') }}">
                    </a>

                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                        <img src="{{URL::asset('image/sanpshot/8.jpg') }}">
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid indival">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 whichever">
                    <h1><strong>Whichever Way You Want To Learn</strong></h1>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="course_information_paragraphs"><img class="right" src="{{URL::asset('image/cb.jpg') }}" width="20" height="20"><strong>Classroom</strong>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="course_information_paragraphs"><img class="right" src="{{URL::asset('image/cb.jpg') }}" width="20" height="20"><strong>Customised</strong>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="course_information_paragraphs"><img class="right" src="{{URL::asset('image/cb.jpg') }}" width="20" height="20"><strong>Live-Online</strong>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="course_information_paragraphs"><img class="right" src="{{URL::asset('image/cb.jpg') }}" width="20" height="20">
                        <strong>Individual / Corporate</strong></div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid indival">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 after_your">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <img class="whicher_img" src="{{URL::asset('image/whicher.jpg') }}" width="659" height="463">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="careerss">
                        <div class="whichever_para"><strong>
                                AFTER OUR COURSES YOU WILL:</strong></div>
                        <ul>
                            <li>Improve your professional skill set</li>
                            <li>Build and grow your portfolio</li>
                            <li>Solve complex challenges people typically face</li>
                            <li>Get great tips and tricks from practicing, professional instructors
                            </li>
                            <li>Increase your confidence to develop your own business</li>
                            <li><strong>Become an accredited specialist</strong> (ace, aca, acu, acp, etc).
                                Read more here</li>
                            <li><strong>Get digital certificates</strong><br>
                                (Great for uploading to your LinkedIn profile and other social media)</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid indival">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h1><strong>Still Not Convinced?</strong></h1>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <div class="still"><strong>18-Month Free<br>Class Retake</strong>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <h>If you have any gaps in your knowledge or want to refresh your skills, you are more than
                                welcome to come back and retake the live online class free of charge up to 18 months after
                                you have taken the class.
                            </h>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <div class="still"><strong>Money-Back <br>
                                    Guarantee</strong>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <h>If you don’t absolutely LOVE your class, we’ll give you a full refund! Let us know on the
                                FIRST day of your training if something isn’t quite right and give us a chance to fix it or
                                give you your money back.
                            </h>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <div class="still"><strong>Lower Price<br>Guarantee</strong>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <h>We think our prices are pretty fair but we won’t be beaten on our fee. We’ll match and
                                discount by 10% any like-for-like Training course price.
                            </h>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <div class="still"><strong>Experienced<br>Instructors</strong>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <h>Equipped with years of industry experience our instructors will assure a successful leap in
                                your knowledge, improvement and preparation.
                            </h>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="two"><strong>“The investment in knowledge pays the best interests.”</strong></div>
                    <br>
                    <p>~ Benjamin Franklin</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 course">
                <h2>That’s us. Now it’s up to you. Enquire now!</h2>
            </div>
        </div>
    </div>
    <div class="container-fluid form_image common_bg_style">
        <div class="container">
            <form method="post" action="#">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <select id="country" name="country" required>
                            <option value="au">Choose Location</option>
                            <option value="au">Australia</option>
                            <option value="ca">Canada</option>
                            <option value="usa">USA</option>
                            <option value="usa">Other</option>
                        </select>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <input type="text" id="fname" name="fname" placeholder="Company" required>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <input type="text" id="fname" name="fname" placeholder="Your name*" required>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <input type="text" id="fname" name="fname" placeholder="Your email*" required>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <input type="text" id="fname" name="fname" placeholder="Your Phone" required>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <textarea placeholder="Your message*" required></textarea>
                        <div class="special_offers">
                        <input type="checkbox" name="checkbox" value="">
                      I would like to get news about courses and special offers</div>
                        <button input type="submit" name="submit" class="btn">ENQUIRE NOW</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="container-fluid lovings">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <img src="{{URL::asset('image/sanpshot/10.jpg') }}" width="900" height="145">
            </div>
        </div>
    </div>
@endsection


