<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content='@yield('keywords')'>
    <meta name="description" content='@yield('description')'>
    <title>Kabs Animation - @yield('title')</title>
    <link rel="stylesheet" href="{{URL::asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/style.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/guna.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/responsive.css') }}">
    <script src="{{URL::asset('js/jquery.js') }}"></script>
    <script src="{{URL::asset('js/bootstrap.js') }}"></script>
    <script src="{{URL::asset('js/custom.js') }}"></script>
</head>
<body>
<div id="mySidenav" class="sidenav">
    <a href="#" class="closebtn">&times;</a>
    <a href="{{ url('/') }}">Access All Areas</a>
    <a class="sid" href="{{ url('/') }}">Training vouchers</a>
    <a href="{{ url('/') }}"> Learning Paths</a>
    <a class="sid" href="{{ url('/') }}">A 3D Animator</a>
    <a class="sid" href="{{ url('/') }}">A CAD Visualiser</a>
    <a class="sid" href="{{ url('/') }}">A Designer</a>
    <a class="sid" href="{{ url('/') }}">An eLearning Specialist</a>
    <a class="sid" href="{{ url('/') }}">An Interactive Designer</a>
    <a class="sid" href="{{ url('/') }}">An Interior Designer</a>
    <a class="sid" href="{{ url('/') }}">A VFX Specialist</a>
    <a class="sid" href="{{ url('/') }}">A Video Production Specialist</a>
    <a class="sid" href="{{ url('/') }}">A Web Designer</a>
    <a class="sid" href="{{ url('/') }}">A Web Developer</a>
    <a href="{{ url('/') }}">Zero to HERO</a>
    <a class="sid" href="{{ url('/') }}">3ds Max</a>
    <a class="sid" href="{{ url('/') }}">After Effects</a>
    <a class="sid" href="{{ url('/') }}">About</a>
    <a class="sid" href="{{ url('/') }}">AutoCAD Certification</a>
    <a class="sid" href="{{ url('/') }}">Captivate</a>
    <a class="sid" href="{{ url('/') }}">Cinema 4D</a>
    <a class="sid" href="{{ url('/') }}">About</a>
    <a class="sid" href="{{ url('/') }}">Illustrator</a>
    <a class="sid" href="{{ url('/') }}">InDesign</a>
    <a class="sid" href="{{ url('/') }}">JavaScript</a>
    <a class="sid" href="{{ url('/') }}">About</a>
    <a class="sid" href="{{ url('/') }}">Maya</a>
    <a class="sid" href="{{ url('/') }}">NodeJS</a>
    <a class="sid" href="{{ url('/') }}">Photoshop</a>
    <a class="sid" href="{{ url('/') }}">premiere pro</a>
    <a class="sid" href="{{ url('/') }}">Revit Architecture</a>
    <a class="sid" href="{{ url('/') }}">UX / UI Design</a>
    <a href="{{ url('/') }}">Ultimate Mastery</a>
    <a class="sid" href="{{ url('/') }}">After Effects: Ultimate Mastery</a>
    <a class="sid" href="{{ url('/') }}">Cinema 4D: Ultimate Mastery</a>
    <a class="sid" href="{{ url('/') }}">InDesign: Ultimate Mastery</a>
    <a class="sid" href="{{ url('/') }}">Illustrator: Ultimate Mastery</a>
    <a class="sid" href="{{ url('/') }}">Photoshop: Ultimate Mastery</a>
    <a class="sid" href="{{ url('/') }}">Premiere Pro: Ultimate Mastery</a>
</div>
<div class="container-fluid header">
    <div class="container-fluid menu">
            <nav class="navbar-inverse custom_class" data-spy="affix" data-offset-top="150">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle side_menu_toggle" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="logo">
                        <a href="{{ url('/') }}"> <img src="{{URL::asset('image/logo.png') }}" width="300px" height="55px"> </a>
                    </div>
                </div>

                <div class="meanu">
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a href="{{ url('/') }}" data-toggle="dropdown" class="dropbtn">COURSES <b
                                            class="caret"></b> </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a  href="{{ url('/adobe') }}">ADOBE <i class="icon-arrow-right"></i></a>
                                        <ul class="dropdown-menu  sub-menu">
                                            <li><a  href="{{ url('/expresiencedesign') }}">Acrobat pro DC</a></li>
                                            <li><a  href="{{ url('/after_effects') }}">After Effects</a></li>
                                            <li><a  href="{{ url('/animate') }}">Animate</a></li>
                                            <li><a  href="{{ url('/captivate') }}">Captivate</a></li>
                                            <li><a  href="{{ url('/creativecloud') }}">Creative Cloud </a></li>
                                            <li><a  href="{{ url('/expresiencedesign') }}">Experience Design</a></li>
                                            <li><a  href="{{ url('/illustrator') }}">lllustrator</a></li>
                                            <li><a  href="{{ url('/indesign') }}">In Design</a></li>
                                            <li><a  href="{{ url('/lightroom') }}">Light room</a></li>
                                            <li><a  href="{{ url('/muse') }}">Muse </a></li>
                                            <li><a  href="{{ url('/photoshop') }}">Photoshop Coures</a></li>
                                            <li><a  href="{{ url('/premierepro') }}">Premiere Pro</a></li>
                                        </ul>
                                    </li>

                                    <li>
                                        <a  href="{{ url('/autodesk') }}">AUTODESK <i class="icon-arrow-right"></i></a>
                                        <ul class="dropdown-menu sub-menu">
                                            <li><a href="{{ url('/dsmax') }}">3ds Max</a></li>
                                            <li><a href="{{ url('/autocad') }}">Auto CAD</a></li>
                                            <li><a href="{{ url('/fusion') }}">Fusion 360</a></li>
                                            <li><a href="{{ url('/inventor') }}">Inventor</a></li>
                                            <li><a href="{{ url('/bulindinginformation') }}">Building information Modelling(BIM)</a>
                                            </li>
                                            <li><a  href="{{ url('/maya') }}">Maya</a></li>
                                            <li><a  href="{{ url('/navisworks') }}">Navisworks</a></li>
                                            <li><a  href="{{ url('/revit') }}">Revit</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a  href="{{ url('/game') }}">3D/VR/GAMES <i class="icon-arrow-right"></i></a>
                                        <ul class="dropdown-menu sub-menu">
                                            <li><a  href="{{ url('/dsmax') }}">3ds Max</a></li>
                                            <li><a  href="{{ url('/cinema') }}">Cinema 4D</a></li>
                                            <li><a  href="{{ url('/hololens') }}">HoloLens</a></li>
                                            <li><a  href="{{ url('/maya') }}">Maya</a></li>
                                            <li><a  href="{{ url('/unity') }}">unity 3D</a></li>
                                            <li><a  href="{{ url('/unreal') }}">Unreal Engine 4</a></li>
                                            <li><a  href="{{ url('/sketchup') }}">Sketech Up</a></li>
                                            <li><a  href="{{ url('/fusion') }}">Fusion 360</a></li>
                                            <li><a  href="{{ url('/zbrush') }}">Zbrush</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a  href="{{ url('/expresiencedesign') }}">DESIGN <i class="icon-arrow-right"></i></a>
                                        <ul class="dropdown-menu sub-menu">
                                            <li><a  href="{{ url('/creativecloud') }}">Creative Cloud</a></li>
                                            <li><a  href="{{ url('/digitalart') }}">Digital Art</a></li>
                                            <li><a  href="{{ url('/designtequ') }}">Design Techniques</a></li>
                                            <li><a  href="{{ url('/illustrator') }}">lllustrator</a></li>
                                            <li><a  href="{{ url('/indesign') }}">In Design</a></li>
                                            <li><a  href="{{ url('/photoshop') }}">Photoshop Courses</a></li>
                                            <li><a  href="{{ url('/uxdesign') }}">Ux Design</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a  href="{{ url('/unity') }}"> UNITY </a>
                                    </li>
                                    <li>
                                        <a  href="{{ url('/cinema') }}"> MAXON </a>
                                    </li>
                                    <li>
                                        <a  href="{{ url('/expresiencedesign') }}">VIDEO/ANIMATION <i class="icon-arrow-right"></i></a>
                                        <ul class="dropdown-menu sub-menu">
                                            <li><a  href="{{ url('/dsmax') }}">3ds Max</a></li>
                                            <li><a  href="{{ url('/after_effects') }}">After Effects</a></li>
                                            <li><a  href="{{ url('/cinema') }}">Cinema 4D</a></li>
                                            <li><a  href="{{ url('/davinci') }}">Davinci Resolve</a></li>
                                            <li><a  href="{{ url('/home') }}">Final Cut Pro</a></li>
                                            <li><a  href="{{ url('/maya') }}">Maya</a></li>
                                            <li><a  href="{{ url('/home') }}">Motion</a></li>
                                            <li><a  href="{{ url('/nuke') }}">Nuke</a></li>
                                            <li><a  href="{{ url('/premierepro') }}">Premiere Pro</a></li>
                                            <li><a  href="{{ url('/unity') }}">Unity 3D</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li><a  href="{{ url('/certification') }}">CERTIFICATION</a></li>
                            <li>
                                <a href="#">
                                    <span class="slide_menu" style="font-size:17px; cursor:pointer">LEARNING PATHS</span>
                                </a>
                            </li>
                            <li><a  href="{{ url('/bespoke') }}">BESPOKE</a></li>

                            <div class="dropdowns">
                                <button class="dropbtns">ENQUIRE NOW
                                    <i class="fa fa-caret-down"></i>
                                </button>
                                <div class="dropdowns-content">
                                    <a href="{{ url('/') }}"> OR CALL:<font color="#b0c435"> 822 045 6017</font> </a>
                                </div>
                            </div>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</div>
@yield('content')
<div class="container-fluid footer">
    <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <h3>About Us</h3>
                <h5>Our classes are all hands-on</h5>
                <h5>and led by Certified Adobe,</h5>
                <h5>Autodesk, Maxon and Unity </h5>
                <h5>instructors who are also </h5>
                <h5>designers and developers.</h5>
                <h5>You’re in good hands!</h5>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <h3>Links</h3>
                <h5><a href="{{ url('/') }}"> Home </a></h5>
                <h5><a href="{{ url('/') }}"> Courses </a></h5>
                <h5><a href="{{ url('/') }}"> Packages </a></h5>
                <h5><a href="{{ url('/') }}"> Certification </a></h5>
                <h5><a href="{{ url('/') }}"> Video </a></h5>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <h3>Contact</h3>
                <h5>No 236c, Newpet,</h5>
                <h5>Near Daily Thanthi Office,</h5>
                <h5>Roundana Krishnagiri. </h5>
                <h5>Company No: 8220456017</h5>
                <br>
                <h5><a href="{{ url('/') }}"> Admin@kabsanimation.com </a></h5>
                <h5><a href="{{ url('/') }}"> Tel. 824 832 3087</a></h5>
                <br>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <h3>Follow us</h3>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <a href="{{ url('/') }}"> <img class="follows" src="{{URL::asset('image/fb.png') }}" width="35" height="50"></a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <a href="{{ url('/') }}">  <img class="follows" src="{{URL::asset('image/twitter1.png') }}" width="35" height="50"></a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <a href="{{ url('/') }}">  <img class="follows" src="{{URL::asset('image/linked.png') }}" width="35" height="50"></a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <a href="{{ url('/') }}">    <img class="follows" src="{{URL::asset('image/gmail.png') }}" width="35" height="50"> </a>

                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 whats_link">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <a href="{{ url('/') }}">
                        <button class="browser_whatsapp">WHATSAPP LINK  <img src="{{URL::asset('image/whatsapp.png') }}" width="25" height="25">
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

