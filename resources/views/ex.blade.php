<!DOCTYPE html>
<html lang="en">
<head>
    <title>CSS Template</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        #learning-paths .card::after {
            padding-top: 52%;
            padding-bottom: 0;
        }
        .card:not(.card-list-style)::after {
            padding-top: 56.25%;
            padding-bottom: 114px;
            display: block;
            content: '';
        }
        #learning-paths .card {
             max-height: none;
         }#learning-paths .card {
              cursor: auto;
              border-radius: 5px;
              padding: 0;
              margin-bottom: 20px;
              /* background-color: #000; */
              max-height: 105px;
          }
        .no-touch .card {
            box-shadow: none;
        }
        .card {
             border-left: 1px solid #eee;
             border-right: 1px solid #eee;
             border-radius: 5px;
             box-shadow: 0 0 8px rgba(0,0,0,.1);
             padding: 0;
             margin-top: 0;
         }
        .card {
            cursor: pointer;
            position: relative;
            background-color: #fff;
            overflow: hidden;
            border-top: 1px solid #eee;
            border-bottom: 1px solid #eee;
            padding: 10px 0;
            margin-top: -1px;
        }
        *, ::after, ::before {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        ul, ul li {
            list-style: none;
        }
        .tab-content > .active {
            visibility: visible;
        }body {
             font-family: proxima-nova,"Helvetica Neue",Helvetica,sans-serif;
             font-size: 13px;
             line-height: 1.42857143;
             color: #000;
         }#learning-paths .path-display {
              text-align: center;
          }


    </style>
</head>
<body>

<div class="card">
    <a class="card-content" href="/learning-paths/3D-Animation/improve-your-vector-illustration-skills">
        <div class="path-info">
            <h4>Improve Your Vector Illustration Skills</h4>
            <p><strong>Learning Path •</strong> 42 hours</p>
        </div>
    </a>
    <img src="https://cdn.lynda.com/static/landing/images/hero/ImproveYourVectorIllustrations_1200x630-1505154718721.jpg" alt="Improve Your Vector Illustration Skills" data-appear-top-offset="300">
</div>

</body>
</html>