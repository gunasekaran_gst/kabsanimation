@extends('layout.main')
@section('title', 'Dsmax | Animation Center in Krishnagiri')
@section('keywords', 'Animation Center in Krishnagiri')
@section('description', 'Animation Center in Krishnagiri')
@section('content')
    <div class="container-fluid after_home common_bg_style">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 adobe_animate">
                    <strong> Autodesk 3ds Max<br> Courses</strong>
                    <div class="animate"> These 3ds Max training courses are designed to get you up to speed with the
                        visualisation of architectural designs – interiors, furniture, lighting, from libraries or image
                        mapping. Whether you are a professional creative designer or a new user, Academy Class will take you
                        through a deck of tools and techniques to create extraordinary effects. 18-month free class retake
                        included.
                    </div>
                    <h5 class="animatess"><strong> Pick a 3ds Max course or package below.
                            Unsure which 3ds Max course level will be best for you?<a href="#"> <font color="#bfd432"> Click
                                    here </font> </a>to take our free online skills assessment and find out!
                        </strong>
                    </h5>
                    <img alt="adobe" title="adobe" src="{{URL::asset('image/after/adobe.jpg') }}" width="150" height="50">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="after_videoss">
                        <iframe class="iframeses" src="https://www.youtube.com/embed/W1w5Gk-E0Nk?feature=oembed"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                    <div class="descript"><strong>Class Snapshots:</strong></div>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/1.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/2.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/3.jpg') }}"><br>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 course_information">
                    <h2><strong>Information on courses:</strong></h2>
                    <div class="description"><strong>Full description</strong></div>
                    <div class="course_information_paragraph">3ds Max training courses at Academy Class range from the basic
                        beginner’s to the advanced professional level. We offer full courses in 3ds Max through which we
                        train students. Our 3ds Max courses are authorized by Autodesk. Now you can attend these courses in
                        the cities of London, Edinburgh, Glasgow, and Manchester.
                    </div>
                    <div class="course_information_paragraph">We have designed our coursework as such that makes learning
                        3ds Max a fun-filled experience. At Academy Class the tutors provide you practical examples along
                        with the technicalities of the software to guarantee you a complete command over 3ds Max.
                    </div>
                    <div class="course_information_paragraph">We have developed a whole curriculum spanning on different
                        levels of 3ds training which would make these courses not only an enriching experience but will also
                        leave you content and pleased with attending Academy Class.


                    </div>
                    <div class="course_information_paragraph">These 3ds Max courses features in-class training, supportive
                        learning environment, industry-certified trainers and up-to-date syllabus. Academy Class provides
                        you state-of-the-art PC workstations to facilitate advanced learning.


                    </div>
                    <div class="course_information_paragraph">Academy Class provides you high quality 3ds Max training
                        course and guarantees that it is a satisfying learning experience with us. We also offer you free of
                        charge re-sit of the course, within 18 months of your first day of training to refresh what you’ve
                        learned.
                    </div>
                    <div class="course_information_paragraph">Academy Class promises you total learning satisfaction with
                        regards to your 3ds Max course. Now you do not have to worry about investing a huge amount for your
                        training and not reaping the benefits. With Academy Class you can have complete peace of mind while
                        you leave your training in our capable hands.
                    </div>
                    <div class="course_information_paragraph">The 3ds Max courses offered have five types which defines the
                        level of grip Academy Class provides you over the subject matter. These 3ds Max training courses are
                        designed to get you up to date as swiftly as possible. Whether you are a professional creative
                        designer or a new user Academy Class got it all to gear you up to use 3ds Max like you know it by
                        heart.
                    </div>
                    <div class="course_information_paragraph">All classes at Academy Class’s begin at 9:30AM and end between
                        4:30PM
                    </div>

                    <div class="description"><strong>Blended Learning</strong></div>
                    <div class="course_information_paragraph">It’s the best opportunity to get the most out of your learning
                        experience while blending technology with classroom instructions. We supply training videos, notes
                        and/or reference texts.
                    </div>
                    <div class="description"><strong>18-month Free Class Retake</strong></div>
                    <div class="course_information_paragraph">If you have any gaps in your knowledge or want to refresh your
                        skills, you are more than welcome to come back and retake the live online class free of charge up to
                        18 months after you have taken the class.
                    </div>
                    <div class="description"><strong>Money-Back Guarantee</strong></div>
                    <div class="course_information_paragraph">If you don’t absolutely LOVE your class, we’ll give you a full
                        refund! Let us know on the FIRST day of your training if something isn’t quite right and give us a
                        chance to fix it or give you your money back.
                    </div>
                    <div class="description"><strong>Funding</strong></div>
                    <div class="course_information_paragraph">Because we’re committed to your success, we’re offering you
                        the opportunity to pay for your training monthly, rather than the whole cost upfront.
                    </div>
                    <a href="#">
                        <div class="more_info"><strong> Click here for more information</strong></div>
                    </a>
                    <div class="description"><strong>Experienced Instructors</strong></div>
                    <div class="course_information_paragraph">Equipped with years of industry experience our instructors
                        will assure a successful leap in your knowledge, improvement and preparation.
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="container-fluid log_form common_bg_styless">
        <div class="container">
            <div class="enq"><strong>Enquire now!</strong></div>
            <form method="post" action="#">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <select id="country" class="input" name="country" required>
                            <option value="au">Choose Location</option>
                            <option value="au">Australia</option>
                            <option value="ca">Canada</option>
                            <option value="usa">USA</option>
                            <option value="usa">Other</option>
                        </select>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <input type="text" class="input" id="fname" name="fname" placeholder="Company" required>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <input type="text" class="input" id="fname" name="fname" placeholder="Your name*" required>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <input type="text" class="input" id="fname" name="fname" placeholder="Your email*" required>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <input type="text" class="input" id="fname" name="fname" placeholder="Your Phone" required>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <textarea placeholder="Your message*" class="input" required></textarea>
                        <div class="special_offers">
                            <input type="checkbox" name="checkbox" value="">
                            I would like to get news about courses and special offers</div>
                        <button input type="submit" name="submit" class="btn">ENQUIRE NOW</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="container-fluid share_with">
        <div class="container">
            <div class="share"><strong>Share with:</strong></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 images_icon">
                    <a href="#"> <img src="image/twitter.png" width="60" height="40"> </a>
                    <a href="#"> <img src="image/fb3.png" width="60" height="40"> </a>
                    <a href="#"> <img src="image/in.png" width="60" height="40"> </a>
                </div>
            </div>
        </div>
    </div>

@endsection