
@extends('layout.main')
@section('title', 'Bespoke | Animation Center in Krishnagiri')
@section('keywords', 'Animation Center in Krishnagiri')
@section('description', 'Animation Center in Krishnagiri')
@section('content')
    <div class="container-fluid bespoke">
        <div class="container">
            <h2 class="bespoke_courses"><strong>Bespoke Courses</strong></h2>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 bespoke_bg">
                    <div class="theprocess"><span style="color: #ffffff;"><span
                                    style="color: #333333;"><strong>The Process</strong></span><br> </span>
                        <ul class="process">
                            <li><span style="color: #333333;">We analyse where you are now and where you want to be in the short and longer term.</span>
                            </li>
                            <li><span style="color: #333333;">A package is agreed with you to match your learning objectives.</span>
                            </li>
                            <li><span style="color: #333333;">You decide the format you prefer – class, private, online, videos.</span>
                            </li>
                            <li><span style="color: #333333;">You won’t be thrown to the wolves! Our support continues long after your training finishes.</span>
                            </li>
                            <li>
                                <span style="color: #333333;">Review – we’ll measure your feedback and review</span>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 bespoke_bg">
                    <div class="theprocess"><span style="color: #333333;"><strong>Benefits</strong></span>
                        <ul class="process">
                            <li><span style="color: #333333;">Content is specific to you and your current skillset.</span>
                            </li>
                            <li><span style="color: #333333;">Live projects can be developed during training delivery, optimising the time spent away from the office.</span>
                            </li>
                            <li><span style="color: #333333;">You receive insight into best practice, short cuts and workflow processes.</span>
                            </li>
                            <li><span style="color: #333333;">You can call upon the experienced trainer to offer ‘consultancy’ in relation to the project.</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid log_form common_bg_styless">
        <div class="container">
            <div class="enq"><strong>Enquire now!</strong></div>
            <form method="post" action="#">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <select id="country" class="input" name="country" required>
                            <option value="au">Choose Location</option>
                            <option value="au">Australia</option>
                            <option value="ca">Canada</option>
                            <option value="usa">USA</option>
                            <option value="usa">Other</option>
                        </select>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <input type="text" class="input" id="fname" name="fname" placeholder="Company" required>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <input type="text" class="input" id="fname" name="fname" placeholder="Your name*" required>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <input type="text" class="input" id="fname" name="fname" placeholder="Your email*" required>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <input type="text" class="input" id="fname" name="fname" placeholder="Your Phone" required>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <textarea placeholder="Your message*" class="input" required></textarea>
                        <div class="special_offers">
                            <input type="checkbox" name="checkbox" value="">
                            I would like to get news about courses and special offers</div>
                        <button input type="submit" name="submit" class="btn">ENQUIRE NOW</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection