@extends('layout.main')
@section('title', 'Uxdesign | Animation Center in Krishnagiri')
@section('keywords', 'Animation Center in Krishnagiri')
@section('description', 'Animation Center in Krishnagiri')
@section('content')

    <div class="container-fluid unreal_home common_bg_style">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 adobe_animatess">
                    <strong>User Experience (UX)<br> Design Courses</strong>
                    <div class="animate">Academy Class delivers UX Design training courses from beginner to advanced levels.
                        All User Experience Design training is taught by industry-experienced instructors, who aim to make
                        sure you learn everything needed to put your new skills into practice in the workplace. 18-month
                        free class retake included.
                    </div>
                    <h5 class="animatess"><strong>Pick a UX design course or package below.
                            Unsure which Ux design course level will be best for you?<a href="{{ url('/') }}"> <font color="#bfd432">
                                    Click

                                    here </font> </a>to take our free online skills assessment and find out!
                        </strong>
                    </h5>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                    <div class="unreal_img">
                        <img class="adobe" alt="ux" title="ux" src="{{URL::asset('image/ux.jpg') }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                    <img class="adobe" src="{{URL::asset('image/sanpshot/1.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/2.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/3.jpg') }}"><br>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 course_information">
                    <h2><strong>Information on courses:</strong></h2>
                    <div class="description"><strong>Full description</strong></div>
                    <div class="course_information_paragraph">Your UX Design course will feature small class sizes,
                        state-of-the-art Mac and PC workstations, great learning environments, industry-certified trainers,
                        the latest courseware, and careful attention to your specific needs and experience.
                    </div>
                    <div class="course_information_paragraph">Designing websites is not just about building blocks, it needs
                        to start with understanding user experience, planning and designing websites that work for your
                        clients. Research is an essential component of the UX design process; the purpose is to gather data
                        about the user’s behaviour, needs, limitations, etc. We will be looking at how you can use research
                        methods to collect this valuable data, which is used to make informed design decisions.
                    </div>

                    <div class="course_information_paragraph">Additionally, if you’re not confident about using what you learn on our public scheduled User Experience Design training courses, you can retake the class free of charge within 18 months of your first day of training. That’s the unique Academy Class Guarantee of Learning Satisfaction, delivering peace of mind for your training investment.
                    </div>

                    <div class="course_information_paragraph">Designing websites is not just about building blocks, it needs
                        to start with understanding user experience, planning and designing websites that work for your
                        clients. Research is an essential component of the UX design process; the purpose is to gather data
                        about the user’s behaviour, needs, limitations, etc. We will be looking at how you can use research
                        methods to collect this valuable data, which is used to make informed design decisions.
                    </div>

                    <div class="description"><strong>Blended Learning</strong></div>
                    <div class="course_information_paragraph">It’s the best opportunity to get the most out of your learning
                        experience while blending technology with classroom instructions. We supply training videos, notes
                        and/or reference texts.
                    </div>
                    <div class="description"><strong>18-month Free Class Retake</strong></div>
                    <div class="course_information_paragraph">If you have any gaps in your knowledge or want to refresh your
                        skills, you are more than welcome to come back and retake the live online class free of charge up to
                        18 months after you have taken the class.
                    </div>
                    <div class="description"><strong>Money-Back Guarantee</strong></div>
                    <div class="course_information_paragraph">If you don’t absolutely LOVE your class, we’ll give you a full
                        refund! Let us know on the FIRST day of your training if something isn’t quite right and give us a
                        chance to fix it or give you your money back.
                    </div>
                    <div class="description"><strong>Funding</strong></div>
                    <div class="course_information_paragraph">Because we’re committed to your success, we’re offering you
                        the opportunity to pay for your training monthly, rather than the whole cost upfront.
                    </div>
                    <a href="{{ url('/') }}">
                        <div class="more_info"><strong> Click here for more information</strong></div>
                    </a>
                    <div class="description"><strong>Experienced Instructors</strong></div>
                    <div class="course_information_paragraph">Equipped with years of industry experience our instructors
                        will assure a successful leap in your knowledge, improvement and preparation.
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="container-fluid log_form common_bg_styless">
        <div class="container">
            <div class="enq"><strong>Enquire now!</strong></div>
            <form method="post" action="#">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <select id="country" class="input" name="country" required>
                            <option value="au">Choose Location</option>
                            <option value="au">Australia</option>
                            <option value="ca">Canada</option>
                            <option value="usa">USA</option>
                            <option value="usa">Other</option>
                        </select>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <input type="text" class="input" id="fname" name="fname" placeholder="Company" required>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <input type="text" class="input" id="fname" name="fname" placeholder="Your name*" required>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <input type="text" class="input" id="fname" name="fname" placeholder="Your email*" required>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <input type="text" class="input" id="fname" name="fname" placeholder="Your Phone" required>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <textarea placeholder="Your message*" class="input" required></textarea>
                        <div class="special_offers">
                            <input type="checkbox" name="checkbox" value="">
                            I would like to get news about courses and special offers</div>
                        <button input type="submit" name="submit" class="btn">ENQUIRE NOW</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="container-fluid share_with">
        <div class="container">
            <div class="share"><strong>Share with:</strong></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 images_icon">
                    <a href="{{ url('/') }}"> <img alt="twitter" title="twitter"  src="{{URL::asset('image/twitter.png') }}" width="60" height="40"> </a>
                    <a href="{{ url('/') }}"> <img alt="fb3" title="fb3" src="{{URL::asset('image/fb3.png') }}" width="60" height="40"> </a>
                    <a href="{{ url('/') }}">  <img alt="in" title="in" src="{{URL::asset('image/in.png') }}" width="60" height="40"> </a>
                </div>
            </div>
        </div>
    </div>
@endsection