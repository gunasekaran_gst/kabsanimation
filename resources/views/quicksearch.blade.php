
@extends('layout.main')
@section('title', 'Quicksearch | Animation Center in Krishnagiri')
@section('keywords', 'Animation Center in Krishnagiri')
@section('description', 'Animation Center in Krishnagiri')
@section('content')
    <div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 academy_class">
                <h2 class="adobe_courses"><strong>Autodesk</strong></h2>
                <div class="course_information_paragraph">Scroll over the icons to find out more
                </div>
                <input id="woocommerce-product-search-field-0" class="search-field" placeholder="Search products…" value="" name="s" type="search">
                <div class="course_information_paragraph">Authorised Classes by Certified Autodesk Instructors Deliver Results! Academy Class delivers Accredited Autodesk training courses at all levels, ranging from beginner to advanced. All training is taught by Autodesk-certified, industry-experienced instructors, who aim to teach you everything you need to put your skills into practice in the workplace.
                </div>
            </div>
        </div>
    </div>
@endsection