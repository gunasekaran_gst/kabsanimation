@extends('layout.main')
@section('title', 'Animate | Animation Center in Krishnagiri')
@section('keywords', 'Animation Center in Krishnagiri')
@section('description', 'Animation Center in Krishnagiri')
@section('content')
    <div class="container-fluid after_home common_bg_style">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 adobe_animate">
                    <strong> Adobe Animate<br> Courses</strong>
                    <div class="animate"> Academy Class delivers Adobe Animate training courses from beginner to
                        advanced levels. All Animate training is taught by Adobe-certified, industry-experienced
                        instructors, who aim to make sure you learn everything needed to put your new skills into
                        practice in the workplace. 18-month Free class retake included.
                    </div>
                    <h5 class="animatess"><strong> Pick a Animate course or package below.
                            Unsure which Animate course level will be best for you?<a href="{{ url('/') }}" > <font color="#bfd432"> Click here </font> </a>to take our free online
                            skills assessment and find out!</strong>
                    </h5>
                   <img src="{{URL::asset('image/after/adobe.jpg') }}" width="150" height="50">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="after_videoss">
                        <iframe class="iframeses" src="https://images-tv.adobe.com/avp/vr/15a99ccf-0e7c-4601-b270-87dd82624086/5078a43c-81f9-4a93-836c-815278b83a8e/e9cf12a0-7c4b-414f-a5c9-97ef49340aa9_20160203035417.960x540at1200_h264.mp4?"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 choose_your">
                    <div class="descript"><strong>Class Snapshots:</strong></div>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/1.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/2.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/3.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/4.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/5.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/6.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/7.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/3.jpg') }}"><br>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 course_information">
                    <h2><strong>Information on courses:</strong></h2>
                    <div class="description"><strong>Full description</strong></div>
                    <div class="course_information_paragraph">Adobe Animate CC is the new industry standard application for
                        creating rich animations for the web. Taking over where Flash left off, Animate provides a full set
                        of features for creating cutting-edge interactive animations and publish them across all modern
                        platforms such as Flash/AIR, HTML5, WebGL and more. Now you can truly reach viewers on any desktop
                        or mobile device with no restrictions.
                    </div>
                    <div class="course_information_paragraph">The Academy Class Animate CC courses are designed to teach you
                        everything you need to get you up and running with the software in no-time. Whether you will be
                        creating HTML5 Rich Media Banner Ads, Interactive Animations or a fancy Animated Infographic, the
                        course material will give you transferable skills that can be applied to producing any type of
                        content you want.
                    </div>
                    <div class="course_information_paragraph">The introduction covers the very basics including the software
                        interface, the tool set, how to set up your projects and some of the most commonly used industry
                        workflows. You will then learn the core concepts of Animate CC such as symbols, transforms, easings
                        and masking.
                    </div>
                    <div class="course_information_paragraph">As you get more comfortable with creating different types of
                        layouts and animations, it’s time to move onto more advanced concepts and look at scripting for
                        adding an interactive layer to your creations via actions, triggers and events. Following that you
                        will learn how to publish the project to your platform of choice.
                    </div>
                    <div class="course_information_paragraph">The curriculum is designed to give novices a head start with
                        Animate CC even with no previous experience with using it, Adobe Flash, or any other equivalent
                        software before. Previous Adobe Flash users, however, will also benefit by learning the updated
                        toolset of Animate CC, the latest industry workflows and how to produce animations for modern,
                        mobile platforms.
                    </div>
                    <div class="course_information_paragraph">All our Aniamte CC course instructors are industry-experienced
                        Adobe-Certified professionals. Students will be given a solid theoretical foundation followed-up by
                        a varied set of exercises aimed at replicating real-world-project situations. At the end of the
                        course you will be able to fully deliver Animate CC based projects.
                    </div>
                    <div class="course_information_paragraph">At Academy Class we assure you a total learning satisfaction
                        with regards to your Adobe Animate CC training courses. Now you no longer have to agonise over
                        investing a huge amount in your training and not reaping the benefits. With Academy Class you can
                        have complete peace of mind whilst you leave your training in our capable hands.
                    </div>
                    <div class="course_information_paragraph">Academy Class provides you high quality Animate CC training
                        course and guarantees that it is a satisfying learning experience with us. We also offer you free of
                        charge re-sit of the course, within 18 months of your first day of training to refresh what you’ve
                        learned.
                    </div>
                    <div class="course_information_paragraph">All our classes at Academy Class begin at 9.30am and running
                        till 4:30pm. Enrol now in one of the best Animate CC, Adobe-authorised training courses.
                    </div>
                    <div class="description"><strong>Blended Learning</strong></div>
                    <div class="course_information_paragraph">It’s the best opportunity to get the most out of your learning
                        experience while blending technology with classroom instructions. We supply training videos, notes
                        and/or reference texts.
                    </div>
                    <div class="description"><strong>18-month Free Class Retake</strong></div>
                    <div class="course_information_paragraph">If you have any gaps in your knowledge or want to refresh your
                        skills, you are more than welcome to come back and retake the live online class free of charge up to
                        18 months after you have taken the class.
                    </div>
                    <div class="description"><strong>Money-Back Guarantee</strong></div>
                    <div class="course_information_paragraph">If you don’t absolutely LOVE your class, we’ll give you a full
                        refund! Let us know on the FIRST day of your training if something isn’t quite right and give us a
                        chance to fix it or give you your money back.
                    </div>
                    <div class="description"><strong>Funding</strong></div>
                    <div class="course_information_paragraph">Because we’re committed to your success, we’re offering you
                        the opportunity to pay for your training monthly, rather than the whole cost upfront.
                    </div>
                    <a href="{{ url('/') }}">  <div class="more_info"> <strong> Click here for more information</strong> </div> </a>
                    <div class="description"><strong>Experienced Instructors</strong></div>
                    <div class="course_information_paragraph">Equipped with years of industry experience our instructors
                        will assure a successful leap in your knowledge, improvement and preparation.
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="container-fluid log_form common_bg_styless">
        <div class="container">
            <div class="enq"><strong>Enquire now!</strong></div>
            <form method="post" action="#">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <select id="country" class="input" name="country" required>
                            <option value="au">Choose Location</option>
                            <option value="au">Australia</option>
                            <option value="ca">Canada</option>
                            <option value="usa">USA</option>
                            <option value="usa">Other</option>
                        </select>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <input type="text" class="input" id="fname" name="fname" placeholder="Company" required>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <input type="text" class="input" id="fname" name="fname" placeholder="Your name*" required>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <input type="text"  class="input" id="fname" name="fname" placeholder="Your email*" required>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <input type="text" class="input" id="fname" name="fname" placeholder="Your Phone" required>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <textarea placeholder="Your message*" class="input"  required></textarea>
                        <div class="special_offers">
                            <input type="checkbox" name="checkbox" value="">
                            I would like to get news about courses and special offers</div>
                        <button input type="submit" name="submit" class="btn">ENQUIRE NOW</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="container-fluid share_with">
        <div class="container">
            <div class="share"><strong>Share with:</strong></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 images_icon">
                    <a href="{{ url('/') }}"> <img alt="twitter" title="twitter"  src="{{URL::asset('image/twitter.png') }}" width="60" height="40"> </a>
                    <a href="{{ url('/') }}"> <img alt="fb3" title="fb3" src="{{URL::asset('image/fb3.png') }}" width="60" height="40"> </a>
                    <a href="{{ url('/') }}">  <img alt="in" title="in" src="{{URL::asset('image/in.png') }}" width="60" height="40"> </a>
                </div>
            </div>
        </div>
    </div>
@endsection