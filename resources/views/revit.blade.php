@extends('layout.main')
@section('title', 'Revit | Animation Center in Krishnagiri')
@section('keywords', 'Animation Center in Krishnagiri')
@section('description', 'Animation Center in Krishnagiri')
@section('content')

    <div class="container-fluid after_home common_bg_style">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 adobe_animatess">
                    <strong>Autodesk Revit<br>Courses</strong>
                    <div class="animate">Learn the essentials of 3D building models and to create 2D drawings with
                        schedules, estimates and facility planning reports from the construction designs. Edit and
                        regenerate complex surfaces, sub regions and building pads. You will also learn to easily share
                        model data with the design team for a design process that will help you deliver projects
                        collaboratively. 18-month Free class retake included.
                    </div>
                    <h5 class="animatess"><strong>Pick a Revit course or package below.
                            Unsure which Revit course level will be best for you? <a href="{{ url('/') }}"> <font color="#bfd432">
                                    Click
                                    here </font> </a> to take our free online skills assessment and find out!
                        </strong>
                    </h5>
                    <img alt="adobe" title="adobe" src="{{URL::asset('image/after/adobe.jpg') }}" width="150" height="50">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="after_videoss">
                        <iframe class="iframeses" src="https://www.youtube.com/embed/ghmb6o2ed_4?feature=oembed"></iframe>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                    <div class="descript"><strong>Class Snapshots:</strong></div>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/1.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/2.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/3.jpg') }}"><br>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 course_information">
                    <h2><strong>Information on courses:</strong></h2>
                    <div class="description"><strong>Full description</strong></div>
                    <div class="course_information_paragraph">Revit is Autodesk’ s Building Information Modeling (BIM)
                        software that caters to all aspects of a building designing project, used in architectural projects
                        around the world by professional architects, designers and facility planners. This powerful
                        application is popular because it lets you create and work with integrated construction documents.
                    </div>
                    <div class="course_information_paragraph">Academy Class brings its Revit courses to equip designers and
                        architects with the essentials of 3D building models and create 2D drawings with schedules,
                        estimates and facility planning reports from the construction designs. You will learn to easily
                        share model data ac with the design team for a collaborative design process that would help you
                        deliver projects resourcefully.</div>
                    <div class="course_information_paragraph">These Revit training courses are taught by Autodesk
                        certified and industry experienced architects and construction designers. You will learn to
                        develop high quality and more precise architectural designs using information-rich models for
                        more well-versed design decisions to support sustainable design, clash detection, construction
                        planning, and fabrication. Moreover, any changes made are auto-updated throughout the project,
                        keeping the design and documentation synchronized and consistent.
                    </div>
                    <div class="course_information_paragraph">The Revit courses will begin with the basics of 3D
                        building designing and will include training of Building Information Modeling, viewing the
                        building model, loading additional building components, using dimensions and constraints, design
                        modeling, detailing and drafting, construction documentation, using walk through technique, sun
                        and shadow techniques, creating legends and keynotes, and rendering the projects.
                    </div>
                    <div class="course_information_paragraph">
                        Academy Class will also educate students in mass objects, work sets and phases, annotations,
                        modifying schedules, area and room plans, graphic outputs, design options and logical formulae.
                        Upon the completion of your Revit training course at Academy Class you will be geared with the
                        knowledge to create both residential and commercial multifaceted architectural models.
                    </div>
                    <div class="course_information_paragraph">
                        With Academy Class you can have complete peace of mind while you leave your training in our
                        expert hands. These Revit training courses are designed to get you up to date with construction
                        designing as swiftly as possible. If after attending the Revit training with Academy Class you
                        do not get a chance to use the software or forget something then you are more than welcome to
                        come back and retake the class free of charge. That is the Academy Class’ guarantee of learning.
                    </div>
                    <div class="course_information_paragraph">
                        All our classes at Academy Class begin at 9.30am and running till 4:30pm. Enroll now in one of
                        the best Revit, Autodesk authorized training courses!
                    </div>

                    <div class="description"><strong>Blended Learning</strong></div>
                    <div class="course_information_paragraph">It’s the best opportunity to get the most out of your
                        learning
                        experience while blending technology with classroom instructions. We supply training videos,
                        notes
                        and/or reference texts.
                    </div>
                    <div class="description"><strong>18-month Free Class Retake</strong></div>
                    <div class="course_information_paragraph">If you have any gaps in your knowledge or want to refresh
                        your
                        skills, you are more than welcome to come back and retake the live online class free of charge
                        up to
                        18 months after you have taken the class.
                    </div>
                    <div class="description"><strong>Money-Back Guarantee</strong></div>
                    <div class="course_information_paragraph">If you don’t absolutely LOVE your class, we’ll give you a
                        full
                        refund! Let us know on the FIRST day of your training if something isn’t quite right and give us
                        a
                        chance to fix it or give you your money back.
                    </div>
                    <div class="description"><strong>Funding</strong></div>
                    <div class="course_information_paragraph">Because we’re committed to your success, we’re offering
                        you
                        the opportunity to pay for your training monthly, rather than the whole cost upfront.
                    </div>
                    <a href="{{ url('/') }}">
                        <div class="more_info"><strong> Click here for more information</strong></div>
                    </a>
                    <div class="description"><strong>Experienced Instructors</strong></div>
                    <div class="course_information_paragraph">Equipped with years of industry experience our instructors
                        will assure a successful leap in your knowledge, improvement and preparation.
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="container-fluid log_form common_bg_styless">
        <div class="container">
            <div class="enq"><strong>Enquire now!</strong></div>
            <form method="post" action="#">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <select id="country" class="input" name="country" required>
                            <option value="au">Choose Location</option>
                            <option value="au">Australia</option>
                            <option value="ca">Canada</option>
                            <option value="usa">USA</option>
                            <option value="usa">Other</option>
                        </select>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <input type="text" class="input" id="fname" name="fname" placeholder="Company" required>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <input type="text" class="input" id="fname" name="fname" placeholder="Your name*" required>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <input type="text" class="input" id="fname" name="fname" placeholder="Your email*" required>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <input type="text" class="input" id="fname" name="fname" placeholder="Your Phone" required>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <textarea placeholder="Your message*" class="input" required></textarea>
                        <div class="special_offers">
                            <input type="checkbox" name="checkbox" value="">
                            I would like to get news about courses and special offers</div>
                        <button input type="submit" name="submit" class="btn">ENQUIRE NOW</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="container-fluid share_with">
        <div class="container">
            <div class="share"><strong>Share with:</strong></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 images_icon">
                    <a href="{{ url('/') }}"> <img src="{{URL::asset('image/twitter.png') }}" width="60" height="40"> </a>
                    <a href="{{ url('/') }}"> <img src="{{URL::asset('image/fb3.png') }}" width="60" height="40"> </a>
                    <a href="{{ url('/') }}">  <img src="{{URL::asset('image/in.png') }}" width="60" height="40"> </a>
                </div>
            </div>
        </div>
    </div>
@endsection