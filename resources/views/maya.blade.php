@extends('layout.main')
@section('title', 'Maya | Animation Center in Krishnagiri')
@section('keywords', 'Animation Center in Krishnagiri')
@section('description', 'Animation Center in Krishnagiri')
@section('content')

    <div class="container-fluid after_home common_bg_style">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 adobe_animate">
                    <strong>Autodesk Maya <br>Training Courses</strong>
                    <div class="animate"> Whether you are a professional creative graphic designer, animation specialist or
                        a novice learner, our Maya training course introduces you to industry-standard animation software.
                        Maya is a comprehensive 3D solution for visual effects artists, modellers and animators to create
                        high-quality content for film and TV, games and advertising through 3D animation, modelling
                        simulation, and rendering feature sets. 18-month Free class retake included.
                    </div>
                    <h5 class="animatess"><strong>Pick a Maya course or package below.
                            Unsure which Maya course level will be best for you?<a href="#"> <font color="#bfd432"> Click
                                    here </font> </a> to take our free online skills assessment and find out!
                        </strong>
                    </h5>
                    <img alt="adobe" title="adobe" src="{{URL::asset('image/after/adobe.jpg') }}" width="150" height="50">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="after_videoss">
                        <iframe class="iframeses" src="https://www.youtube.com/embed/v3_CCG8z5gg?feature=oembed"></iframe>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                    <div class="descript"><strong>Class Snapshots:</strong></div>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/1.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/2.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/3.jpg') }}"><br>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 course_information">
                    <h2><strong>Information on courses:</strong></h2>
                    <div class="description"><strong>Full description</strong></div>
                    <div class="course_information_paragraph">Maya training courses at Academy Class will teach you the
                        fundamentals of 3D designing and animations. Students will get to learn the Maya basics of Skinning,
                        Lighting and Rendering, and Utility Nodes, Texturing and Texture Mapping, Adobe Photoshop Basics,
                        Tiling and Layering Textures, Block Texturing a Non-Organic Object and Texture Distressing.
                    </div>
                    <div class="course_information_paragraph">Furthermore, students with our Maya training course will also
                        learn advanced Shading networks with File Textures, Mental Ray Shaders, Maya Hypershade, Graph
                        Editor and Key Framing, Deformer and Path Animation, Clusters, Blend-shapes and connections,
                        Expressions and Rigging, Lamp Animations.
                    </div>
                    <div class="course_information_paragraph">The Maya training courses at Academy Class will also entail
                        the Art of Lighting, Gobos and Cookies, Daylight, Night and Artificial Lighting, Image Based
                        Lighting, Photographic Lighting, Lighting and Shadows, Ray Tracing and Depth of Field, Final Gather
                        and Image Based Lighting, Render Layers and Compositing and Photon Mapping and Caustics. Along with
                        that you will also learn NURBS Coliseum, Sandal and Kettle.
                    </div>
                    <div class="course_information_paragraph">At Academy Class you will also be trained at Curve Modeling,
                        Procedural Texturing, Polygonal Modeling, Efficient Modeling, Organic Modeling, Hard Surface
                        Polygonal Modeling, Non Organic UV Mapping, Organic UV Mapping. You will also be studying facial
                        expressions and lip-sync, Alpha channels and Z-buffers, 2D and 3D paint effects, visual effects,
                        advanced compositing, and advanced digital painting. In addition with character Kinematics, special
                        effects and character animation, character Sculpting, Anatomy and character designing.


                    </div>
                    <div class="course_information_paragraph">These Maya courses feature in-class training, supportive
                        learning environment, industry-certified trainers and up-to-date syllabus. Academy Class provides
                        you state-of-the-art Mac and PC workstations to facilitate advanced learning. All our Maya training
                        courses instructors are Autodesk-certified trainers.
                    </div>
                    <div class="course_information_paragraph">If after taking the Maya training course with Academy Class
                        you do not get a chance to use the technology or forget something then you are more than welcome to
                        come back and retake the class free of charge. That is the Academy Class’ guarantee of learning.
                    </div>
                    <div class="course_information_paragraph">The Maya training courses offered have three levels which
                        define the level of grip Academy Class provides you over the subject matter. These Maya training
                        courses are designed to get you up to speed on Maya swiftly. Academy Class offers you Maya training
                        courses in a number of cities: London, Glasgow, Manchester, Cardiff, Newcastle, Birmingham and
                        Leeds.
                    </div>
                    <div class="course_information_paragraph">All our classes at Academy Class begin at 9.30am and running
                        till 4:30pm. Enroll now in one of the best Maya, Autodesk-authorized training course.
                    </div>

                    <div class="description"><strong>Blended Learning</strong></div>
                    <div class="course_information_paragraph">It’s the best opportunity to get the most out of your learning
                        experience while blending technology with classroom instructions. We supply training videos, notes
                        and/or reference texts.
                    </div>
                    <div class="description"><strong>18-month Free Class Retake</strong></div>
                    <div class="course_information_paragraph">If you have any gaps in your knowledge or want to refresh your
                        skills, you are more than welcome to come back and retake the live online class free of charge up to
                        18 months after you have taken the class.
                    </div>
                    <div class="description"><strong>Money-Back Guarantee</strong></div>
                    <div class="course_information_paragraph">If you don’t absolutely LOVE your class, we’ll give you a full
                        refund! Let us know on the FIRST day of your training if something isn’t quite right and give us a
                        chance to fix it or give you your money back.
                    </div>
                    <div class="description"><strong>Funding</strong></div>
                    <div class="course_information_paragraph">Because we’re committed to your success, we’re offering you
                        the opportunity to pay for your training monthly, rather than the whole cost upfront.
                    </div>
                    <a href="#">
                        <div class="more_info"><strong> Click here for more information</strong></div>
                    </a>
                    <div class="description"><strong>Experienced Instructors</strong></div>
                    <div class="course_information_paragraph">Equipped with years of industry experience our instructors
                        will assure a successful leap in your knowledge, improvement and preparation.
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="container-fluid log_form common_bg_styless">
        <div class="container">
            <div class="enq"><strong>Enquire now!</strong></div>
            <form method="post" action="#">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <select id="country" class="input" name="country" required>
                            <option value="au">Choose Location</option>
                            <option value="au">Australia</option>
                            <option value="ca">Canada</option>
                            <option value="usa">USA</option>
                            <option value="usa">Other</option>
                        </select>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <input type="text" class="input" id="fname" name="fname" placeholder="Company" required>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <input type="text" class="input" id="fname" name="fname" placeholder="Your name*" required>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <input type="text" class="input" id="fname" name="fname" placeholder="Your email*" required>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <input type="text" class="input" id="fname" name="fname" placeholder="Your Phone" required>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <textarea placeholder="Your message*" class="input" required></textarea>
                        <div class="special_offers">
                            <input type="checkbox" name="checkbox" value="">
                            I would like to get news about courses and special offers</div>
                        <button input type="submit" name="submit" class="btn">ENQUIRE NOW</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="container-fluid share_with">
        <div class="container">
            <div class="share"><strong>Share with:</strong></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 images_icon">
                    <a href="{{ url('/') }}"> <img src="{{URL::asset('image/twitter.png') }}" width="60" height="40"> </a>
                    <a href="{{ url('/') }}"> <img src="{{URL::asset('image/fb3.png') }}" width="60" height="40"> </a>
                    <a href="{{ url('/') }}">  <img src="{{URL::asset('image/in.png') }}" width="60" height="40"> </a>
                </div>
            </div>
        </div>
    </div>
@endsection