@extends('layout.main')
@section('title', 'Unity | Animation Center in Krishnagiri')
@section('keywords', 'Animation Center in Krishnagiri')
@section('description', 'Animation Center in Krishnagiri')
@section('content')

    <div class="container-fluid autocad_home common_bg_style">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 adobe_after">
                    <strong>Unity 3D Training<br>Courses</strong>
                    <h5 class="strong"><strong>Unity is a flexible and powerful development platform for creating
                            multiplatform 3D and 2D games and interactive experiences. </strong>
                    </h5>
                    <h5 class="strong">It’s a complete ecosystem for anyone who aims to build a business on creating
                        high-end content and connecting to their most loyal and enthusiastic players and customers.
                    </h5>
                    <h5 class="strong"><strong>18-month free class retake included. </strong>
                    </h5>

                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                    <div class="after_video">
                        <iframe class="iframe" src="https://www.youtube.com/embed/gyVcPEq9jkE"></iframe>

                    </div>
                </div>
            </div>
        </div>


        <div class="container-fluid ss_sanpshot">
            <div class="container">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses button">
                        <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                            <button type="button" class="moredetails" class="moredetails-arrow-down">MORE DEATILS</button>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                        <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                            <button type="button" class="charwith">CHAT WITH US</button>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                        <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                            <button type="button" class="phonenumber">8220456017</button>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                        <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                            <button type="button" class="enquire">ENQUIRE NOW</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="descript"><strong>Class Snapshots:</strong></div>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/1.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/2.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/3.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/4.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/5.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/3.jpg') }}"><br>
                </div>


                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 course_information">
                    <h2><strong> Course Information:</strong></h2>
                    <div class="description"><strong>Unity 3D Course Description</strong></div>
                    <div class="course_information_paragraph">Academy Class provides training in Unity 3d. We offer full
                        courses in Unity 3d through which we train students. Now you can attend these courses in the cities
                        of London, Glasgow, and Manchester.
                    </div>

                    <div class="course_information_paragraph">Academy Class courses in Unity 3d range from beginner’s to
                        advanced professional levels. We can assure you will learn everything that will help your
                        professional career and take home all the knowledge you need, because all our Unity 3d instructors
                        are highly experienced.
                    </div>
                    <div class="description"><strong>What will you learn?</strong></div>
                    <div class="course_information_paragraph"><img src="{{URL::asset('image/cb.jpg') }}" width="20" height="20">  Our Adobe
                        You will learn to develop high quality and more precise architectural designs using information-rich
                        models for more well-versed design decisions to support sustainable design, clash detection,
                        construction planning, and fabrication.
                    </div>

                    <div class="course_information_paragraph"><img src="{{URL::asset('image/cb.jpg') }}" width="20" height="20">  These Adobe
                        The Revit courses will begin with the basics of 3D building designing and will include training of
                        Building Information Modeling, viewing the building model, loading additional building components,
                        using dimensions and constraints, design modeling, detailing and drafting, construction
                        documentation, using walk through technique, sun and shadow techniques, creating legends and
                        keynotes, and rendering the projects.
                    </div>

                    <div class="description"><strong>Blended Learning</strong></div>
                    <div class="course_information_paragraph">It’s the best opportunity to get the most out of your learning
                        experience while blending technology with classroom instructions. We supply:
                        <ul class="h5 strong">
                            <li>training videos,</li>
                            <li>notes and/or</li>
                            <li>reference texts.</li>
                        </ul>
                    </div>

                    <h2 class="learn"><strong> How You Want To Learn</strong></h2>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 course_information">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <img class="right"  src="{{URL::asset('image/cb.jpg') }}" width="20" height="20">
                            <div class="course_information_paragraphs"><strong>Individual</strong></div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 course_information">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <img class="right"  src="{{URL::asset('image/cb.jpg') }}" width="20" height="20">
                            <div class="course_information_paragraphs"><strong>Customis<br>ed</strong></div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 course_information">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <img class="right"  src="{{URL::asset('image/cb.jpg') }}" width="20" height="20">
                            <div class="course_information_paragraphs"><strong>Classroom</strong></div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 course_information">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <img class="right"  src="{{URL::asset('image/cb.jpg') }}" width="20" height="20">
                            <div class="course_information_paragraphs"><strong> Live-Online </strong></div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h1><strong>Still Not Convinced?</strong></h1>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="still"><strong>18-Month Free<br>Class Retake</strong>
                        </div>
                        <h>If you have any gaps in your knowledge or want to refresh your skills, you are more than
                            welcome to come back and retake the live online class free of charge up to 18 months after
                            you have taken the class.
                        </h>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="still"><strong>Money-Back <br>
                                Guarantee</strong>
                        </div>
                        <h>If you don’t absolutely LOVE your class, we’ll give you a full refund! Let us know on the
                            FIRST day of your training if something isn’t quite right and give us a chance to fix it or
                            give you your money back.
                        </h>
                    </div>


                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="still"><strong>Lower Price<br>Guarantee</strong>
                        </div>
                        <h>We think our prices are pretty fair but we won’t be beaten on our fee. We’ll match and
                            discount by 10% any like-for-like Training course price.
                        </h>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="still"><strong>Experienced<br>Instructors</strong>
                        </div>
                        <h>Equipped with years of industry experience our instructors will assure a successful leap in
                            your knowledge, improvement and preparation.
                        </h>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="two"><strong> “The investment in knowledge pays the best interests.”</strong></div>
                            <br>
                            <p class="frank">~ Benjamin Franklin</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="container-fluid ss_sanpshot">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses button">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                        <button type="button" class="moredetails" class="moredetails-arrow-down">MOREDEATILS</button>
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                        <button type="button" class="charwith">CHAT WITH US</button>
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                        <button type="button" class="phonenumber">8220456017</button>
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                        <button type="button" class="enquire">ENQUIRENOW</button>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid course_time">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 course_times">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
                        <h3 class="course-time"><span
                                    style="color: #ffffff;">Course<br> Times:</span>
                        </h3>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 course_times">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
                        <img  src="{{URL::asset('image/course1.png') }}" width="70" height="70">

                        <br>
                        <br>
                        <p style="text-align: center;">
                            <span style="color: #00969d; margin-top: 5px;">9:00 – 11:00</span><br>
                            <span style="color: #ffffff;">Course</span>
                        </p>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 course_times">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
                        <img  src="{{URL::asset('image/course2.jpg') }}" width="70" height="70">

                        <br>
                        <br>
                        <p style="text-align: center;">
                            <span style="color: #00969d; margin-top: 5px;">11:00 – 11:15</span><br>
                            <span style="color: #ffffff;">Break</span>
                        </p>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 course_times">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
                        <img  src="{{URL::asset('image/course3.png') }}" width="70" height="70">
                        <br>
                        <br>
                        <p style="text-align: center;">
                            <span style="color: #00969d; margin-top: 5px;">11:15 – 13:00</span><br>
                            <span style="color: #ffffff;">Course</span>
                        </p>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 course_times">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
                        <img  src="{{URL::asset('image/course4.png') }}" width="70" height="70">
                        <br>
                        <br>
                        <p style="text-align: center;">
                            <span style="color: #00969d; margin-top: 5px;">13:00 – 14:00</span><br>
                            <span style="color: #ffffff;">Break</span>
                        </p>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 course_times">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
                        <img  src="{{URL::asset('image/course5.png') }}" width="70" height="70">
                        <br>
                        <br>
                        <p style="text-align: center;">
                            <span style="color: #00969d; margin-top: 5px;">14:00 – 16:30</span><br>
                            <span style="color: #ffffff;">Course</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <h2 class="course_leaves"><strong>Course Levels</strong></h2>
                    <div class="single_course"> SINGLE COURSES</div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <img  src="{{URL::asset('image/single1.jpg') }}" width="45" height="45">
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="yellow"><strong>Yellow Belt:<br>101</strong></div>
                            <div class="package">Beginners</div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <img  src="{{URL::asset('image/single2.jpg') }}" width="45" height="45">
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="yellow"><strong>Green Belt:<br>201</strong></div>
                            <div class="package">Intermediate</div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <img  src="{{URL::asset('image/single3.jpg') }}" width="45" height="45">
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="yellow"><strong>Black Belt:<br>301</strong></div>
                            <div class="package">Advanced</div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <img  src="{{URL::asset('image/single4.jpg') }}" width="45" height="45">
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="yellow"><strong>Certified: 401</strong></div>
                            <div class="package">Expert</div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="single_course">PACKAGED COURSES</div>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <img  src="{{URL::asset('image/single5.jpg') }}" width="45" height="45">
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="yellow"><strong>Zero to Hero</strong></div>
                            <div class="packagese">Complete<br>
                                courses 101,<br>
                                201 and 301 in a<br>
                                combined and<br>
                                discounted<br>
                                package.
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <img  src="{{URL::asset('image/single6.jpg') }}" width="45" height="45">
                        </div>

                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="yellow"><strong>Ultimate<br>Mastery</strong></div>
                            <div class="packagese">Complete<br>
                                courses 101,<br>
                                201, 301 and<br>
                                401 in a<br>
                                combined and<br>
                                discounted<br>
                                package.
                            </div>
                        </div>
                    </div>

                </div>


                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 course_information">
                    <h2 class="enquirenow"><strong> That’s us. That’s you. Enquire now!</strong></h2>
                    <form method="post" action="#">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <select id="country" name="country" required>
                                <option value="au">Choose Location</option>
                                <option value="au">Australia</option>
                                <option value="ca">Canada</option>
                                <option value="usa">USA</option>
                                <option value="usa">Other</option>
                            </select>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                            <input type="text" id="fname" name="fname" placeholder="Company" required>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                            <input type="text" id="fname" name="fname" placeholder="Your name*" required>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                            <input type="text" id="fname" name="fname" placeholder="Your email*" required>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                            <input type="text" id="fname" name="fname" placeholder="Your Phone" required>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                            <textarea placeholder="Your message*" required></textarea>
                            <div class="special_offers">
                                <input type="checkbox" name="checkbox" value="">
                                I would like to get news about courses and special offers</div>
                            <button input type="submit" name="submit" class="btn">ENQUIRE NOW</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <div class="container-fluid ss_sanpshot">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses button">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                        <button type="button" class="moredetails" class="moredetails-arrow-down">MOREDEATILS</button>
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                        <button type="button" class="charwith">CHAT WITH US</button>
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                        <button type="button" class="phonenumber">8220456017</button>
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                        <button type="button" class="enquire">ENQUIRENOW</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection